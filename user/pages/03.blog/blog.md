---
title: JOURNAL
subheading: 'Backpacking, Running, and Cycling Adventures'
header_image: topo_web_1.jpg
content:
    items: '@self.children'
    limit: 10
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
media_order: header_sub.jpg
body_classes: blog-list
---

