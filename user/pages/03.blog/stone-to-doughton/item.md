---
title: 'Stone Mountain to Doughton Park'
subheading: 'There is a thread that weaves its way all the way across North Carolina and that thread is called the Mountains-to-Sea trail. We used a tiny bit of this thread to connect Stone Mountain State Park and Doughton Park in the form of a lollipop route. Keep reading to find out about how this adventure went.'
taxonomy:
    category:
        - Backpacking
        - Fastpacking
    tag:
        - 'Trip Reports'
        - Fastpacking
        - 'North Carolina'
published: true
date: '23-03-2022 16:38'
---

![](/user/pages/03.blog/stone-to-doughton/map_stone-doughton.jpg?classes=caption " ")

Dates of this trip were June 25, 2021 to June 27, 2021.

[Stone Mountain State Park](https://www.ncparks.gov/stone-mountain-state-park/home?target=_blank) near Sparta, North Carolina was a place that I frequently visited as a kid. Growing up, we would go there for car camping and day hikes as a family. Seeing the exposed granite dome from which the park gets its name always inspired me and is a big part of why I developed an interest in the outdoors. 

Given that it was summer and in summer, you go camping (duh), my buddy Chris and I traded emails/texts about doing a trip. In our brainstorm, I remembered that the [Mountains-to-Sea Trail](https://mountainstoseatrail.org/?target=_blank) (MST) runs through the state park and it got me thinking about finding another set of trails that we could hook up to via the MST. After a quick look at [gaiagps.com](https://www.gaiagps.com/?target=_blank), I was pleased as punch to see that there is indeed another network of trails in the nearby [Doughton Park](https://www.blueridgeparkway.org/poi/doughton-park/?target=_blank) along the Blue Ridge Parkway. Could we hook Stone Mountain up to Doughton Park via the MST? By the fortune of our well spent tax money (and countless volunteer hours of trail maintenance), we could. Another question, could we fastpack it? As luck would have it, there are many features along the trail that support such an endeavor (more on that later). Now for the details about our plan.

## The Plan

Out here on the east coast of the United States, it's not typical to be able to string together a route that traverses between 2 parks which have camp-able areas. We don't really have the trail infrastructure aside from the Appalachian Trail to use as a means to hook entities together like you might find out in Colorado or in Washington state. When I called the park rangers and campground offices to secure the necessary permits and parking passes to make this adventure happen, I got a lot of "You want to do what?" and "Wait, you want to connect Stone Mountain to Doughton?" I'm guessing my inqueries were pretty unique. 

This unique route starts and finishes at the Widow's Creek trailhead in Stone Mountain State Park. There are established backpack-in campsites which require reservation via permit (available on a first come, first served basis at a kiosk at the trailhead) within about a mile of the parking lot. But why camp there when you can go up and over a few mountains and camp somewhere else completely outside of the state park? Our route uses the MST and ascends over 2000 ft. from Widow's Creek up to the Blue Ridge Parkway. It then traverses the ridge and hugs the Parkway until it reaches the border of Doughton Park and takes a turn southwest and descends about 2000 ft. via Cedar Ridge Trail. At the bottom of the descent, the route takes a turn to the northwest and briefly uses Grassy Gap Fire Road until Basin Cove Campground, an established campground maintained by the USFS which includes bear boxes for gear storage and fire grates (super convenient especially for a fastpacking oriented trip). Note: Basin Cove requires a permit which can be obtained by calling Doughton Park in advance of the trip. Night 1 is spent at Basin Cove. On day 2, the route steeply ascends 2000 ft back up to the Parkway via Bluff Ridge Primitive Trail and hooks back up with the MST. Upon hooking back up with the MST, the route takes a swing back to the northeast and heads across the ridge, eventually joining the section of MST which was used the previous day, forming a lollipop. The route eventually heads back down the mountain via the MST towards Widow's Creek but before reaching the main trailhead where the cars are parked, takes a turn to the west up the section of Widow's Creek Trail that connects to the backpack-in campsites. Night 2 is spent at one of these campsites. Day 3 uses Widow's Creek Trail for a short hike back out to the cars.

The fact that our plan was so uncommon made the logistics of getting permits needlessly complicated. Several phone calls could have been avoided if more information was included on the various websites. Another weird thing was that we had to reserve the backpack-in campsite in Stone Mountain State Park for 2 nights even though we'd only be staying there 1 night in order to ensure that we'd have a spot to stay for night 2. The campsite at Doughton Park was free, so I suppose it evened out? This was kind of clumsy and I wish there was a better way to do it. Then again, not many people do what we were planning on doing. But maybe more people should?

###Permits

A permit is required to camp inside of Stone Mountain State Park and can be obtained on a first come, first served basis via a kiosk located at the Widow's Creek trailhead. More info on camping in Stone Mountain is located [here](https://www.ncparks.gov/stone-mountain-state-park/camping?target=_blank). A permit is also required to camp in Doughton Park and that can be acquired by calling the District Ranger Office and contact info is available [here](https://www.nps.gov/blri/planyourvisit/doughton-park-trails.htm?target=_blank) in the Primitive Camping section of the page.

###ITENERARY &amp; MAP

We brought our GPX files exported from gaiagps into Google Earth, extracted distance and elevation data then dropped the numbers into a spreadsheet that calculates our itenerary based on a 2 mph pace. Then we printed the map and itenerary on either side of a sheet of [Terraslate paper](https://www.amazon.com/TerraSlate-Paper-Waterproof-Weatherproof-8-5x11-inch/dp/B00NWVGOF4/ref=sr_1_5?gclid=Cj0KCQjw5-WRBhCKARIsAAId9FleDZz88FiGTZzqAatqDYR5rRui2QeQAHsEsqODO6HojLv1WsENpLYaAlJCEALw_wcB&hvadid=178156556102&hvdev=c&hvlocphy=9008686&hvnetw=g&hvqmt=e&hvrand=10326909517435635587&hvtargid=kwd-160979461722&hydadcr=11342_9743660&keywords=terraslate%2Bpaper&qid=1647972338&sr=8-5&th=1?target=_blank). Below are the itenerary and map.

![](/user/pages/03.blog/stone-to-doughton/mts-062521-trip-details.png?classes=caption "Spreadsheet numbers are based on GPX data created in gaiagps then imported into Google Earth") 

![](/user/pages/03.blog/stone-to-doughton/stone-doughton-printout-1.jpg?classes=caption "Map printout.") 

## Day 1
### 14.54 MI | +3,556′ GAIN | -3,340′ LOSS

The start of this route is an easy gentle grade along Widow's Creek Trail but then quickly ascends upwards to the Parkway and has you wondering why Widow's Creek is called... Widow's Creek. As we picked up elevation, the vegetation changed and the air became dryer and dryer until finally we reached the top near Green Mountain. We hung a west (hung a west?)... Took a turn to the west towards Brinegar Cabin all the while taking in the amazing views along the ridge. The route crisscrosses the Parkway until it reaches Brinegar Cabin and the intersection with Cedar Ridge Trail. We turned onto Cedar Ridge and quickly descended the mountain to Grassy Gap Fire Road, hung a right (or hung a northwest) and meandered up towards Basin Cove Campground. Basin Cove was entirely vacant which was surprising given how nice it is. Maybe it's nice because it's vacant. Or maybe there's something we don't know. Either way... it was nice. And vacant. 

The best thing about this campground is the access to nearby creek and the bear boxes at each campsite. These provided peace of mind and eliminated the need to hang a bear bag. We could just shove everything in the bear box and call it a night. Badda bing badda boom. After setting up camp, sitting around for a bit enjoying the vacantness, we made our individual dinners and got the fire going. After sunset, we went for a bug hunt and found a spider on the larger side of the insect food chain because we like to go looking for insects that could digest our eyeballs right before bed. On that note, we called it a night and headed to our respective shelters.

![](/user/pages/03.blog/stone-to-doughton/day_1_IMG_6784.jpg?classes=caption "Parking lot selfie before the mayhem.")

![](/user/pages/03.blog/stone-to-doughton/day_1_IMG_6788.jpg?classes=caption "Heading up Widow's Creek Trail; the easy part. Photo by Chris.")

![](/user/pages/03.blog/stone-to-doughton/day_1_IMG_6791.jpg?classes=caption "Map check shortly before the part of the trail where you're almost at the top. Matchy matchy socks. Photo by Chris.")

![](/user/pages/03.blog/stone-to-doughton/day_1_IMG_6804.jpg?classes=caption "The part of the trail where you're at the top but not quite at the actual top. Photo by Chris.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_164653946.jpg?classes=caption "Amazing views off to the east.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_172114276.jpg?classes=caption "On the road again. Like a band of gypsies we go down the highway.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_172146105.jpg?classes=caption "Onward towards Brinegar Cabin.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_180156416.jpg?classes=caption "Stopped at Brinegar Cabin for a snack.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_180238729.jpg?classes=caption "Brinegar Cabin.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_201203611.jpg?classes=caption "Grassy Gap Fire Road just in front of Basin Cove Campground.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_211231278.jpg?classes=caption "El lugar donde acampamos.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_211253134.jpg?classes=caption "Mi tienda.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_214413277.MP.jpg?classes=caption "Aro de fuego.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210625_214926554.jpg?classes=caption "Caja de oso.")

(OK, stop with the Spanish captions.)

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210626_104342230.PORTRAIT.jpg?classes=caption "Boiling some water for dinner.")

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210626_004058902.MP.jpg?classes=caption "El hombre que es el jefe del fuego.")

(Sorry. Couldn't resist. _¡El fuego!_)

![](/user/pages/03.blog/stone-to-doughton/day_1_PXL_20210626_013029833.NIGHT.jpg?classes=caption "A spider about the size of my palm.")

## Day 2
### 15.09 MI | +4,164′ GAIN | -3,773′ LOSS

We woke up around sunrise and got the morning chores started. There was no need to go retrieve the bear bag because everything was tucked away in the convenient bear box nearby. Coffee was prepared, plans were discussed, camp was broken down, GPS watches were started, and finally we got on the trail. Our first waypoint was the 3 way intersection of Grassy Gap Fire Road, Bluff Ridge Primitive Trail, and Basin Creek Trail. Our route took Bluff Ridge Primitive Trail up to the MST. The elevation change was dramatic ascending nearly 2000 ft. in less than 3 miles. As we climbed, our conversation descended into an epic brainstorm session about a whole town where every business had the theme of poop. Businesses about doing "the business," as it were. Scoop and Poop, Home Depoo, Shirts'n Squirts, Continence Corral, The Boweling Alley, the Crack Stack... we just kept going. It kept us entertained as the brutal climb wore on. 

We finally topped out at the Bluff Mountain Shelter and not long after, joined back up with the MST. We hung a right and headed northeast along the MST until we hit the Bluffs Restaurant for a pit stop and some ice cream. Let me tell you, that was the best ice cream I've ever had. After enjoying the delightful treat, we got back to the task at hand of hiking along the ridge back to the Cedar Ridge Trail intersection, joining our route from the previous day and forming a lollipop. We then jogged our way along the ridge and eventually headed back down the mountain to Widow's Creek Trail and eventually, our campsite for night 2.

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_104854179.PORTRAIT.jpg?classes=caption "Coffee, and breakfast.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_105250212.MP.jpg?classes=caption "Coffee, breakfast, Scott and Chris.")

![](/user/pages/03.blog/stone-to-doughton/day_2_IMG_6851.jpg?classes=caption "The start of the brutal climb up the Primitive Trail and the start of our hilarious conversation. Photo by Chris.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_131332289.PORTRAIT.jpg?classes=caption "Oh it was strenuous alright.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_131717486.jpg?classes=caption "Bluff Mountain Shelter. You can't camp here but it would definitely be a good spot to wait out a storm.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_131952272.jpg?classes=caption "Eine schöne Aussicht.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_134619991.jpg?classes=caption "Laufen am Grat entlang.")

(Wait, now he's captioning in German? _Who is this guy?_)
    
![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_140221172.jpg?classes=caption "Ice cream!")

![](/user/pages/03.blog/stone-to-doughton/day_2_ice-cream-bluffs.jpg?classes=caption "2 happy dudes eating ice cream. Photo by Chris.")
    
![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_140221172.jpg?classes=caption "Another pic  of the ice cream! It deserves to be here twice.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_152022602.jpg?classes=caption "On the road again.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_161814182.jpg?classes=caption "Almost at Widow's Creek Trail.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_171931580.jpg?classes=caption "Final push to campsite number 2. Or was it 3? I am so exhausted!")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_173219274.jpg?classes=caption "Made it. Actually, I didn't make nuthin'! Legs are destroyed.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_184829014.jpg?classes=caption "My tarp.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_184906623_2.jpg?classes=caption "Chris's tarp/hammock.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_220208102.jpg?classes=caption "Getting dinner started.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210626_221138332.PORTRAIT.jpg?classes=caption "Ramen bomb! a.k.a. ramen and instant mashed potatoes.")

![](/user/pages/03.blog/stone-to-doughton/day_2_PXL_20210627_000704069.PORTRAIT.jpg?classes=caption "My tarp is lit.")

## Day 3 Part 1
### 1.61 MI | +49′ GAIN | -617′ LOSS

Before heading to bed, I recall us talking about how the hill behind our campsite would be the perfect place for a bear or some other creature to do recon on us while we were sitting around our campfire. Well, early in the morning, I heard some kind of creature slowly making its way down the hill. It did not sound like a small-ish creature... more like a medium-ish creature that warrented some hand clapping to scare it away. I clapped and it didn't seem to be startled. More like it just changed its course and bypassed our campsite for the next one, further down the hill. Crisis averted? Who knows. It was probably just a squirrel. A squirrel with red eyes and fangs. That weighed 100 lbs. And had terrifying claws and tentacles coming out of its back. Probably. Probably... yeah. We woke up around 6:15 and broke down camp, had breakfast, and discussed the day's short plan (and the mutant squirrel). The hike/jog out was a nice and easy downhill to the cars. I was in front so I got to break through all of the spiderwebs. You're welcome Chris.

![](/user/pages/03.blog/stone-to-doughton/day_3_PXL_20210627_095915797.NIGHT.jpg?classes=caption "Desayuno de campeones. Frühstück der Champions. Breakfast of champions.")

![](/user/pages/03.blog/stone-to-doughton/day_3_PXL_20210627_101955601.jpg?classes=caption "Mmm. Tasty spiderwebs.")

![](/user/pages/03.blog/stone-to-doughton/day_3_PXL_20210627_103708639.NIGHT.jpg?classes=caption "The cars are just around the next bend.")

## Day 3 Part 2
### 4.64 MI | +153′ GAIN | -74′ LOSS

As if backpack-jogging over 30 miles with over 7,000 ft of elevation gain wasn't enough, we just had to tack on a little run at the end. Chris was still going strong with his 10K streak and we needed to keep that streak going by adding on a bit of a run to complete his daily 10k. From the cars, we headed west on Long Bottom Rd. and jogged out to the gate then turned around and came back. My legs were toast.

## Final Thoughts

Thinking outside the box for this route definitely paid off big. Highlights from this trip included traversing the ridge between Stone Mountain State Park and Doughton Park via the Mountains-to-See Trail, camping at Basin Cove, and, of course, ice cream at the Bluffs Restaurant. The meadows and balds in the Bluffs Lodge area are pretty neat to experience and the views rival those of the Roan Highlands. It's a different world up there! Logistically speaking, I wish it was a little easier to string together this route but it's probably uncommon for people to park in Stone Mountain and leave for the night, then come back on the second night. It would be great if Stone Mountain had the option for overnight parking (paying a fee of some sort) without having to book a campsite for 2 nights. It should be noted that Doughton Park seemed to be a pretty popular area for day hikers at least in the vicinity of the Parkway. So if it's seclusion you're after, this probably isn't the best route. That being said, the infrastructure of trails, camping with access to water (and bear boxes), and restrooms/a restaurant all combine to make this a great area for fastpacking or fast-and-light adventures. [Find out what my buddy Chris had to say about this trip.](https://y2kemo.com/2021/07/backpacking-mst-stone-doughton/?target=_blank)

In the future, I can imagine a trip where we start in Doughton Park instead and head over to Stone Mountain for the day, perhaps even checking out the granite dome for which the park gets its name. All in all, this area has a ton to offer and I can't wait to go back!

![](/user/pages/03.blog/stone-to-doughton/day_3_PXL_20210627_114803404.jpg?classes=caption "Heading home after an awesome trip.")