---
title: 'Spy Rock'
subheading: 'After a few months off the trail, I was itching to get out again so I broke out the map and started looking for easily accessible places for a 1 night trip.'
header_image: 24509907437_cf24a13765_b.jpg
published: true
date: '15-05-2016 09:00'
publish_date: '15-05-2016 09:00'
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - Virginia
---

It had been a few months between backpacking trips. And for the best reason a man could possibly have—becoming a father. In October my wife and I became first-time parents, welcoming our son into the world on a beautiful fall day. Becoming parents in and of itself has been quite an adventure. And lucky for us, one full of smiles, joy and laughter. It’s an adventure that doesn’t come with a map or a compass. All of the baby books in the world can only provide a few ounces of preparation for what’s in store. But you learn as you go and do your best at every turn and things work out. The first 8 months have been a whirlwind of excitement, new smells… sleep deprivation, lots of laughter, some crying, and a new depth of love that I never knew existed. I can’t wait to see what’s in store for our little family unit. But that’s not the point of this post. We’re here to recap tales of adventure in the wilderness!

The last time I got out was our hike up to Apple Orchard Falls back in early September. After a few months off the trail, I was itching to get out again and after getting the green light from the misses, I broke out Nat’l Geographic map 789 and started looking for easily accessible places for a 1 night trip. My mind quickly went back to our last trip in that area in 2014 where our goal was to climb both Three Ridges and The Priest. We accomplished half of that mission, summiting Three Ridges but we weren’t able to get to the top of Priest. I got the bug to climb it and began brainstorming possible routes with Chris. We settled on a route that starts where the Appalachian Trail intersects Crabtree Falls Highway (VA Rt. 56), climbs up and over The Priest via the A.T. and out to Spy Rock then back the same way. Using Hillmap.com, I came up with a route of about 14.5 miles with close to 5,000 feet of elevation gain. This route I created on Hillmap didn’t take into account switchbacks and soon after we started, Chris and I both realized that Hillmap miles are not the same as real world miles.

### DAY 1
#### (9.64 miles | ~4,377′ of elevation gain | ~1,594′ of elevation loss)

We started at the trailhead where the A.T. crosses Crabtree Falls Highway, or VA Rt. 56. It was a cool morning and thinking that temps would warm up, we opted for shorts and t-shirts. Soon after our trailhead arrival we donned our packs, activated Airplane Mode, fired up our GPS watches and we were off. The route starts at the base of The Priest. You essentially climb the entire mountain right out of the gates. With fresh legs, bright eyed and bushy tailed, this initial climb was no problem… at first.

![](https://live.staticflickr.com/4587/24509908847_0db4a540d9_h.jpg?classes=caption,img-fluid 'The start of our hike.')

Contrary to what we had predicted with the weather, temps did not increase. As we drastically gained elevation, the mercury kept dropping and the wind speeds kept increasing. We could hear the loud woosh sound of trees being blasted by the wind overhead at higher elevations. As we ascended the trail, a solo backpacker heading the opposite direction wearing his rain shell with the hood fully drawn down to the sides of his face gave us a wry knowing smile. He nodded as he passed and was on his way down the mountain at a brisk pace. We were beginning to reconsider our choice of layers; or lack there of. One obvious way to stay warm was to increase our speed. We didn’t even have to say anything to know that we were on the same page about that. Chris took the lead blazing trail ahead of me… I could barely keep him in view.

![](https://live.staticflickr.com/4685/24509907437_9e7cad458b_h.jpg?classes=caption,img-fluid 'Our first views of the trip.')

![](https://live.staticflickr.com/4682/24509907287_86dd456d27_h.jpg?classes=caption,img-fluid 'Chris enjoying some elevation.')

![](https://live.staticflickr.com/4634/24509906627_768bd4b5b1_h.jpg?classes=caption,img-fluid 'Trying to keep him in view.')

Pretty soon we were at the top of Priest. We walked around at the summit and found a rock outcropping with some amazing views of the mountain range down below. The air quality was superb and we could see for miles around. With the wind cutting right through us we had to get back on the trail. Descending the back side of Priest, the wind wasn’t nearly as bad. We made our way to the gap at Crabtree Meadows and noted that there were no water sources from this point to Spy Rock. We knew this when we were planning our route but seeing the map on the information station at the gap further confirmed this fact. We would be rationing from that point on.

![](https://live.staticflickr.com/4590/24509905367_11d37e9b5e_h.jpg?classes=caption,img-fluid 'Top of The Priest!')

After the gap the trail rolled along the ridge and eventually climbed drastically up another set of contour lines to the famous Spy Rock. Our legs were jello after blasting up the Priest but there was only one way and that happened to be the opposite of down and an antonym to easy. Thankfully the uphill slog was short lived and ended with an amazing view atop Spy Rock.

![](https://live.staticflickr.com/4679/38666264424_77a64e90a4_h.jpg?classes=caption,img-fluid 'Spy Rock has 360 degree views.')

![](https://live.staticflickr.com/4738/24509903727_bac53869ec_h.jpg?classes=caption,img-fluid 'Looking west.')

![](https://live.staticflickr.com/4732/24509903937_11bb499d18_h.jpg?classes=caption,img-fluid 'Looking north.')

![](https://live.staticflickr.com/4686/24509901307_2945f577d6_h.jpg?classes=caption,img-fluid 'Looking back east at Three Ridges.')

After spending some time snapping photos and checking out the various rock formations, we decided it was time to find a camp site and headed back the way we came. We scoped out a spot along the trail earlier and figured we would head back there and set up camp if we couldn’t find anything on top of the Rock. Everything up top was exposed to the howling wind so it was a no brainer. This spot ended up being perfectly sheltered from the wind and already had a campfire ring built. We set everything up, got a fire going and cooked steaks and corn on the cob. It was time for some caveman TV and catching up.

![](https://live.staticflickr.com/4688/38666272844_2ab1673182_h.jpg?classes=caption,img-fluid 'My leanto plus bivy setup for the night.')

![](https://live.staticflickr.com/4687/24509902957_b7546fef75_h.jpg?classes=caption,img-fluid 'Hammock tarp setup.')

![](https://live.staticflickr.com/4731/38666282714_de1745cbd9_h.jpg?classes=caption,img-fluid 'Cooking steaks and corn on the cob.')

![](https://live.staticflickr.com/4680/38666276814_9a04c9331f_h.jpg?classes=caption,img-fluid 'Sunset and a campfire. Does not get much better.')

When it came time to hit the hay, stepping away from the fire brought on a cold chill that smacked us in the face. It was a harsh reality check that it would probably be a cold night. We turned off Airplane Mode and checked the forecast. Frost advisory. Fantastic!

### DAY 2
#### (8.26 miles | ~1,253′ of elevation gain | ~4,016′ of elevation loss)

After a night of tossing and turning trying to stay warm in my just-barely-the-right-rating sleeping bag for these conditions, I finally fell asleep a mere hour before I set my alarm to wake up. I think Chris had already broken camp by the time I was stirring. He said something about wearing his underwear on his head to keep warm.

![](https://live.staticflickr.com/4597/38666271594_2aa548a01f_h.jpg?classes=caption,img-fluid 'Coffee time.')

The second leg of the trip was the same as leg one except in reverse. We had already come down off of Spy Rock and after ascending Priest again, it would be a rapid descent back to the car. I thought about this while breaking down my tarp and packing up to keep my mind off of the brisk morning temps. The coffee helped too.

![](https://live.staticflickr.com/4692/38666269794_4e4d7795dc_h.jpg?classes=caption,img-fluid 'Getting an early start.')

![](https://live.staticflickr.com/4725/38496903805_ca66624dbc_h.jpg?classes=caption,img-fluid 'Sunrise from the A.T.')

After quickly breaking camp, we got on the trail and headed back the way we had come the previous day. The climb up Priest was a bit rough at first but knowing that it wouldn’t last too long soon helped me settle into a rhythm. I scarfed down a Bonk Breaker and put my head down to do some work. We stopped at the top of Priest near the shelter to filter some much needed water. The shelter was a mess and while I filtered my water, Chris took a few minutes to clean it up after the litter bugs who stayed there the previous night.

Once our bottles were topped off it was all business. We didn’t really stop until we got about halfway down the knee pounding descent of Priest. A brutal climb the day before became a relentless descent the day of. We stopped to let our brakes cool off and take a few gulps of water. I snapped a pic and again noted how amazing the views were that day with the cool crisp air, slowly warming up at this point.

![](https://live.staticflickr.com/4732/38666265564_f3d671ec52_h.jpg?classes=caption,img-fluid 'Just before the drop off after The Priest.')

![](https://live.staticflickr.com/4594/38666262884_a53a545a73_h.jpg?classes=caption,img-fluid 'Creek crossing back down in the valley.')

## Final Thoughts

The mission had been accomplished. We climbed Priest and made a bonus ascent of Spy Rock. Coming down the remainder of the descent was a breeze as we bragged to ourselves about our accomplishments. The trail began to level out and the elevation drop shallowed as we got back to familiar territory. We could hear the road and the parking lot. A minute later we were at the car taking off the packs and giving each other the ceremonial end-of-trip handshake. Another successful mission fulfilled.