---
title: '2022 Grindstone 100'
subheading: 'A few words and images to describe my experience at the 2022 edition of Grindstone 100. It was an awesome event put on by a top notch organization. I did not quite finish the race but that was not necessarily a bad thing. Read on to find out how it went.'
published: true
date: '03-10-2022 14:08'
taxonomy:
    category:
        - Running
    tag:
        - 'Race Reports'
        - 'Ultra Running'
        - Virginia
---

There are a lot of things to unravel about my experience at Grindstone 100. Or are there? Quite simply, I didn't finish. More broadly, I survived a 65.25-ish mile jog with 13,668 feet of gain and 13,763 feet of loss. There were a lot of little things which contributed to my DNF and 1 big thing which I will get into later.

Grindstone is very well organized and very well supported with a small army of logistical personnel, HAM radio operators and smiling volunteers along the 100 mile out-and-back course. These kind souls propelled us forward with their goodwill and warm smiles from aid station to aid station. I had the same impression running Mountain Masochist a few years prior. Clark Zealand and Eco-X put on outstanding events with top notch support!

In a nutshell, the Grindstone 100 is really hard, stays hard, continues to be hard, then gets harder. Was that a good description? No? Ok... here's some more thorough detail from the race website:

"Grit, endurance, temporary loss of sanity. You might need all of these if you want to attempt Grindstone. If you want to finish, well, just keep in mind this is, without a doubt, the hardest 100 miler east of the 100th meridian. Now that you're hooked, Grindstone is going to be an incredible adventure for each and every entrant. From the beautiful start/finish location at Camp Shenandoah to the monster climbs and the solitude of single track ridge running, you just can't beat Grindstone... but you can try! Come and join us for a wonderful event and experience what this 100 mile course has to offer you. It will be an experience you'll never forget!"

![](/user/pages/03.blog/2022-grindstone-100/Grindstone_ElevationMap.jpg?classes=caption "The elevation profile for the course shows lots of climbing.")

It was indeed, in a word, hard. _Are you not entertained?!_ Read on.


## Training

I committed to doing this race waaaay back in January of 2022 which was a long way away from the race date of September 16, 2022. Deciding early gave me a good buffer of time to build up a solid base of winter and spring running miles.

Winter training was composed of a mix of cycling (when I felt inspired and when it wasn't too cold, I rode around 25-40 miles per ride; mostly gravel and cycling paths) and trail running (about 12-15 miles per week). I started ramping up a bit in spring and experienced a recurring sacroiliac joint issue. I was able to get in to see my osteopath to receive a chiropractic adjustment and he prescribed physical therapy for 4 weeks. I found a really good PT who gave me a routine of exercises that focused on hip strength and mobility to deal with the S.I. joint issue which was caused by a muscle imbalance in my hip flexors.

My S.I. joint stayed in check all summer until I got a cold in early August. I took a week off (no running or any activity what-so-ever). Then eased back into activity the following week. But the time away seemed to throw the lower half of my body into a cycle of imbalance because I experienced some calf tightness trending towards calf strain and then 3 weeks from the race, my S.I. joint decided it didn't want to cooperate and generated muscle spasms in my glute and lower back. I was able to deal with the multiple issues by consulting my PT and seeing my osteopath who fit me in at the last minute but the cold + the calf issues + the S.I. joint issues really put a kink in my plan to have a final peak in my mileage before race day. This race would definitely be a "bring what ya brung" kind of an event for me. My mileage was not what I would have hoped, but whatevz. That's life.

![](/user/pages/03.blog/2022-grindstone-100/total_distance_garmin.jpg?classes=caption "My Garmin total mileage chart from the past year. Note the big dip in August due to my cold and injury prevention strategy.")

## Gear

This will get a bit long winded. So if you don't care about gear... 

### TL;DR

I wore various expensive running things and had various electronics and 1 headlamp, a backup headlamp, and a backup to the backup. The end.

## Long Version

If you _do_ care about gear, here you go.

Let's start at the ground level and move our way up, shall we? I trained mainly in La Sportiva Karacals. They were amazing shoes for winter running with their aggressive lugs and very sticky rubber outsoles. I really enjoyed running in them for shorter runs; I was super confident in mud, snow, and on slippery wet rocks. That said, on the longer runs for whatever reason, they just didn't work well for me. I had to keep them pretty tight so that they stayed secure against my high insteps and because of that they just got sort of uncomfortable for anything over 10 miles. YMMV.

As I started seriously ramping up, it became clear that I needed a shoe that could go the distance from a comfort standpoint but provide the same level of confidence that the Karacals did. After many hours of research, I had narrowed my choice of shoe down to the Hoka Speedgoat 5 and the Brooks Cascadia 16. I had the previous version of the Speedgoat before the Karacals and really liked them but they gave me blisters in strange places... like on the ends of my toes or on top of my big toe. WTF? So I decided to take the plunge on the Cascadias. I cruised on over to my local Fleet Feet and tried on a pair, really liked their fit, and made the purchase. As I racked up more training miles, it was clear that I made the right choice because they performed superbly. These were my shoes for race day.

![](/user/pages/03.blog/2022-grindstone-100/cascadias.jpg?classes=caption "Brooks Cascadia 16s and a pair of La Sportiva Karacals in the background.")

As far as worn items go, I used a pair of Balega Hidden Comfort no-show socks as a liner and a pair of Darn Tough lightweight crew socks on top. This combo worked well at preventing blisters, keeping my feet pretty dry, and providing a little extra cushion. For shorts, I have been running in the Path Projects Sykes shorts for a couple years now and really like them. This was my goto short for training runs and for race day. For underwear (sorry... yes, I'm going there) I used the Saxx Ultra Boxers. Super comfy and kept everything secure. For my shirt, I went with a Patagonia Capilene Cool Lightweight shirt. It wicked moisture extremely well and didn't chafe under my running vest.

Historically speaking, I had been running in the Scott Jurek Ultra Vest v.2 since 2011 (a.k.a. forever ago)... yeah, that's a long time. Parts of it were wearing out and different toggles had broken. It still functioned and I could have gotten by with it but I needed something with a bit more capacity and stretch. I did a ton of research and obsessed over it for weeks (just ask my wife!). I wanted to love the most recent vests in Ultimate Direction's lineup but they didn't have the right mix of features I was looking for. I finally decided to switch brands and go with the Salomon Adv Skin 12. It was an excellent vest for training and for race day and I don't know if I'll ever go back to UD for vests. I'm definitely a convert. Their fastpacks and handhelds are still great though.

As far as my weather shell was concerned, there was 0% chance of rain which meant that I could get away with leaving my waterproof at home and go with more of a wind shell. I chose my old reliable Patagonia Houdini. It is super light and sheds the wind incredibly well.

For electronics, I went with the Fenix HM50R headlamp because it takes rechargeable 16340 batteries and is waterproof. Light output is adequate for trail running and also backpacking. In hindsight, I wish I had gone with the HM65R-T because of the longer battery life to lumen output ratio. It is definitely overkill for backpacking though and I wanted something that could be multi-use. I still may cave and get the HM65R-T at some point if I decide to do another 100 mile event. But for now, the HM50R performs very well. I'm still rocking my Garmin Fenix 5 for GPS tracking. I had a pair of Aftershokz Headphones for music. Then my Google Pixel 4a. On top of that, I used a Inui 10,000mAh battery pack to recharge those items.

My list of miscellaneous items included a 100% waterproof dry bag which I used mainly for organization of the misc. items but also in case I needed a reliable, waterproof storage place to stash electronics in the off chance it rained. In that bag, I stored things like medication, vaseline, Body Glide, etc.

![](/user/pages/03.blog/2022-grindstone-100/flat_lay.jpg?classes=caption "Gear laid out just before I packed it up to get ready for the race.")

![](/user/pages/03.blog/2022-grindstone-100/electronics_flat_lay.jpg?classes=caption "Most of the electronics with the exception of my phone and my watch.")

Aaaaand we're done. Just kidding. Now, the moment we have all been waiting for...

## The Race

First: my expectations. This being my first attempt at 100 miles, I didn't really know what to expect. I knew it'd be hard because I'd never travelled more than 50 miles on my feet in a single push before. I also knew I'd experience sleep depravation like I've never experienced before. I also knew that I wanted to be safe and not make dumb decisions during this event. All of that being said, I had zero expectations. Maybe that was a bad thing and contributed to my DNF? Maybe I should have been less wishy washy and more absolute about it all. Or maybe it was a good thing because I oriented myself around making smart decisions and not getting to the finish at all costs.

On race day after arriving at Camp Shenandoah, I ate my packed lunch, checked in, then took a power nap. Around 4:00pm it was time to get to the dining hall for the pre-race meeting. The race director provided a run down of what to expect along the course. The last time I was in a room with this many people at the beginning of a race was at the 2019 Mountain Masochist put on by the same organization.

![](/user/pages/03.blog/2022-grindstone-100/pre-race-meeting.jpg?classes=caption "Pre-race meeting.")

The race started at 6:00pm Friday evening at the corner of the Dining Hall. The weather could not have been better; temps in the mid 70's at the start, low humidity with forecasted lows in the lower 50's at the higher elevations. 0% chance of rain. Perfecto racing conditions.

![](/user/pages/03.blog/2022-grindstone-100/1_header_22grindstone.jpg?classes=caption "Perfect conditions at the start!")

![](/user/pages/03.blog/2022-grindstone-100/hope_lake.jpg?classes=caption "Rounding Hope Lake just before the singletrack.")

We started as a massive pack of runners jogging through the scout camp. We crossed the dam below Hope Lake, ducked down below the dam, and waited in line to get onto a long stretch of single track trail that led to the first climb of the evening up and over Little North Mountain. It was a short punchy climb followed by a nice decent down to VA Rt. 42. As we dropped down into the valley, so did the sun and at Aid Station (AS) 1, the head lamp came out.

![](/user/pages/03.blog/2022-grindstone-100/descent_of_north_mountain.jpg?classes=caption "Descending double track to AS 1.")

![](/user/pages/03.blog/2022-grindstone-100/just_before_as_1.jpg?classes=caption "Crossing railroad tracks just before AS 1.")


After AS 1, the course follows a mix of gravel roads, doubletrack and singletrack trail all the way to top of Elliott Knob. The daylight progressively faded to nothing as we ascended the long climb (which isn't even the "long climb" of the race) up Elliott. Prior to reaching this climb, I heard chatter of other runners saying how much they hated the road that lead to the Elliott Knob Fire Tower. I quickly appreciated their sentiment when we reached this climb. It was certainly long and steep.

![](/user/pages/03.blog/2022-grindstone-100/ascending_elliott.jpg?classes=caption "Ascending Elliott Knob in the darkness.")

The trail leveled out and the fire tower emerged from the thick darkness. After briefly taking in the epic view of the stars at Elliott, I got back on the trail and headed down the mountain a bit to North Mountain trail and headed... well, north. This section was quite rocky and I noted that this would be tough on the way back. The woman running in front of me slipped multiple times and broke one of her trekking poles at one point along that section which was a bummer. We cautiously descended North Mountain trail to Dry Branch Gap at AS 2.

After a quick refill of flasks and a small bite to eat at AS 2, I continued onwards to climb Crawford Mountain. Again, this was a long and steady climb on mainly single track trail. Once at the top, the terrain was similar to the top of Elliott with lots of ankle breaker rocks to negotiate in the dark. I would jog a bit, walk a bit, jog a bit, walk a bit. My headlamp battery began to die around midnight which was expected. I stopped to switch out the battery on the final approach to US 250. After crossing 250, it was a quick bump over a small hill to Dowells Draft where AS 3 awaited. Approaching AS 3 I saw Christmas lights off in the distance and lots of commotion. It sounded like a big party and the volunteers greeted the runners with lots of loud cheers and assisted us in getting our flasks and bottles refilled. It was after midnight at this point and I was starting to feel the urge to take a nap. Must. Keep. Going. I was only at mile 20-ish. But still feeling good.

![](/user/pages/03.blog/2022-grindstone-100/dowells_draft_as.jpg?classes=caption "The big party at AS 3.")

Climbing Dowells Draft trail I knew I had the Wild Oak Trail to look forward to. My buddy Chris and I have backpacked that trail several times which would help out with my mental game. It wouldn't make it any less challenging though. At about mile 24, I started recognizing the territory and saw the Wild Oak Trail junction and proceeded into the darkness to climb Hankey Mountain, a mountain that I have climbed twice in the day time. Bucket list item: Climb Hankey Mountain at night... _check!_ After Hankey, the course follows the ridge over to Lookout Mountain and subsequently, AS 4. This aid station had the usual mix of good things to eat. I grabbed an orange slice, some Nutter Butters and some broth. It was tempting to stop but I had a conversation with my legs about continuing and I told them to keep going and they were like, "Okay."

![](/user/pages/03.blog/2022-grindstone-100/okay-chad.gif?classes=caption "Okay.")

My watch was about 2 miles off at this point. AS 4 is at 30.7 on the race mileage chart but my watch read 28.6. The fact that I was feeling good about being on familiar ground might have optimistically affected my pace a bit (as in, I was probably moving too fast) because I started having some minor stomach issues. Just the typical nausea one my expect to experience after jogging 30+ miles and gaining 6K feet of elevation on zero sleep. I felt that way off and on until North River Gap, a.k.a. AS 5.

AS 5 is a small haven in the woods with lots of buzzing activity. It was well organized and they had all kinds of amazing treats to eat and drink like bacon, tater tots, coffee, broth, all the good stuff. There was another runner, Wade who I had been chatting with just before the aid station and we kind of sat down together and started doing the math. You know... **ULTRA math!** We were on track for a 30-ish hour finish. I spent a good 20 minutes sitting there comfortably, sipping on broth and eating tots. Wade's crew was there and one of them offered to refill my broth and brought me some more tots. It was wonderful to sit down and my legs were enjoying their moment of zen. The time to leave the comfort of the aid station came and my legs struggled to find their will to move on. Below you will find a picture of my legs looking for their will.

![](/user/pages/03.blog/2022-grindstone-100/will.jpg?classes=caption "My legs looking for their will to continue.")

Time for a nap. Nah, just kidding! HAHAHAHA!

We got back on the trail at about 4:45am and began the longest climb of the race up Little Bald Knob. We chatted as we climbed and climbed as we chatted. My stomach issues were full blown nausea at this point... probably the bacon. No, it was the bacon + the coffee. No, bacon + coffee + Tailwind + peanut butter banana. Yeah, definitely that. I managed to keep it down though.

About halfway up Little Bald, we started talking about how we hadn't seen the leaders come through. Not 10 minutes later, the leader comes bounding down the mountain at full speed. He was a full 25 to 30 miles ahead of us. It always amazes me that there are humans who can run that fast on that kind of terrain.

Somewhere near the top, the sun came up. The nausea somehow subsided with the magic of daylight. After 2.5 hours of constant climbing, we were at the top. At dawn, it dawned on me (ha) that I hadn't peed in over 12 hours. This began to worry me a bit. Thinking about it must have triggered my brain to be like "Hey bladder! Yo, what's up?" I stopped by the trail to find a semi-private spot to pee, as one does, and it was a little dark. Gross. Again... worry time! At least the sun hath ariseneth.

![](/user/pages/03.blog/2022-grindstone-100/sunrise.jpg?classes=caption "Sunrise at the top of Little Bald Knob just before AS 6.")

"Shrug." I guess that's what happens when running 100 mile ultras? Your body starts to do some weird things. Stuff you never would have expected starts to happen. Like, I never chafed during training and one thing was for sure: I was beginning to chafe and it was becoming increasingly worse. The thing about chafing is that by the time you feel it, the damage has already been done and it will only get worse with continuous friction. So that was fun. Something else unexpected started happening; I was starting to see things... as in hallucinations.

![](/user/pages/03.blog/2022-grindstone-100/whatever-shrug.gif?classes=caption "Elmo doesn't know either.")

I was seeing skulls everywhere. Patches of moss on trees would look like a skulls, clusters of rocks would look like skulls, a leaf would look like a skull. I also started seeing electrical equipment off in the woods that wasn't there. I was on the ridge between the top of Little Bald and AS 6 and I was seeing electrical transformer boxes in the woods. I would look over, see it, then do a double take and it would be gone. I also saw Baby Shark in clusters of rocks. Yes... Baby Shark. Doubleyou Tea Eff. And the thing that really freaked me out was seeing a pile of rocks that looked like a dead body. It was like those stories you hear about from Mt. Everest where a climber might happen across a frozen corpse of a mountaineer that had just been left up there. I had to tell myself that this was a pile of rocks, this is an ultra marathon not Mt. Everest, and it was not a dead ultra runner. That's when I knew I had to seriously regroup mentally and consider my options.

Finally at 13:55/mile 44.9-ish... AS 6. I texted my wife to check in and texted my buddy Chris about my current hydration status. He suggested that I might be taking on too much sodium so I switched over to drinking just water instead of Tailwind + water. I grabbed a few morsels of food. We departed AS 6 after just a few minutes. I took off with Wade and a small group of other runners and they just slowly dropped me. I could tell was not recovering well after that climb up Little Bald as they just steadily pulled away.

The 6.6 mile section between AS 6 and AS 7 seemed to take forever but it was punctuated by a nice summit of Reddish Knob. This section was also peppered with the smiles of runners who would wave as they were making the return journey. This was encouraging to see. I had hoped to be at the summit of Reddish by sunrise but that wouldn't be the case today. The mid-morning views from up there were still pretty amazing.

![](/user/pages/03.blog/2022-grindstone-100/near_summit_of_reddish.jpg?classes=caption "The paved road up to the top of Reddish Knob.")

![](/user/pages/03.blog/2022-grindstone-100/reddish.jpg?classes=caption "The view from the top of Reddish Knob.")

After Reddish, the course descends a paved road down to AS 6, the turn around. I arrived as Wade and the other group of runners were about to head out. I gave them a wave and they headed back up the paved road while I grabbed some food, refilled my flasks and questioned my life choices.

I came to the realization after doing some **ULTRA math**, that I'd be in the 33-ish hour timeframe for completing this thing. That meant that I would spend a night number 2 out there. I'd be climbing the sketchy rocky sections of Crawford and Elliott in the darkness. After departing the aid station, I let that sink in for about 6.6 miles.

On the way to Aid 8, I had another couple of hallucinations. Same stuff. Skulls and electrical equipment. On a lighter note, my bodily functions were starting to get back to normal. Switching to water did the trick there. No more nausea and things were flowing at a more normal rate. After a brief water and pickle stop at AS 8, I had a decision to make.

I had cell reception up there so I called my wife. We chatted for a bit and I started to break down. I get emotional during these endurance events and this one was no picnic so, of course, I got emotional. I told her about my mental state and my safety concerns. The quote from Ed Viesturs kept coming to mind. "Getting to the top is optional, getting down is mandatory." Up until that point I had not experienced any kind of low point or strong desire to quit. I really wanted to keep going. I had turned my stomach and hydration issues around. The chafing was bad but I could deal. The hallucinations from sleep depravation were really worrying to me and I didn't want to make a mistake going up Crawford, down Crawford, then up Elliott, then down Elliott in the dark.

At the intersection of FS 47 and Wild Oak Trail, just before the trail descends Little Bald Knob, there is a small fire ring. I sat down at this fire ring and texted my wife. "I'm making the call now. Going to drop. I'm so bummed and upset but I think it's the right call." She responded with some very kind and encouraging words that reassured me. I got up and began the long 4 mile descent to North River which would be AS 9. On the way down, I saw another clump of rocks that looked like a dead body to me. I did yet another double take and it transformed from a dead body to just a pile of rocks. This final hallucination gave me confidence in my decision.

![](/user/pages/03.blog/2022-grindstone-100/descent_of_little_bald.jpg?classes=caption "The long descent off of Little Bald Knob.")

I pulled into Aid Station 9 at mile 65.5 with 21:55 on the clock. Almost 22 hours of continuous movement through the mountains of Virginia. My elevation gain/loss was 13,688ft/13,763ft. I let a volunteer know that I was dropping and she found me a ride back to the start/finish. I hopped in a Prius with 4 other runners who had also dropped and we all had similar stories of hallucinations and feeling like we were all having a good run up until a certain point. Perhaps there was something in our Tailwind! Or perhaps, it just wasn't our day.

## Final Thoughts

The Grindstone 100 mile race turned out to be a 100 kilometer event for me. It's the farthest I've ever carried myself on my own 2 feet in a single push and I'm pretty proud of that. The "me" of 10 years ago would have been super bummed and would have had a month long pity party about it. But the "me" of today is pretty ambivalent about it. I feel upset that I invested so much time and energy into this 1 event only to come up short but on the other hand, I'm proud of what I did accomplish and learned a lot of valuable lessons that I can apply to my next attempt at 100. I am still confident in my decision to drop given my mental state. I don't know that I will do another race that starts in the evening unless I'm confident I can get done in 24 to 26 hours. Other people probably felt fine that day or have built up coping mechanisms for the weird stuff that happens to you in a 100 mile push (particularly the sleep depravation). I'm just not there yet.

My one big take away from this is that it is a gift to be able to have adventures like these in the mountains. 

A huge thanks to the volunteers and race director for putting on this awesome event. And an even more huge thank you to my wife and family for putting up with my crazy training schedule and being OK with me doing these kinds of things. I don't know if I'll be back to run Grindstone again but I will definitely be back for some of the other Eco-X events. I am grateful for the opportunity I had to run this race, even with the DNF. 