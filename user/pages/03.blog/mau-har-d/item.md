---
title: 'Mau Har(d)'
subheading: 'The terrain reminded us of our old stomping grounds in Pisgah National Forest, North Carolina. Long, endless ascending through thick forest followed by rocky ridgeline cruising. Descend and repeat. Total climbfest.'
header_image: P1040607_sm.jpg
date: '04-11-2014 16:30'
publish_date: '04-11-2014 16:30'
visible: true
author: 'Scott Cooper'
unpublish_date: '27-01-2022 11:09'
published: true
taxonomy:
    category:
        - Backpacking
    tag:
        - Virginia
        - 'Trip Reports'
---

The plan was hatched. 2 big Blue Ridge summits in 1 day. My buddy Chris and I mapped out an 18.5 mile route starting at the Appalachian Trail junction with VA Rt. 56 year Tyro, VA that would take us up the AT, up Mau-Har Trail, back to the AT then to the summit of Three Ridges and The Priest respectively. Our legs would accumulate nearly 10,000 ft. of elevation gain over the course of the weekend’s trip. The terrain reminded us of our old stomping grounds in Pisgah National Forest, North Carolina. Long, endless ascending through thick forest followed by rocky ridgeline cruising. Descend and repeat. Total climbfest.

### DAY 1

At the start of the hike temps were in the mid-40s but it felt like the lower 30s with the steely cold wind whipping through the valley down Rt. 56. We donned our packs and headed to the footbridge that crosses the Tye River, northbound on the AT. Immersed in fall colors, we headed up the mountain into the Three Ridges Wilderness. I was amazed at the amount of orange and yellow that was still clinging to the trees this deep into fall.

After a short climb up the AT from the trailhead, we took a left turn on Mau-Har Trail and headed even farther up the mountain. The leaves were thicker on the this trail indicating that even fewer people had ventured out this way. It was almost a challenge to spot the trail in some places. We made our way up and over a ridge and then the trail wound down the mountain, eventually turning into a somewhat technical rock scramble next to Campbell Creek. It was here that we found a nice flat spot with multiple fire rings to set up camp for the evening. As a matter of principle, we brought meat for our first meal. Steaks and brats on night 1 are obligatory.

After our meal we sat by the fire exchanging the usual witty banter. We both noted that the wind was howling like crazy up above and were glad we chose to camp low in the gorge by a water source. The forecast called for 20 mph winds but it seemed like they were blowing faster than that up at the top of Three Ridges. We discussed the next day’s plan and headed to bed; Chris in his new hammock and me in my trusty MSR Hubba.

### DAY 2

The next morning we woke up with the sun, had coffee, breakfast and broke camp. We were on the trail by 7:30 am as planned the night before. Onward and upward. After a quick 1.25 miles, we arrived at the northern junction of Mau-Har and the AT. We paused for a quick text to our wives, “Proof of life,” as Chris calls it, and then began our climb on the shoulders of Three Ridges.

We made it to the summit around lunchtime. Our ascent was a mix of blasting cold wind on the eastern side of the mountain followed by calm, sunny conditions when the trail meandered over to the western side of the mountain. But at the top, there was no shelter from the wind except for in rhododendron bushes. We found a spot for a quick bite and some warm beverages. Tea for me, coffee for Chris. Then we discussed our plan for getting down the mountain and then up The Priest.

Coming down off the top, the descent was technical at times. We tried to go as quickly as we could because of our goal to reach Rt. 56 by 2 pm. After bounding down the mountain for a couple of hours, we made it to 56 where we had planned to resupply and fill up our water bottles at the car. The the shortage of daylight and just being utterly spent from our incredible hike, the plan shifted from a summit of The Priest to finding a good spot to set up for the evening. We discussed options and decided to make our way up the AT to Cripple Creek, about a mile into The Priest Wildnerness. We’d have water and potentially a good place to camp that was sheltered from the high winds. Well, there were no flat places and the creek was completely dry. OK. Plan C. We could hike back down the mountain a bit and see if we could find a fire ring somewhere. As luck would have it, we found a good spot about 200 yards from the car along the AT.

Night 2 was about the same as night 1 without the 3.5 lbs. of meat. We set up our shelters, collected wood, got a fire going and had some dehydrated backpacking food for dinner. We reflected back on our day’s hike with satisfaction.

### DAY 3

Day 3 was pretty self explanatory. Wake up, pack up, have some coffee and oatmeal and hike 200 yards to the car. The drive back to Roanoke gave me a chance to think about our trip. Even though we didn’t make it to the top of Priest, we had conquered one of western Virginia’s most challenging hikes. The Priest would have given us bonus points. As far as I’m concerned we accomplished our mission out in the hills.