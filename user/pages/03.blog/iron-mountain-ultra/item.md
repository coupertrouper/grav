---
title: 'Iron Mountain Ultra'
subheading: 'The trail network through the Iron Mountains of Virginia presents some pretty amazing route options and is very well maintained by a group of local volunteers.'
header_image: DSC_9968.jpg
date: '28-02-2018 09:00'
publish_date: '28-02-2018 09:00'
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - Virginia
---

*Note: if you stumbled across this page when looking for info about the Iron Mountain Ultra race, go here. If you're looking for info about a recent backpacking trip to the Iron Mountains in Virginia, read on.

## Our Route

After the holidays had come and gone, I was itching to get out and do some winter backpacking. I dropped a line out to the usual backpacking crew and got a couple of nibbles from a few and one solid bite from my buddy Chris. We normally try to do a February trip to get some snowshoeing in. Because winter trips take a while to plan, I threw the line out pretty early to gather interest and start the process. We had a few options on the table but settled on the Mount Rogers National Recreation area because it's reliably had snow in the past. Aside from a few cold snaps, this year's weather patterns weren't very snow friendly though. So we created 2 options for the route; one shorter option for snow and one longer option for no snow. At first, the shorter option was a simple out-and-back Mt. Rogers summit attempt and it later morphed into a short loop in the Iron Mountains, just north of Rogers. The long option consisted of a high mileage loop in the Iron Mountains using Iron Mountain Trail and Forest Road 84 as the main arteries. Because we had both done Rogers before, the unexplored (for us) territory to the north became more and more intriguing the more we planned.

We were both pretty jazzed about either option and as time progressed, it was looking less and less like we would have snow. As the week of the trip finally arrived, we looked at the forecast and saw no snow. It was bittersweet. No snowshoes this year but we'd have a really awesome loop to navigate. To plan the route, I used a combination of Google Earth and GaiaGPS to create the final map using their USFS layer. GaiaGPS has excellent map layers and a very simple, easy to use route builder. Once the route was built in Gaia, I imported it into Google Earth as a GPX to measure distances and elevation gain/loss by clicking and dragging within the elevation profile pane. We've got a spreadsheet we use that totals up mileage, gain and loss per day as well as start/stop times for each leg. Chris corroborated my mileage and we input the data. After plugging in the numbers, I combined it all into a PDF using Photoshop and printed the trip plan onto a couple of pieces of Terraslate waterproof paper. This paper is awesome and has made planning and navigating much more efficient than carrying the big National Geographic maps, though we do make sure one of us carries a big map as a backup.

![](https://live.staticflickr.com/4677/40403380471_14c7f9533a_h.jpg?classes=caption 'Getting it all packed.') {.center}

I coined the name Iron Mountain Marathon for the route because of it's approximate mileage of 27 (a marathon is 26.2 miles). Normally, for a trip like this, we'd do 3 days and 2 nights. But because of the high mileage, and because neither of us wanted to get up at 3am to make the drive, we decided to make it a 3 night trip starting Friday afternoon instead of Saturday morning. This would allow us to maximize our Saturday hike time and be rested instead of being toasted from waking up early and driving to the trailhead.

## DAY 1
### Mileage: 1.73 mi | Gain: +726 ft | Loss: -49′ ft

We started at the parking lot just before the Grindstone campground on Fairwood Rd. (SR 603). The route uses Flattop Trail to get to Cherry Tree shelter along Iron Mountain Trail. Looking at the forecast the night before, we already knew it was going to be a wet weekend. The parking lot was completely vacant when we both pulled in which meant 1 of 2 things. 1, the weather scared everyone away or 2, we were crazy for continuing with our plan to hike 27+ miles with such a soggy weekend forecast. 

![](https://live.staticflickr.com/4625/40358956862_b5dcc530ff_h.jpg?classes=caption "Squish squish.") {.center}

At any rate, it was game time. With the rain coming down, we donned our rain gear and packs then headed up Flattop Trail to get to the top of the mountain where we knew a dry shelter was waiting for us. All the more reason to get going.

![](https://live.staticflickr.com/4606/38592687660_ca324cb6f4_h.jpg?classes=caption "A rainy start to our hike.") {.center}

![](https://live.staticflickr.com/4627/39692678804_c526bb955c_h.jpg?classes=caption "Getting things set up at the shelter.") {.center}

The rain gradually let up and it turned from soggy to not-that-bad. When we got to the shelter, it was only drizzling so I was able to snap a few photos and then we gathered wood. Of course, everything was wet. We had planned accordingly, each bringing about 5 or 6 different methods for getting a fire started.

After gathering wood and assembling the magic pyramid of wood that was in a less than nominal state, we tried several of the fancy fire starting methods which included an SOL fire starter. While it did burn, it just didn't burn long or aggressively enough to get the wet wood burning. And so we went with the tried and true piece of fat wood and a cheapo fire starter stick for good measure. The fire was finally up and running after some TLC.

![](https://live.staticflickr.com/4764/39692682114_7fef9dae22_h.jpg?classes=caption "Nice warm fire after a cold, soggy hike to the shelter.") {.center}

We broke out our food for dinner. I brought 6 pork chops and Chris brought a couple of potatoes. Since they were wrapped in tinfoil, we just plopped them on the hot coals for cooking. We ate our dinners, consumed some spirits and then saw something white scurry through the shelter in the edge of our headlamp beams. After a few minutes, we saw it again and realized it as a mouse. Our original plan of spending the night in the shelter was thwarted and we both set up our tents and migrated our gear away from our new friend's house for fear of the mouse chewing holes in our gear. Luckily it wasn't raining at that point so we were able to keep stuff relatively dry during the move. After some more merriment and a game of survival cards ([read Chris's trip report for more on that](https://y2kemo.com/2018/02/backpacking-iron-mountain-trail-crocodiles-and-misdirection/?target=_blank)), we headed to our respective tents for the night.

## DAY 2
### Mileage: 12.27 miles | Gain: +1670 ft. | Loss: -2382 ft.

Day 2 started off early as we knew we had a long day of hiking ahead. We got up around 6:30 and started the process of breaking down camp. After gear was packed and tents were put away, Chris boiled some water for breakfast and coffee.

![](https://live.staticflickr.com/4663/25531790217_bf16b2f4e1_h.jpg?classes=caption "Trailside coffee is the best kind of coffee.") {.center}

We got on the trail around 8:30, following the IMT west towards Skulls Gap. Around Double Top, we lost the trail (and by we I mean "I" because I was in the lead) and followed a set of white ribbons tied to trees which I'm guessing were left there by a hunter to find his/her way back.

![](https://live.staticflickr.com/4614/25531789627_36838b2d24_h.jpg?classes=caption 'We explored off trail a bit.') {.center}

![](https://live.staticflickr.com/4753/38592685970_18b9d4c4ba_h.jpg?classes=caption 'Using them map and compass skillz.') {.center}

![](https://live.staticflickr.com/4602/38592685620_47fb6a55d1_h.jpg?classes=caption 'Found the road!') {.center}

We got that feeling of "we're not staying found," revaluated the map and determined we just needed to bushwhack directly south and we would hit FR84, which at that point was also IMT. After doing some map and compass navigation, found the road and were back in business and could proceed to Go to collect our metaphorical $200.

We cruised along the ridge of Sharp Top then arrived at Skulls Gap noting that some of the trees along the ridge looked pretty bad. Almost as if some kind of blight or beetle had begun killing trees. We looked around at Skulls Gap "picnic" area but there were no skulls nor were there any picnic tables. The area was also scant a fire ring which was bizarre considering the large tent icon on the map. Perhaps we just didn't look thoroughly enough. What we did find, however, was a 2 sided privy which had been overgrown with moss. After looking at each other with a "huh..." and a shrug, we proceeded on our way making the turn to the northwest along FR84.

![](https://live.staticflickr.com/4700/39692676174_e3e5f9e3ba_h.jpg?classes=caption "On the way down to Whitetop Rd.") {.center}

FR84 starts off gravel, descends to Whitetop Rd. where pavement takes over for a short stretch, then morphs back into gravel for the remainder of the road. After the pavement ended, I decided to take off my rain shell. Chris looked at me and said "You know what's about to happen, right?" The forecast called for 100% chance of rain later in the day. I grumbled, knowing that I might be cursing us to an early fate of more sogginess. My reasoning was sound though. We were about to begin a long slog uphill to Hurricane Gap.

After the first bit of climbing, it was misting just enough to be pleasant and not enough to need a shell. We kept pressing on up the mountain, passing Jerry's Creek trail and some other unnamed logging roads and tires. Lots and lots of tires. Apparently the Iron Mountains are where you go if you have tires you want to get rid of. We found Old FR84 and made the switch to begin yet more climbing to Hurricane Gap.

![](https://live.staticflickr.com/4754/39692676574_e534c89a0e_h.jpg?classes=caption "On our way to Hurricane Gap.") {.center}

Finally reaching the Gap, we checked the maps and again noticed a lack of fire rings. The plan was to either camp here or further down the road along Hurricane Creek. Because A) there was no water at the gap and B) there were no established campsites, we decided to press on to the creek. As we kept going we started to get a sinking feeling that there was not going to be a campsite and that we'd have to create one. It was only gravel forest road and lots of dense forest. But almost as soon as we approached the spot where we had dropped our pin in Google Earth, we found a turn-off with a fire ring. The backpacking gods were in our favor for the moment.

We found a place to set up our tents away from the fire ring and then gathered wood. As we were piling it all up, the rain started to fall.  Once the pyre was built, Chris walked up to me with the ace he had up his sleeve. A Coghlan's disk fire starter, a.k.a. The Hamburger. I attempted to open the plastic wrapped disk and powdery pink tinder exploded all over my hands and arms. Chris instructed me to simply light the whole thing and also informed me that it might be a good idea to brush off the pink stuff because I, myself, had also become tinder as a result of the flammable hamburger shards all over my extremities.

![](https://live.staticflickr.com/4702/40358956642_04772fd6ca_h.jpg?classes=caption "The rain just kept coming.") {.center}

The rain continued to fall as The Hamburger tried its very best to get going. It did but the wood did not. Time to break out the tarp to set up a group shelter to have a dry place to cook dinner. I worked on getting a tarp up and also learned about paracord management. Note the ridiculously long strand of paracord in the photo that Chris took above. Yeah, that was frustrating. I've since cut it down into more manageable sized lengths. Chris found a rock for me to sit on and rolled it into place. He broke out his Helinox chair and then we proceeded to sit under the tarp to cook dinner. After our meal, the cold started to sit in and we decided to call it an early night. 6:00pm and we were already in our tents for the night. Ain't nothin' wrong with catching up on some Z's in the rain.

![](https://live.staticflickr.com/4616/38592688780_b56a0c3faf_h.jpg?classes=caption "Early to bed.") {.center}

## DAY 3
### Mileage: 15.01 miles | Gain: +3,265 ft. | Loss: -2,448 ft

As per usual, we woke up around 6:30 and Chris snapped his fingers and his stuff magically disappeared into his pack. No, seriously. He is a magician when it comes to packing efficiently. I feel like a 2 year old bumbling about putting random objects into other random objects while he purposefully assembles his pack. While I caught up, we noticed that the sun was poking through the trees. Seeing the sun after a day and a half of wet conditions was a definite morale booster. We finished up camp breakdown tasks which included breakfast and coffee then got back on the route. while drinking our coffee, we noticed that the moisture on our tents had frozen. All we had to do to get it off was shake and brush off the frozen precip. No soggy rain flies to carry... double science bonus! Onward.

![](https://live.staticflickr.com/4669/40403388811_fafc9d81f1_h.jpg?classes=caption "Making some coffee and letting things dry out.") {.center}

![](https://live.staticflickr.com/4612/40403387741_24feae33dc_h.jpg?classes=caption "Two caffeinated blokes.") {.center}

![](https://live.staticflickr.com/4670/40403385911_6150096520_h.jpg?classes=caption "The sun!") {.center}

We hooked up with Trail 4530, headed up the hill to the AT intersection and stopped for a second, slightly confused by the sign at said intersection which came earlier than expected and didn't match what we were seeing with the route on Chris's Garmin watch. The sign indicated Hurricane Shelter was uphill and we had read somewhere that Raccoon Shelter had changed names to Hurricane. Knowing that Raccoon Shelter was actually off of our map to the north totally threw us off. Our USFS map didn't indicate a shelter in the direction the sign was pointing. We were confuzzled. And instead of following our guts (and our maps because the route had us going downhill), we decided to go with reason and headed uphill toward this fabled Hurricane Shelter. After about a quarter mile, I sent Chris a telepathic message straight to his cerebral cortex and he turned around shaking his head saying "This isn't right." Now his watch was saying we should have made the turn. We headed back downhill to where our maps said we should turn downhill and proceeded. The sign came up a whole quarter mile before we were expecting it and the AT is slightly confusing in this area because of the way it snakes around. And Hurricane Shelter? WTF. It somehow escaped our maps. We later found out the location of the mysterious shelter at a map on a kiosk.

![](https://live.staticflickr.com/4662/38592691190_6445c25a4c_h.jpg?classes=caption "THe old AT goes through a field of sleeping rock trolls. The cold never bothered them anyway.") {.center}

Back on the route, we crossed several streams and tiptoed through the field of sleeping rock trolls. Before we knew it, we had reached Hurricane Campground and also noted that we could have just saved ourselves the confusion and stayed on FR84. Oh well. We like rock trolls. And we also like adding miles onto our route.

We eventually found our way to the Virginia Highlands Horse Trail, the next leg of our route. The trail went straight up the mountain into the Raccoon Branch Wilderness Area. The terrain changed very rapidly as we increased elevation and as the trees thinned out, we saw a wood pecker watching us. I was able to snap a photo and then he flew away. Pushing onward, we both quieted down as the relentless climb wore on. Chris stepped on a stick and it let out a loud pop. "That was my will breaking," he exclaimed. It was a hard climb but we did make it to the top finally and then to the AT intersection.

Looking at the elevation profile, there was supposed to be yet another major climb but we never hit it. The route made its turn to the south west and downhill in the direction of the road. Now that we were on the AT and heading downhill, we were moving pretty quickly towards Comers Creek Falls. At the falls we snapped a few pics after crossing the bridge then proceeded up the trail.

![](https://live.staticflickr.com/4657/40403383221_026aa6337a_h.jpg?classes=caption "Comers Creek Falls.") {.center}

We had planned to camp in this area somewhere but the walls of the canyon were way too steep and neither of us had hammocks. And in the same fashion that night 2's campsite appeared, out of nowhere as hope was beginning to fade, a perfect campsite popped up right next to a picturesque waterfall with a perfectly made fire ring and a perfect pile of perfect firewood that seemed to be the perfect amount for the perfect fire. We'd have the perfect night of sleep, too, because of the perfect waterfall right behind the perfect campsite. It would be, well, perfect. Except for one thing. The weather was awesome and the next day's forecast was crap. AND we still had 1800 feet of elevation gain and 4 miles + change to get back to Cherry Tree to close the loop. Do we make the climb and close the loop with perfect weather or do we wait and potentially schlep it up the mountain in the soggy conditions? We sat down for some lunch to contemplate the decision at the perfect campsite.

![](https://live.staticflickr.com/4625/39506820865_31bc7e2de8_h.jpg?classes=caption "To infinity. And beyond!")

"We can do it," Chris said. I was enthusiastic and agreed but my legs were trying to tell me to call for help like Toby tells Edward to call for help because Gordon is stuck on the tracks in that episode of Thomas the Ta... oh never mind. If you have kids you get that life begins to be compartmentalized into lessons from children's TV shows when you're exhausted. We ~~begrudgingly~~ eagerly put on our packs and headed up the mountain, waving goodbye to the most awesome campsite ever. 

![](https://live.staticflickr.com/4743/40403382831_54e698014a_h.jpg?classes=caption "The view before the massive climb.") {.center}

My chipper attitude soon gave way to drudgery as the trail mercilessly gained elevation. Up and up it went until finally we hit the AT intersection. We went the wrong way down the AT at first, then reevaluated a couple of times. After deliberating with spent brains and tired lungs, we found the correct direction and eventually the intersection we had been looking for. 

![](https://live.staticflickr.com/4658/40403382591_04f9410418_h.jpg?classes=caption "A long section of ridgeline along IMT.") {.center}

We also noted that the trail continued to go up. I let out a sigh and Chris gave me a couple of his Clif Bloks then took the lead pulling me up the hill the rest of the way. I focused on breathing and summoning the strength to stay in his slipstream (and also forcing my mind into a state of denial about how much I was hurting). We got to the top then Chris turned on the after burners. We were huffing along at a brisk 3.5 mph along the ridge. The sooner we could get to the campsite at Cherry Tree, the sooner we could get these packs off.

![](https://live.staticflickr.com/4702/38592689280_c744836320_h.jpg?classes=caption "Finally back at Cherry Tree shelter.")

After quite an arduous climb and a fast cruise along the ridge, we made it to the forest road and then back on IMT and finally, Cherry Tree. The packs came off and we replenished water, got a fire going and then cooked dinner. We were so hungry that we both ate 2 Mountain House meals instead of the planned 1. We chatted for a while then sat by the fire as daylight faded. Along with it, our desire to stay awake. It was time for bed. We hung up our food bags and then hit the hey.

## DAY 4
### Mileage: 1.73 mi | Gain: +49 ft | Loss: -726 ft

When waking up on the fourth day, it was nice knowing that we didn't have much distance to cover to get back to the cars. We got up around 6:00 and started the morning routine. Both of Chris's timekeeping devices' batteries had died overnight so he was left with his internal clock while I was sitting pretty with my Suunto Ambit which, even after 3 days of use, had sixty-something percent battery life remaining. My alarm had yet to go off when I awoke to the sounds of Chris breaking down his tent. "You awake?" He asked. "Yeah... now I am," I replied. "What time is it?" He asked. "Not even six o'clock. Are you already breaking down camp?" I grumbled. "Yeah... I'm pretty much packed." His internal clock is apparently on Scotland time. At any rate, it was motivation for me to get myself going and packed. Plus he had already started brewing coffee. Major bonus!

![](https://live.staticflickr.com/4748/38592687870_4a5cad49ce_h.jpg?classes=caption "Foggy descent back to the parking lot.") {.center}

After snapping some final photos, we said our goodbyes to Cherry Tree Shelter and the mouse that inhabited it and headed down the trail to the cars. It was all down hill save for one short section along the gravel forest road. As we descended back down into the valley, we encountered a lifting fog. It was a great way to end an epic backpacking trip.

## FINAL THOUGHTS

The trail network through the Iron Mountains of Virginia presents some pretty amazing route options and is very well maintained by a group of local volunteers. The main feature, Iron Mountain Trail, is just an awesome trail to explore and I would really like to come back to this area to hike the southern section of it which descends down from Damascus. Views were often obstructed by trees which is something to keep in mind if the goal of the trip is to take pictures of sweeping vistas. Instead, this area presents some epic trails, dense forests, lots of creeks and some really interesting waterfalls. I imagine this area would be great for fall foliage because of this. It's also not quite as popular as the Grayson Highlands area just to the south and crowds may not be as heavy during peak backpacking months.

As far as this trip goes, the route name Iron Mountain Ultra definitely lives up to the "ultra" moniker. As Chris and I were chatting about distance, he pointed out that anything over 26.2 is technically an ultra. And so the route colloquially changed from Marathon to Ultra. We stuck to the plan and finished the route we mapped out, did our first 3 night trip and covered over 30 miles of trails that we hadn't explored before. And though we got off route a couple of times, we successfully used our map and compass skills to get back to where we needed to be. Overall, this trip was a success!

[Read Chris's account of our trip to the Iron Mountains here.](https://y2kemo.com/2018/02/backpacking-iron-mountain-trail-crocodiles-and-misdirection/?target=_blank)