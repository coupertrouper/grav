---
title: 'The Lion''s Plateau'
subheading: 'This year, our February trip found us in the Dolly Sods Wilderness in West Virginia. The trails are not blazed. And the wildlife is a little bigger than in most places on the east coast.'
header_image: lions_cover.jpg
date: '23-02-2014 16:00'
publish_date: '23-02-2014 16:00'
visible: true
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - 'West Virginia'
        - Winter
---

Every winter my buddy Chris, [who also created a write-up about this trip](https://y2kemo.com/2014/02/backpacking-dolly-sods-february/?target=_blank), and I try to plan a multi-day route someplace where we get to see the white stuff. This year, our February trip found us in the Dolly Sods Wilderness in West Virginia. The Sods lies on a high-altitude plateau featuring windswept plains, tundra like bogs, and an ecosystem not unlike those found in southern Canada. Backpacking in the DSW during the winter was very interesting and presented some different challenges than other places we’ve hiked. The weather can rapidly change from perfectly clear to whiteout conditions without warning. The trails are not blazed. And the wildlife is a little bigger than in most places on the east coast. More on that later.

### DAY 1

We accessed the Sods from Whitegrass Ski Touring Center taking Timberline Trail to Forest Rd. 80 which eventually became Rocky Ridge Trail leading us into the wilderness area. It’s tricky accessing the Sods in the winter. Forest Rd. 75 traverses the top of the plateau and can normally be used to access the area but in the winter, the road is closed because consistent snowfall makes it impassable. In my research I came across Whitegrass and sent Chip Chase a message. He immediately got back to me with suggestions for a possible place to start our route and snow conditions. As the trip date moved closer he followed up with a local weather forecast (Check out Whitegrass Ski Touring Center here.). In the forecast: a little snow, a lot of sunshine and not too cold. We were set. Chris shot some video of the conditions below.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345971/28247536029_132a4f125f_k.jpg?classes=caption 'We started out ascending Whitegrass trails then FR 75.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345972/40026454121_6b0656ec5c_k.jpg?classes=caption 'FR 75 eventually turns into a Rocky Ridge Trail.') {.center}

<iframe style="margin: 50px auto;"width="1136" height="639" src="https://www.youtube.com/embed/YVbAUA1wBzo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The forest road eventually peters out and turns into Rocky Ridge Trail which skirts the western edge of the wilderness area. The plan was to make a loop out of Rocky Ridge Trail to Blackbird Knob Trail to Raven Ridge Trail and back to Rocky Ridge Trail. There’s the planned route… and then there’s what actually happened.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345971/28247533399_0258e0d0a3_k.jpg?classes=caption 'Turns out snow melts when it gets warm.') {.center}

We made our turn east on Blackbird Knob Trail and what had been an easy and obvious snow covered path through pine forest and rhododendron bushes had become a knee deep rushing stream of ice cold water. “Is this the trail?” we asked. “Did we somehow mistaken a creek for the trail?” Using our maps and compasses, we determined that we were in fact on the trail but with the warmer weather that day, the snow had started to quickly melt filling up the trail with water. The brush was too thick on the sides of the “trail” to bushwhack and the drifts on the side of the flowing water had all but melted. We had no choice but to walk in the shin deep water. Since the stream of snowmelt just continued to get deeper, we made a decision to turn around and alter our route a bit. No sense in walking around with wet, frozen feet.

At this point it was a little later in the day so we headed back up to Rocky Ridge Trail and found a cache of firewood near an established campsite and decided to set up there. After cooking some dinner in the form of brats and kabobs we enjoyed some cigars and rum by a large campfire under a cold clear sky. We brainstormed a route for the next day. A decision was made to simply take Rocky Ridge Trail north to Raven Ridge and play it by ear. Lights went out around 11 or so.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345970/40026454971_dffbd9421d_k.jpg?classes=caption 'Set up camp in the trees.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345967/40026455311_5f0c978dca_k.jpg?classes=caption 'Cold and windy away from the campfire. Perfect next to the campfire.') {.center}

### DAY 2

The next day, we slept in a bit, had breakfast then headed north up Rocky Ridge Trail. At times we lost the trail because of the snow and relied on our compasses to navigate. Using Rocky Knob as our landmark, we eventually found the trail again and continued north towards the edge of the wilderness.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345970/28247532269_2dee27fb84_k.jpg?classes=caption 'Do you see a trail? Neither did we. Chris using his mad compass skillz.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345968/28247533859_47acf9f8c0_k.jpg?classes=caption 'Crusing along the frozen tundra.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345967/40026454181_98b6ce6c08_k.jpg?classes=caption 'Amazingly beautiful frozen marshlands.') {.center}

Approaching the boundary we began to see the Private Property signs for the power company’s Mount Storm power station. We took a turn east onto Raven Ridge Trail and after almost a mile, we found another place to set up camp. We collected firewood, boiled water, ate dinner, snapped pictures of the sunset and Chris fortified our position. After a couple more cigars, we agreed to break camp early the next morning to have plenty of time to get back to the car. Chris headed to bed and I stayed up for a bit letting my boots dry by the fire, still wet from our drudge through the stream the day before.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345964/40026454281_361b6c702d_k.jpg?classes=caption 'Trail signs kept us on course as we approached the boundary. Less snow too.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345967/28247532609_a38a86ccc0_k.jpg?classes=caption 'FIRE!') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345965/28247534819_c5c7e7e67c_k.jpg?classes=caption 'Enjoying the cold sunset near Raven Ridge trail.') {.center}

### DAY 3

7am the next morning we woke up, had some coffee and breakfast and then broke camp. We were expecting snow based on the forecast but it never came. We had a perfect blue sky for our hike out. It was cold enough the night before that a crust developed on the snow. Our snowshoes were able to glide across the top of the snow which in many places was quite deep.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345963/28247531419_e72055c0a9_k.jpg?classes=caption 'Deep crusty snow.') {.center}

We cruised at a fast 3 mph across the crusty snow and frozen tundra of the Sods and made short work of our 6.7 mile hike out of the wilderness. There were a few quick climbs up Rocky Knob and then up and over Rocky Ridge but for the most part, it was downhill.

Along the way, we saw fresh mountain lion tracks. The tracks followed the trail for a while and then scampered off east into the wilderness. Aside from another set of human tracks and the occasional ski grooves, the cat tracks were the only other sign of life up there. That weekend, the plateau definitely belonged to the large cat.

<div class="side-by-side">
    <div class="left">
    	<img src="https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_400/v1557345965/28247532279_e511801b75_k.jpg">
    </div>
    <div class="right">
    	<img src="https://res.cloudinary.com/wscottcooper/image/upload/c_scale,h_533,w_400/v1557345961/28247533629_c1ff2c16c8_k.jpg">
    </div>
</div>

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345965/39994995732_1b62ec9cb3_k.jpg?classes=caption 'Are you sure you really want to look in that cave?') {.center}

The large cat tracks gave us a little incentive to move quicker. Chris pushed the pace a bit and we made it back to the car in about 3 hours. Another successful winter trip in the books.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345964/39994995562_8721beb6b7_k.jpg?classes=caption 'Making haste, in fact, does not make waste. We had extra time for post-hike breakfast!') {.center}

## FINAL THOUGHTS

No matter what time of year you go, pack extra wool socks because your feet will get wet as many of the trails cross creeks (or become creeks in the late winter thaw). Be aware of the forecast. Always have your compass and map handy because there are no trail blazes. There are signs on the outer edges of the wilderness, but no colored blazes. Definitely check out the views from the top of Rocky Knob on the western side of the wilderness if you are in that area. If going in the winter and you don’t already have them, be sure to rent snowshoes from Whitegrass. And speaking of snow, keep your mileage goals realistic. Deeper snow means slower travel.

The Sods is a beautiful place to spend a weekend backpacking.

I hope to go back soon.