---
title: 'A New Sense of Cold'
subheading: 'We elected that it would be safer to bail out sooner and head down Rt. 261. And so, we ended up hitching a ride back into town with a nice family driving a warm Jeep. Our first DNF.'
date: '10-02-2013 09:00'
publish_date: '10-02-2013 09:00'
taxonomy:
    category:
        - Backpacking
    tag:
        - Winter
author: 'Scott Cooper'
---

We knew that this one would be cold. Unlike last winter, 2013 has been presenting much more seasonable temperatures. Our intended route covered the entire Roan Highlands Crest but we knew we might need to scale back some because of the snow. Luckily there are a few options up there. The planned route was a length of the Appalachian Trail that stretched from Iron Mountain Gap all the way down to Rt. 19E. The total distance was an expansive 29 miles with a lot of climbing and descending huge balds. I had done parts of this section of the Highlands the previous spring, summer and fall. So I knew the area pretty well and was confident I could navigate it if conditions turned sour.

### DAY 1

We parked at Mountain Harbour, a hiker hostel on 19E that offers shuttles to different points along the AT. We opted to park here and take a shuttle after hearing about car break-ins along 19E and at Iron Mountain Gap. After chatting for a bit with the friendly owners about the snowy conditions and watching the last bit of a Sharktopus episode, we hoisted our gear into their 4×4 and were on our way up to Iron Mountain Gap.

It took a good 45 minutes to get to our trail head. The freshly fallen snow called for cautious driving up the mountain switchbacks and through the higher elevation gaps. Our quiet driver, intently focused on the task at hand, handled it well.

When we got to Iron Mountain Gap and stepped out of the vehicle, the wind was ripping and it felt like it was about 20 degrees outside. I know that we were both reassessing our choice of clothing at that point. It was noon and it felt frigid. What was it going to feel like at 8pm?

Once we got on the trail, the snow depth ranged from about 8 inches to 2 feet in the drifts. The AT was fairly sheltered along this stretch and the wind that was buffeting us at the trail head had subsided and we were feeling pretty good.

We made our way up the AT along the spine of Iron Mountain. Ascending and descending the ridgetop, the snow got deeper and deeper as we gained elevation. When we reached Greasy Creek, the tracks we had been following scampered off down a side trail and we were left with virgin snow and a bit of route finding to figure out where the trail was in places. The AT skirts along the NC/TN border and sometimes, what looks like the trail is actually the fire road along the border. At any rate, the dial on the clock was approaching 4:30 and we needed to find a spot to camp. It just so happened that we were close to Clyde Smith shelter. We elected to set up shop there for the night.

Once we got our shelters set up, the priority became warmth. The temperature was starting to drop and we needed to get a fire going fast. We started gathering wood and twigs and leaves. The snow that had fallen the previous evening was a wet and heavy snow. A lot of the standing dead wood we collected was coated in snow and a bit wet. After amassing a pretty good pile of wood, we gave the fire our best shot. We could get a small twig fire going but couldn’t get the larger stuff to take. After 3 small twig fires and about 2 hours of relentless fire tending, we decided to give up and get in our sleeping bags. At this point it was about 20F outside and we were freezing our butts off.

It was an early night without any of the pre-planned activities of pork chops, cigars and whiskey by the fire. Our manly feast was thwarted by Mother Nature and we were instead cowering in our sleeping bags eating cold dehydrated rice and beans and drinking luke warm water. One hidden bonus of being confined to your sleeping bag and calling it an early night is the obvious large amount of sleep. I definitely got caught up in my cozy 0 degree bag.

DAY 2
Early to bed, early to rise. We got up before dawn. I think the stars were still out. One thing about setting up and breaking down camp when it is so cold out is that it seems to happen a lot quicker. Something about the cold makes you move more deliberately… perhaps that’s just because you’re trying to keep warm.

We got camp broken down, ate a quick bite of oatmeal and had a quick Via and were on the trail by 8. We made our way up to Little Rock Knob and took in the views. This is the first place where you can really take in how high this ridgeline is.

With the lack of food from the previous night and the constant climbing we were already feeling pretty thrashed and we had only just made it to Hughes Gap. The fact that our water was all frozen didn’t help much either. All of the water sources from Clyde Smith shelter on were frozen over. We were also pretty dehydrated. We stopped for a quick bite of food at Hughes Gap then began the slog up Roan High Knob. From Hughes Gap on it was just a brutal climb to the top.

As we got higher up the mountain, closer to Roan High Bluff, some snow started to roll in. We were so zonked at this point that we decided to take a break and re-evaluate the route. The way the AT is situated south of Roan Mountain, TN, we had the option to bail out on the Overmountain Trail and just hike into town and back to our cars. We took a break, pondered it a bit and decided to do just that.

The light and gentle snow eventually gave way to a bit of a snow storm. We experienced high winds and heavy blowing snow. Visibility became quite low at times but we kept trudging our way through the roaring wind and sideways precipitation. The camera had to go in the pack from this point on because the snow was getting wet and heavy. I was able to pull it out a couple more times to snap some quick pics but they don’t do the conditions justice. This was probably the most dramatic snow storm both of us had ever hiked through.

At this point we were starting to get worried about our water intake. We found another side trail that led to a spring but like all the others before it, this one was frozen over. Trudging back up the hill we decided to melt some snow with our stoves. The effort to melt the snow was almost not worth the few ounces of water we were able to drink.

After literally a sip of water each, we got our packs back on and proceeded onwards up the mountain. Switch back after switch back went by. The relentless climb went on forever and ever. The pace became begrudgingly slow as we lumbered up the mountain. But we eventually made it to a clearing in the trail and deliriously realized that we had made it to the top. We celebrated with a small nip of whiskey and then almost puked from our shear exhaustion. That was by far the closest I’ve ever come to loosing my (Clif bar) lunch on a backpacking trip.

From the clearing on, we cruised along the top of Roan High Bluff and then to Roan High Knob. We summited and then talked about re-evaluating the route again. There was no water up there and we were really dehydrated to the point where we weren’t processing liquids normally and the conditions only continued to worsen. We elected that it would be safer to bail out sooner and head down Rt. 261. And so, we ended up hitching a ride back into town with a nice family driving a warm Jeep.

It was a brutal and cold hike but not a loss by any means. We summited one of the tallest peaks on the east coast and traversed half of the entire Roan Highlands Crest in the snow. Definitely a feather in the cap. We will make another assault on this route when the weather warms up and tick off the full 29 miles.