---
title: 'Solo Autumn Overnight'
subheading: 'A speed hike would be fun and I was definitely trained up for it. But my brain needed the kind of hard refresh that only slow autumn miles on the trail can provide.'
header_image: solo-autumn-overnight.jpg
date: '29-10-2016 09:00'
publish_date: '29-10-2016 09:00'
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - Virginia
---

The Priest. Yes, The Priest will do. And maybe Spy Rock. I had slated the final weekend in October for a solo trip of some kind. I had been bouncing back and forth between a repeat of my Triple Crown loop or some kind of overnighter with the specific purpose of enjoying some of the fall colors. Speed hike or relaxed trip with no agenda? With the craziness of the past few months, I opted to for the latter. I needed a mental RAM dump. Something to reset all those internal processes back to zero. A speed hike would be fun and I was definitely trained up for it, having some of my fitness from the Blue Ridge Relay and the Star City Gran Fondo still in my legs and lungs. But my brain needed the kind of hard refresh that only autumn miles on the trail can provide.

I set off a little bit later than usual because of dad duties. Arriving at the trailhead around 1:00, I could immediately see that my idea of enjoying the fall colors was not entirely original. The parking lot was full and I had to park across the street in the small gravel overflow lot. I started at the trailhead where the A.T. crosses Crabtree Falls Highway, or VA Rt. 56. The trail immediately begins to scale the entire mountain without relenting until the very top. After about 2 miles of climbing, my initial brainstorm of making an attempt at going all the way out to Spy Rock began to fade. I resigned myself to simply summiting The Priest instead of pushing on to Spy Rock.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1557345455/30677837792_27e842fc77_k.jpg?classes=caption 'Heading into the Priest Wilderness.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,q_auto:good,w_1600/v1557345454/30794832935_b303601499_k.jpg?classes=caption 'First views after the monster climb.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,q_auto:best,w_1600/v1557345451/30758069736_8ee55de8e0_k.jpg?classes=caption 'More epic views from the summit.') {.center}

After some leaf peeping and photo snapping, I headed back over to the campsite for some dinner and a small fire. Sunset came quickly and before I knew it, the stars were out. Time to head back up to the overlook and attempt to capture some photos of the Milky Way. The air was crystal clear and there was no moon so the Milky Way could be seen with the naked eye. After an hour or so I headed back down to the tent for some shut eye.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,q_auto:best,w_1600/v1557345452/30794829265_5b1b75810c_k.jpg?classes=caption 'My campsite at a secret location just off of the trail near the summit of Priest.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,q_auto:best,w_1600/v1557345447/30794822005_ed80275dab_k.jpg?classes=caption 'Interesting vegetation near the summit.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,q_auto:best,w_1600/v1557345449/30493738490_19f4473723_k.jpg?classes=caption 'Small fire at an established fire ring.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,q_auto:best,w_1600/v1557345448/30159322253_3ab0c6019e_k.jpg?classes=caption 'Dinner is served!') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,q_auto:good,w_1600/v1557345443/30159316103_b9f68f4a6f_k.jpg?classes=caption 'Attempting to take photos of the stars.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345441/30758036986_23e579fa4f_k.jpg?classes=caption 'I suck at star photography.') {.center}

My alarm went off at 6:00. My brain slowly booted up and I began to break camp down as I groggily got myself together for the day. I stepped out of the tent and headed back up to the lookout to capture some sunrise images. The ridge ran east to west so I wasn’t at the best angle to capture the sun but the colors were vivid nonetheless. After snapping some photos, I made myself some Kuju Coffee then fully broke camp. I was on the trail headed back down to the car the way I had come.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,q_auto:good,w_1600/v1557345440/30493724730_4b7bba7829_k.jpg?classes=caption 'Sunrise from the summit of the Priest.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345442/30493714170_77196dade7_k.jpg?classes=caption 'Boiling water for coffee.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345441/30493716540_5353efb722_k.jpg?classes=caption 'Kuju pocket-pour-over coffee for the win.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345440/30159305393_256dd5fa77_k.jpg?classes=caption 'The pouches have built in arms that fold out and rest on the mugs rim.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345442/30493710530_422bd23b2f_k.jpg?classes=caption 'Time to head back down the mountain.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,q_auto:good,w_1600/v1557345437/30493701560_71babe1df3_k.jpg?classes=caption 'Final views just before the trail descends back down into the trees.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345437/30493694490_4ff53f8249_k.jpg?classes=caption 'Back down in the valley, on my way out of the woods.') {.center}

As it always turns out and with every trip, this autumn jaunt up a mountain is exactly what I needed. The Priest never disappoints, especially when the leaves are turning. It’s a challenging climb to the top with some of the most rewarding views in the area.