---
title: 'Training Log: Weeks 9-12'
media_order: training_wk9-12.jpg
subheading: 'Foiled Vacation, Hurricane Dorian, and Bronchitis'
header_image: training_wk9-12.jpg
published: false
---

## 8/26-9/22

## Goals:
Maintain balance. Get mental break during beach vacation. Start adding back to back runs to the training routine.

#### MONDAY
Rest and easy greenway hike | 2 mi.<br>
**Exercises & Stretching:** Stretch. Pushups (2x15). Toe/calf raises (3x20 of each).

#### TUESDAY
Easy greenway walk | 20:00<br>
**Exercises & Stretching:** Stretch. Pushups (2x15). Toe/calf raises (3x20 of each).

#### WEDNESDAY
Afterwork greenway run | 35:00 | 3.5 mi.<br>
**Exercises & Stretching:** Stretch. Pushups (2x15). Toe/calf raises (3x20 of each).

#### THURSDAY
Rest<br>
**Exercises & Stretching:** Stretch.

#### FRIDAY
Easy greenway run | 35:00 | 2.4 mi.<br>
Starting to feel a cold coming on.<br>
**Exercises & Stretching:** Stretch. Pushups (2x15). Toe/calf raises (3x20 of each).

#### SATURDAY
Rest & drive to Southport, NC. Which seemed semi-crazy considering Hurricane Dorian was churning in the Atlantic over the Bahamas with a projected course that tracked directly over the NC coast.<br>

#### SUNDAY
Definitely battling a cold or flu or something. Chose to rest instead of run. And watch the hurricane on The Weather Channel.

## The Wheels Came Off

Here's where the wheels came off of the train. I ran on Tuesday with Erin and felt surprisingly good considering how crappy I felt the day before. I ran an easy 4.25 mi. and didn't push the pace or anything. Just wanted to get out and run with my wife. The weather was awsome considering there was an impending hurricane. We had a great beach day and enjoyed the sun and sand as a family on Oak Island. The storm's path was more certain and it was projected to make landfall near Wilmington, NC. We had been following it all week knowing that we'd probably evacuate at some point as it approached. We made the decision to leave on Wednesday around lunchtime. Packing up, we could feel the humidity increase to a tropical level and saw the trees start to sway as the wind gradually increased. There was an ominous energy in the air and we felt like it was the right choice to head home. On the drive home, I developed a bad fever and the cough I had been battling all week only intensified. The rest of the week we had our staycation at home and made sure our almost-4-year-old felt like it was a special time even though we were at home. Meanwhile, my fever continued to hover around the low to mid-grade mark and I continued popping Tylenol every time it would get above 99. This continued into the next week and I finally went to the doctor and was diagnosed with acute bronchitis. Fun times. She put me on antibiotics as she diagnosed it as a bacterial infection. The meds slowly cleared it up and she cleared me to run that weekend. Basically, a 2 week break from running during the middle of my ultra running training plan. Would the rest be detrimental or beneficial? To be determined. I did manage to keep up my stretching and light exercises like calf/ankle raises through the duration of the bug. I also got a greenway hike in with the fam.

Would I call it the mental break we all needed? Nope. Definitely not. I did get to catch up on sleep. Albeit, in the midst of fight off a chest infection. We got some quality family time though, which I am grateful for.

## First Solid Week Back: 9/15-9/22
At last, no more fever. I can run again!

#### SUNDAY
Easy greenway run | 1:00 | 6.29 mi.<br>
**Exercises & Stretching Routine**

#### MONDAY
Easy greenway walk | 20:00<br>
**Exercises & Stretching Routine**

#### TUESDAY
Easy greenway run | 45:00 | 4.6 mi.<br>
**Exercises & Stretching Routine**

#### WEDNESDAY
Rest.<br>
**Exercises & Stretching Routine**

#### THURSDAY
Rest.<br>
**Exercises & Stretching Routine**

#### FRIDAY
Easy neighborhood A.M. run | 26:00 | 2.4 mi.<br>
Easy greenway hike | 2 mi.<br>
**Exercises & Stretching Routine**

#### SATURDAY
Rest.<br>
**Exercises & Stretching Routine**

#### SUNDAY
Mill Mountain run | 2:02:00 | 11.2 mi | 1108 ft. of gain<br>
Felt great! Energy level on the climb was good and made it to the top without stopping. Lungs felt fine with no residual coughing or gunk. Definitely not back to where I was before bronchitis but a lot stronger than I had anticipated I would be. The 2 weeks off of my legs felt good to. It was like a mega-taper right in the middle of training. Who knows what the science is behind that is but I imagine that a taper where you just do easy jogs in the midst of a big training plan might be beneficial. Anyway... onward and upward (hopefully)!

## Take Aways

Being sick sucks. But it put things into perspective. I'm grateful for a wife that will take care of me/the kids when I conk out on the couch with a fever. I'm also grateful that the hurricane spared my parents-in-law's house and neighborhood. And finally, I'm triple grateful that it happened early-ish in my training, giving me time to recover and gain back what I lost before race day in November. 

### 39 DAYS UNTIL RACE DAY!

