---
title: 'Wild Oak Trail Adventure'
media_order: wildoak.jpg
subheading: 'Beautiful terrain, physical challenge, and route planning logistics. The Wild Oak Trail presented the perfect mix for this weekend adventure that turned out to be sublime.'
header_image: wildoak.jpg
date: '10-11-2018 09:00'
publish_date: '10-11-2018 09:00'
visible: true
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - Virginia
---

Autumn is by far my favorite time to backpack in the southeast. The leaves changing colors, the cooler temperatures, and the beautiful sunsets in the Appalachian mountains are all hard to beat during this time of year. Any time is a good time to go backpacking but the fall is definitely special. Turns out my buddy Chris also knows this. And so does his bro-in-law Ken. Consequently, we assembled our team of 3 and for our destination, we chose to go to the Wild Oak Trail in the George Washington National Forest. This 27 mile loop offers amazing views from some of the more remote peaks in the national forest and covers terrain ranging from easy riverside trails, steep climbs, to ridgeline rollers that seem to go on forever. The trail system is very well maintained by the local mountain bike club and it's easy to see why mountain bikers, hunters, runners and backpackers come to this area to recreate.

More info about Wild Oak Trail can be found [here](https://en.wikipedia.org/wiki/Wild_Oak_Trail?target=_blank).

## Route

My buddy Chris summoned his route building skills and with a mix of using [Gaia](https://www.gaiagps.com?target=_blank), [Garmin Connect](https://connect.garmin.com/modern/course/25538622?target=_blank) and [AllTrails](https://www.alltrails.com/trail/us/virginia/wild-oak-loop?target=_blank), he came up with a 2 night, 30 mile route that followed the Wild Oak Trail with a spur at mile 23 that would go out and back to the reservoir to get water. One of the main logistical hurdles we had to overcome was finding (clean) water. Because the route consisted mainly of ridgeline hiking, there was virtually no water aside from the river crossing. This was confirmed after doing a ton of research and looking at various maps. For day 1 we agreed that camping at the river at mile 9.75 would be best. Getting there would be tough as we'd have a long day of climbing then descending Little Bald. But we'd have running water. Aside from the river, water was scarce according to research and maps. 

Because of this, Chris devised a plan to travel off of the ridge around mile 23 to go down to the Staunton Reservoir to camp near water. We didn't really know what to expect down in the valley because there isn't a ton of info aside from what's on maps. We had a worst case scenario mapped out which involved staying at the North River Campground but it meant that we'd be somewhat committed to taking a bailout trail along North River back to the car. At any rate, we had our plan and a bailout option mapped out in case things went pear shaped. Below is a pic of the map and here is a link to the route on [Garmin Connect](https://connect.garmin.com/modern/course/25538622?target=_blank).

![](https://live.staticflickr.com/65535/48139816361_454da70a82_o.png?classes=caption "The Wild Oak Trail route with a plan to dip down to the reservoir on day 2.")

## Gear

For this trip, I spent some time documenting my gear choices. I knew the weather would be cold and had to pack accordingly. I went with my Gregory Z45 to accommodate larger items like my 0° Mountain Hardwear Lamina sleeping bag, down puffy coat, and extra layers. Oh and potatos/brats. Here are some of the things that went into my pack.

![](https://live.staticflickr.com/65535/48058909901_69ba60e409_h.jpg?classes=caption,img-fluid "Some of the contents of my possibles pouch and fire kit.")

![](https://live.staticflickr.com/65535/48059007077_7cd9568454_h.jpg?classes=caption,img-fluid "Firekit with redundant materials for starting a fire.")

![](https://live.staticflickr.com/65535/48058961608_1b7c03f7e9_h.jpg?classes=caption,img-fluid "Admin stuff that goes into the brain of my pack. First aid, headlamp, TP, etc.")

![](https://live.staticflickr.com/65535/48058961963_287c359d67_h.jpg?classes=caption,img-fluid "All the things!")

![](https://live.staticflickr.com/65535/48059008987_153800b399_h.jpg?classes=caption,img-fluid "Danzig Baltic Porter from Devils Backbone. Good beer and packing go hand and hand.")

## Day 1
### 10.37 MI | +3,724′ GAIN | -2,979′ LOSS

The drive up to the Wild Oak trailhead was thankfully uneventful. Traffic was tame on I-81 and as we moved off of main roads  onto country roads, we got more and more excited to finally get on the trail after the long drive.

Upon arrival, we did the usual last minute gear prep, locked up the vehicle and got on the trail. Just before our grand depart, we chatted with a mountain biker who was doing the loop clockwise. We would be heading counter-clockwise around the loop. Little did we know that this trail system was very popular with mountain bikers and we would soon find out why.

![](https://live.staticflickr.com/65535/48058935558_d21a17fce3_h.jpg?classes=caption,img-fluid "Chris drove us to the trailhead.")

![](https://live.staticflickr.com/65535/48058982167_07bab569e8_h.jpg?classes=caption,img-fluid "Arriving at a reasonable hour.")

The trail meandered through the forest for a few hundred yards and then forked where the loop started. Right (northwest) to go counter-clockwise and left (south/southwest) to go clockwise. We took a right to begin our counter-clockwise trek. Soon after the fork, we began our 7 mile ascent of Little Bald. We completed the first pitch of uphill only to realize that we had a lot more uphill to go. Basically, the theme of this part of the story was "How to Go Uphill in as Short a Distance as Possible." Actually, it wasn't short. 7 miles is a long friggin' climb! The elevation profile consisted of a lot of short steep sections smattered between long and slow grades up the mountain. We couldn't complain though. No. We're not complaining. But our legs did complain just a little. OK, a lot.

![](https://y2kemo.com/wp-content/uploads/2018/11/wo-skidmore.jpg?classes=caption,img-fluid "So much climbing! Photo credit: Chris Dunst")

All of that being said, the weather was amazing. It was a cold, clear and crisp morning complete with gorgeous leaf color just past peak orange. All we had to do was look up and it wasn't so bad. Choosing to look at the bright side of things, we eventually made our way to the first view of the route and it was definitely worth the amount of work we had just put in. Going into this trip, we knew that there would be a lot of elevation gain but we were equally anticipating some awesome ridgeline views.

![](https://live.staticflickr.com/65535/48058936878_7b14fd9c4b_h.jpg?classes=caption,img-fluid "After almost no warmup, the trail went straight up to the top of the ridge.")

![](https://live.staticflickr.com/65535/48058938588_16d3085b90_h.jpg?classes=caption,img-fluid "No complaints about the views though.")

Continuing with the theme of "How Can We Add Yet More Climbing?" We proceeded higher up the ridgeline and eventually stopped for a snack near Groom's Ridge. We regrouped after getting strung out on the trail then stuffed our faces with Clif bars and other random sports nuturition goodies. Then sobered up to the fact that we would have to push pretty hard to get to our planned destination by sundown. AND we had yet more climbing to do to get to the top of Little Bald. Additionally, there are no shortcuts to the top. We said "onward!" Our legs relunctantly followed the order.

![](https://live.staticflickr.com/65535/48058939668_5b313f903d_h.jpg?classes=caption,img-fluid "And more climbing.")

![](https://live.staticflickr.com/65535/48058890386_19ea1f47a1_h.jpg?classes=caption,img-fluid "Leaf color in full force.")

![](https://live.staticflickr.com/65535/48058941453_e32e79ed3e_h.jpg?classes=caption,img-fluid "Ridgeline cruising for a while.")

As we pushed up the mountain, we noticed the wind and cold increasing. On the windward side of the ridge, it was cold and blustery and we needed our shell layers and gloves. On the non-windward side of the ridge, it was quiet and layers were shed. After this back and forth a couple of times, and more elevation gain, we finally made it to the top of Little Bald. I went out to explore a pond that was noted on a map I had seen online. The pond was located at the end of a long and straight jeep road. After looking at the pond once, I deduced that I would definitely not use it as a water source. Worst case scenario, I would walk down off of the ridge to the river before using that nasty water. I was also hopefull that I would find some good views but none were found. I probably would have found them if I had kept walking but we needed to keep making progress towards our campsite.

![](https://live.staticflickr.com/65535/48058892096_fd106746e3_h.jpg?classes=caption,img-fluid "I would not use this as a water source.")

After my mini recon mission, Ken had caught up and we got back to the task at hand. Time to get down off of the mountain and proceed to Camp Todd, our planned campsite for night 1. It was about a 3 mile descent with a ton of elevation loss. 

As we were making our way down the ridge, I saw the mountain biker we had bumped into at the trailhead coming the opposite direction. First, I thought "Wow, he's a mutant for being able to get through almost the entire loop already today." Second, I thought "That long climb up Little Bald would be an amazing descent on a mountain bike. Hole. E. Cow."

It was at about that moment that we wished we, too, had mountain bikes. The route took us 2000 feet back down into the valley where it met up with the river. The hike down off of the ridge was not the most pleasant experience for our feet but in many ways, it was better than going uphill. We finally made it to the bottom and just before the river there was a fork. Going left would be shorter but would require a river crossing. Going right would be longer but we'd be able to use a bridge along FR95 to cross the river. Given the cold temps, we opted to go the longer route to avoid dangerously cold and wet feet even though it meant that we would have less time to set up camp. 

![](https://live.staticflickr.com/65535/48058892631_2609e1ad2e_h.jpg?classes=caption,img-fluid "Shadows getting long as the sun began to go down. Time to get to our campsite.")

![](https://live.staticflickr.com/65535/48058943303_f4332feeee_h.jpg?classes=caption,img-fluid "Descending TWOT back down into the valley where our campsite awaited.")

Happy to have warm and dry feet, we approached Camp Todd and started searching for campsites. Just after the bridge, we found a dispersed roadside campsite complete with a fire ring and a ton of dry wood. Given that daylight was scant, we committed to this campsite and started setting up. After quickly pitching tents and hammocks, we got the fire going, and dinner cooking. Temps were dropping fast and it was time to get warm and cook those brats and potatoes that we had just toted 10-ish miles up and over a mountain.

![](https://live.staticflickr.com/65535/48058945048_798b1662e3_h.jpg?classes=caption,img-fluid "Get that fire going! It was super cold.")

![](https://live.staticflickr.com/65535/48058944408_cee7ba5279_h.jpg?classes=caption,img-fluid "Baked potatoes. Yum. Not picured... Brats. Even more yum.")

## Day 2
### 15.32 MI | +3,445′ GAIN | -3,967′ LOSS

If day 1 wasn't hard enough, day 2 was set to be tougher with a planned 3000 feet of elevation gain and a whopping 13.5 miles of walking. There would be a lot of up and down and not a lot of daylight to do it. We awoke at sunrise, got coffee and breakfast going while breaking camp. It. Was. Cold. The forecasted lows were in the upper 20s and it turns out meteorologists are generally correct. At some point, Chris finally stirred as Ken and I were finishing up our final tasks of breaking down our tents and getting packed up. He got up and began his routine and pretty soon, all of our bags were packed and it was time to get back on the trail to begin our doosy of a hike. 

![](https://live.staticflickr.com/65535/48058895696_f16c922627_h.jpg?classes=caption,img-fluid "Coffee time!")

![](https://live.staticflickr.com/65535/48058945968_93e3fe124e_h.jpg?classes=caption,img-fluid "Our campsite on the morning of day 2.")

Things started off on FR95 and after a short distance, we passed Camp Todd and thought, "Huh, this would have been a way better place to spend the night." "Hey, look at that nice pre-cut pile of logs over there!" Oh well. We quickly found the Wild Oak trail and said bye to Camp Todd. Time for more uphill. We began our ascent of Big Bald and once a the top, we discovered that the views weren't that great and were mainly obstructed by trees. I did manage to snap a single photo.

![](https://live.staticflickr.com/65535/48058897181_0cd44cd4b8_h.jpg?classes=caption,img-fluid "At the top of Big Bald.")

We descneded down the trail from Big Bald towards Braley Pond Rd. At an intersection with a nasty looking pond, and Gordon's Peak trail, we hung a left to continue on Wild Oak trail and proceed downhill to the road. We crossed the road and then braced ourselves for another doosy of a climb. Our plan indicated that we would hit an intersection with Dowell's Draft trail about 2.5 miles from the road. However, we hit an intersection as well as a steep incline after only 1.75 miles of hiking. The sign seemed to indicate that Wild Oak went straight up the incline and Dowell's Draft went to the right, around the incline. Confuzzled, we opted to go up rather than around because the sign said "Wild Oak". How bad could the incline be, right? It turns out it was pretty bad. It was so steep that what looked like the top was actually a false summit. I don't remember how many false summits there were but the climb went on forever and the grade did not let up until the top. After this crazy climb, which I would not want to repeat doing with a 40 lb. backpack, we stopped for a breather. Chris waited on me, then Chris and I waited on Ken while Chris played the Rocky theme on his iPhone while Ken made his way towards us. 

![](https://live.staticflickr.com/65535/48058947588_92dfac636e_h.jpg?classes=caption,img-fluid "We took a blazed shortcut to the top without realizing it because we like to do things the hard way.")

![](https://live.staticflickr.com/65535/48058948033_6883b65e70_h.jpg?classes=caption,img-fluid "Taking a breather at the top. That was tough.")

After another short bit of hiking, we came to a set of switchbacks which rewarded us an awesome view at the top. A group of mountain bikers passed us like it was no big deal to negotiate the rocky, technical trail. We took a quick break for a snack and some photos then got back to hiking.

![](https://live.staticflickr.com/65535/48058948528_b86b4f99d2_h.jpg?classes=caption,img-fluid "Ken making his way to the top of one of the big climbs along TWOT.")

The day was starting to get away from us and it was time to think about reevaluating our plan. Ultimately, we decided to use our originally planned route of hiking down off of the mountain at Lookout Mountain but instead of trying to find a campsite at the reservoir, continue on to North River Campground and camp there. Then hike out using North River Gorge trail instead of trying to hike back up the long and arduous climb of Lookout Mountain on day 3. Our legs were already toast and trying to do that would not be good.

![](https://live.staticflickr.com/65535/48058899791_16a3834555_h.jpg?classes=caption,img-fluid "Electing to go down the forest service road in search of a trail that would take us down to the reservoir. We never found said trail.")

![](https://live.staticflickr.com/65535/48058997552_9e6d006869_h.jpg?classes=caption,img-fluid "Committed, we had to camp at the car camping campground at the bottom. At least there was water and plenty of firewood.")

On our way down from Lookout, we looked for that spur trail that we had intended to use but never found it. As we descended in elevation, so did the sun. The daylight was beginning to fade and so were our spirits as it was looking like we'd be setting up in the dark. After rounding a switchback along the gravel road, we could hear the sounds of car doors slamming down below and looking through the trees, we saw the forest road. Letting out a sigh of relief, we knew that we were almost there. With a few minutes of daylight to spare, we found the campground, promptly paid the campsite fee, and quickly began setting up shop for the night. Bonus: there were bathrooms with pit toilets. Soon after setting up camp, we got a fire going in the provided fire ring and dinner started. The campsite we chose was right next to the North River and we got to fall asleep to the sounds of rushing water. Day 2, done.

## Day 3
### 5.98 MI | +141′ GAIN | -335′ LOSS

We awoke to slightly overcast skies and cold temps in the 20s around 6:30am. After some breakfast and coffee, we started studying the map and realized that we'd have at least 7 river crossings along North River Gorge trail to get out (we ended up crossing 8 times). Fun fun! Especially considering the cold ambient temperature. We all began expressing our hopes that the creek would be passable. Otherwise we'd have to hike all the way back up Lookout Mountain.

![](https://live.staticflickr.com/65535/48058953158_825247aa71_h.jpg?classes=caption,img-fluid "Cold morning at the campground.")

![](https://live.staticflickr.com/65535/48058999837_befd8b9e83_h.jpg?classes=caption,img-fluid "Hiking along the gravel forest service road FR95.")

![](https://live.staticflickr.com/65535/48059000702_7ea58a3939_h.jpg?classes=caption,img-fluid "The first of 8 frigid creek crossings.")

The first crossing was the worst and the word "frigid" doesn't quite do it justice. It was the kind of cold that goes straight to your brain and makes you go cross eyed. Seriously. I started to have a headache by the end of the first creek crossing and could feel my pupils dilating. Definitely not something I would want to do at sunset and I kept thinking about night 1 and being glad that we had opted for the bridge instead of the river ford. Here's a video that Chris shot of me crossing.

<iframe style="margin: 50px auto;"width="1136" height="639" src="https://www.youtube.com/embed/V1BKT8iNbSs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](https://live.staticflickr.com/65535/48058955313_9562fa6338_h.jpg?classes=caption,img-fluid "Cold water!")

The consecutive creek crossings weren't as bad as the first in terms of temperature. We had gotten used to it and, in a strange way, it was a nice anti-inflammatory psuedo ice bath for our legs. Therapeutic after 2 days of strenuous ridgeline hiking.

![](https://live.staticflickr.com/65535/48058955798_e0f558fe0d_h.jpg?classes=caption,img-fluid "Getting the boots back on after carrying them. We did this for every crossing.")

![](https://live.staticflickr.com/65535/48059004137_969ecf2393_h.jpg?classes=caption,img-fluid "Final creek crossing.")

After the final crossing, we started to see more and more signs of human activity. More foot prints and more mountain bike tracks in the mud usually means that a trailhead is nearby. We saw the proper TWOT off to our right which we would have descended, had we stuck to the original plan and to our left, the suspension bridge we had been anticipating. After snapping a few pics of the bridge, we crossed and headed up the trail to the top of the gorge. Nearing the top, the trail meandered through the forest a bit and we heard a few cars pass by on FR95 which meant that we had almost closed the loop. After a few hundred yards, we arrived at the intersection where the loop starts and ends. Yards turned into feet and feet into the sweet relief of dropping our packs at the car. Mission accomplished.

![](https://live.staticflickr.com/65535/48058908331_26b5db6ca3_h.jpg?classes=caption,img-fluid "Suspension bridge that led to the end of our big TWOT loop.")

## Final Thoughts

The route we completed presented the perfect mix of beautiful terrain and physical challenge. As fall tends to be a dryer time of year, we were also faced with the unique challenge of planning our campsites according to where we could reliably find water. I can't yet speak to the availability of water in other seasons, but sourcing water during the fall ended up being the crux issue of this route and ultimately caused us to deviate from the plan. Though we didn't complete TWOT in its entirety, we still ended up finishing the loop in adventurous style (thankfully the creek crossings were manageable) and had a great time overall. TWOT is such a fun trail and the purist within me wants to come back to complete the entire loop. We will be back!

Chris prepared a write-up about this trip, too. [Check it out here.](https://y2kemo.com/2018/11/backpacking-wild-oak-steep-ascents-and-river-crossings/?target=_blank) He's _way_ funnier than me.