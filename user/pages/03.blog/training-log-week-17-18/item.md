---
title: 'Training Log: Week 17-18'
subheading: 'The final stretch before race day.'
header_image: training_wkfinal.jpg
published: false
date: '01-11-2019 16:00'
publish_date: '01-11-2019 16:00'
---

## 10/20-11/1

## Goals:
Get one final big week in and have a smooth taper. And stay healthy!

#### MONDAY
Rest/easy greenway hike | ~ 2 mi.<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### TUESDAY
Easy pace evening runs | 40-45:00 | ~ 4.5 mi.<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### WEDNESDAY
Rest and lunch hikes | 20-35:00 | 1-3.5 mi.<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### THURSDAY
Rest<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### FRIDAY
Rest or easy greenway run | 30-45:00 | 3-4.5 mi.<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### SATURDAY
Rest day.

#### SUNDAY
Long run day.

## Big Run Highlight

My final big run consisted of a huge 18 mile loop in the George Washington Nat'l Forest with tons of gravel road running and some ridgeline cruising along the Appalachian Trail. I wanted to get on some terrain that closely resembled what I would be dealing with on race day and this loop had it all.

## Taper

Time for that sweet taper. All those miles in the legs and feeling confident about race day! Now, I just have to avoid getting sick and pay attention to every single step I take so as to not tweak anything before the race.