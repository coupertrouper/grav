---
title: 'Training Log: Week 7'
subheading: 'Strive to maintain balance and build on fitness gains.'
header_image: training_wk7.jpg
published: false
visible: true
---

## 8/12-8/18

## Goals:
Keep up the progress. Keep up the balance of training/family/work/life. Focus on getting more sleep and eating well. Continue with upper body strength exercises.

#### MONDAY
Rest. Greenway hike.<br>
**Exercises & Stretching:** Stretch.

#### TUESDAY
Easy greenway run | 32:00 | 3.4 mi.<br>
Burpees after run | 3 x 10<br>
**Exercises & Stretching:** Stretch. Pushups. Toe raises. Calf raises.

#### WEDNESDAY
Rest<br>
Easy greenway hike | 22:00<br>
**Exercises & Stretching:** Stretch. Pushups. Toe raises. Calf raises.

#### THURSDAY
Rest<br>
Easy greenway hike | 23:00<br>
**Exercises & Stretching:** Stretch.

#### FRIDAY
Easy greenway run | 20:00<br>
**Exercises & Stretching:** Toe raises. Calf raises. Stretch.

#### SATURDAY
Rest.<br>
**Dinner:** Tortilla soup with shredded chicken.

#### SUNDAY
Big run | 11.75 mi. | 1128 ft. of gain<br>

## Take Aways

#### Total Mileage: 17.15 mi.

Coming off of the good news about my ankle, I felt really positive about my runs this past week. I managed to fit 3 runs in and even though 1 workout was only 20 minutes, I still felt good about it because it was time on my feet. Got some Sole inserts for my shoes and had the doc heat mold them to my feet. I did my first big run in them and they seemed to help support my feet and I felt more stable overall. New blister spots though... so that's fun. Just need to stay the course and make small corrections as I go.