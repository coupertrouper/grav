---
title: 'Roan Highlands Recon'
subheading: 'The intent was to do some reconnaissance of the Roan Highlands to check out routes for winter backpacking. And to spend some time catching up with backpacking buddies.'
header_image: P1020902_wp.jpg
date: '05-11-2011 16:30'
publish_date: '05-11-2011 16:30'
visible: true
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - 'North Carolina'
---

The intent was to do some reconnaissance of the Roan Highlands to check out routes for winter backpacking. And to spend some time catching up with backpacking buddies. I always like to get to know an area in the warmer months before it gets coated in white for the winter. November had thus far been quite mild. We parked at the end of Roaring Creek Rd off of NC 19E and stuck to our regularly scheduled start time of 9:00 am-ish. I had to do a bit of research to find this trailhead. There aren’t very many articles out there about this area. The majority of hikers seem to park at Carvers Gap and follow the AT to Grassy Ridge Bald, our eventual destination. But I wanted to do something different than the crowds. After some digging, I found a route that would take us from the end of Roaring Creek Rd up to the AT and then south towards Carvers Gap. We would be heading the opposite way of the crowds, giving us a little more solitude and a bigger selection of campsites. In theory.

![](https://live.staticflickr.com/6111/6330377645_72a281752f_b.jpg?classes=caption,img-fluid "We followed the Overmountain Victory trail.")

![](https://live.staticflickr.com/6118/6330377869_ddd9329c48_b.jpg?classes=caption,img-fluid "Mike and Jen smiling before the climb up to Grassy Ridge.")

![](https://live.staticflickr.com/6033/6331131252_cd994df958_b.jpg?classes=caption,img-fluid "As we ascended, we were treated to more views of the landscape down below.")

We were optimistically hoping to see a little snow–which we did. Once we got to the balds, there was a slight trace of snow from the previous week. Again, being that it was so warm, it didn’t stick around for very long. Not quite the epic snowfall of the previous winter but it was still nice to have that small bit of it to tease us for the upcoming months.

![](https://live.staticflickr.com/6031/6330378837_f2c8ebcd5c_b.jpg?classes=caption,img-fluid "We could see Grandfather Mountain from the gap at the top of Roaring Creek road.")

![](https://live.staticflickr.com/6094/6331132308_d2376eb6ef_b.jpg?classes=caption,img-fluid "Not as much snow as we had hoped but the views were still amazing.")

![](https://live.staticflickr.com/6217/6331132506_c399cac22a_b.jpg?classes=caption,img-fluid "Probably should not eat these.")

![](https://live.staticflickr.com/6114/6331132722_293110d677_b.jpg?classes=caption,img-fluid "Tent set up ready to enjoy the sunset.")

The hike was fairly straightforward. A simple route with a moderate amount of elevation gain. We hiked all the way to Grassy Ridge Bald and elected to set up camp and then scout around the ridge. Partly because it was approaching dinner time and partly because we simply wanted to soak in the amazing views. It was a gorgeous blue-bird day without a cloud in the sky. We could see Mt. Mitchell and the Black Mountains off to the southwest and the Tennessee Valley off to the north.

![](https://live.staticflickr.com/6055/6331132928_49d4450250_b.jpg?classes=caption,img-fluid "Amazing sunset view from the campsite.")

![](https://live.staticflickr.com/6091/6330380211_739c36fdb5_b.jpg?classes=caption,img-fluid "Dinner time!")

![](https://live.staticflickr.com/6113/6331133508_fc36e54152_b.jpg?classes=caption,img-fluid "Sitting by the fire, enjoying our dehydrated backpacking meals.")

The simple, not-too-ambitious route made for a nice relaxing vibe. We just sat around the campfire telling stories and drinking wine for the rest of the evening. The goal was to check the area out, after all. Not to cover lots of miles. Which meant that we could really relax and be observant.

![](https://live.staticflickr.com/6093/6330380809_1b61b6a9af_b.jpg?classes=caption,img-fluid "The Outdoor Research Nighthaven lit up.")

![](https://live.staticflickr.com/6094/6330380975_fdfcd15c97_b.jpg?classes=caption,img-fluid "Looking back down towards Johnson City, TN.")

![](https://live.staticflickr.com/6031/6324281686_4cf5a40efc_b.jpg?classes=caption,img-fluid "Light trails leading back to the tent.")

We awoke to an amazing sunrise. Looking out over the cliffs there was an endless blanket of soft white clouds filling the valleys below. The perfect spot to have a couple of mugs of coffee and just recharge. This is the reason I go to places like this. It doesn’t really get much better than an alpine sunrise chased by the warmth of several cups of joe.

![](https://live.staticflickr.com/6118/6330381059_9d3a18350c_b.jpg?classes=caption,img-fluid "Gorgeous sunrise about to happen.")

![](https://live.staticflickr.com/6110/6331134646_318ef716f4_b.jpg?classes=caption,img-fluid "The view of the sunset was one of the best I had ever seen up there.")

![](https://live.staticflickr.com/6050/6331134850_511133424a_b.jpg?classes=caption,img-fluid "Early morning at the campsite.")

We broke camp and hiked down to Carvers Gap, crossing over Jane and Round Balds and then turned around and headed back the opposite direction on the AT back to the trailhead at Roaring Creek Rd. All in all, a great little 1 night weekend trip with friends. I can’t wait to go back and explore this area in the winter.