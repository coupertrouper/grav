---
title: 'Altered Course'
subheading: 'The lessons from past winter trips still hold true this year. Plan out your route then basically cut it in half if there''s over a foot of snow on the ground.'
header_image: altered-course.jpg
date: '27-02-2015 09:00'
publish_date: '27-02-2015 09:00'
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - Virginia
        - Winter
---

The lessons from past winter trips still hold true this year. Plan out your route then basically cut it in half if there's over a foot of snow on the ground. Chris and I decided on Grayson Highlands for our annual February backpacking trip which is known to have dramatic weather changes all year round. Here is Chris's account of the adventure. We planned out our route about a month before the trip with the caveat that we would adapt the route at game-time based on the conditions.

## THE PLAN

We always tend to be ambitious with our route and this trip was no exception. Our "Plan A" route was to start at the Grindstone campground and head up Mt. Rogers trail to Deep Gap, then head northbound on the AT all the way around Mt. Rogers and across SR 603 to Iron Mountain trail to Flat Top, then Flat Top back to the cars. The distance of this loop would be a grand total of 24.8 miles. That's a lot. Especially for winter. We'd soon find out just how ambitious and perhaps unrealistic that plan was.

### DAY 1

When we arrived at our meeting spot, the ground was covered in over a foot of fresh snow and we couldn't even get our cars parked. Chris tried but got his car stuck and I had to push him out. Plan A would have to take a back seat. We then drove back down SR 603 to the only trailhead where we could find a place to park which was Fox Creek Horse Camp. The gravel loop, normally used for horse trailers, had been driven on and somewhat cleared so we parked there and came up with a Plan B.

Our new plan formulated into taking the AT southbound as far as we could go for one day's hike, then playing it by ear from where we would end up setting up camp. We donned our packs and locked up our cars, snapped a ceremonious selfie and got on the trail. From the very beginning of this trip, we were hiking on virgin snow. It was a pretty cool feeling knowing we were the only humans out there and conditions, aside from having to change up parking because of too much snow, could not have been better. Next time, I'll be bringing my large snow shovel if there's snow in the forecast to clear out a place to park.

![](https://live.staticflickr.com/4686/27597462509_6b7ca82f0e_c.jpg?classes=caption,img-fluid "After deciding our plan of action we started our hike.")

![](https://live.staticflickr.com/4644/27597464509_2621c4c674_c.jpg?classes=caption,img-fluid "We made it to Thomas Knob Shelter and decided to press on.")

After a tough slog through the snow up the mountain along the AT, daylight was starting to become scarce so we decided that we should probably begin looking for a place to camp. We were on a slope so it would be a challenge. I was happy that I brought my mountaineering snow shovel because it allowed us to dig out tent platforms in the slope. It's a cumbersome thing to carry but has been worth it every time on trips where there is snow.

![](https://live.staticflickr.com/4732/27597464429_87e4ea90db_c.jpg?classes=caption,img-fluid "The long slog up towards the ridge.")

![](https://live.staticflickr.com/4692/24509723547_8a819a89af_c.jpg?classes=caption,img-fluid "Map check to weigh our options.")

![](https://live.staticflickr.com/4594/24509718767_07c42e1f02_c.jpg?classes=caption,img-fluid "Decided to set up camp because we were running out of daylight.")

We found a decent spot, got our campsite set up and started a fire. We opted to bring brats and potatoes as our meal for night 1. I got a fire going and Chris cooked dinner. After our not-so-ultralight meal, we discussed the next day's plans and then headed to bed.

### DAY 2

We woke up early and got camp broken down efficiently. Our plan was to hike up to the ridge, check out the views and then head down Lewis Fork Trail to make eventually make our way back to the cars. The conditions were perfect at the top. It was a sunny blue sky day and we had the whole place to ourselves. We could not have asked for better backpacking weather.

![](https://live.staticflickr.com/4639/27597464239_5f1bc5faff_c.jpg?classes=caption,img-fluid "Great weather at the start of our hike.")

![](https://live.staticflickr.com/4599/27597463939_9a92052853_c.jpg?classes=caption,img-fluid "Making our way up to the ridge on fresh legs.")

![](https://live.staticflickr.com/4601/27597463779_02c729ee74_c.jpg?classes=caption,img-fluid "We made it to the ridge where the amazing views are.")

Day 2 was awesome. The views at the top of the ridge are what keep me coming back to this area. During winter when air pollution isn't bad, you can see for miles. We cruised around a bit along the ridge then headed back down the mountain to find a campsite along Lewis Fork trail. 

![](https://live.staticflickr.com/4645/27597459589_22bf879c57_c.jpg?classes=caption,img-fluid "The views just keep me coming back.")

![](https://live.staticflickr.com/4682/27597463609_b3ff352739_c.jpg?classes=caption,img-fluid "The snow makes the landscape feel more epic.")

![](https://live.staticflickr.com/4595/24509722627_52387a96f5_c.jpg?classes=caption,img-fluid "We could see the summit of Mount Rogers off in the distance.")

The descent was easy with the fresh snow to cushion our steps. We glided along the trail and then stopped for water when we reached the creek. After a snack, we got back on the trail and found a nice place to set up camp across from the creek.

![](https://live.staticflickr.com/4687/27597462219_f065c67d12_c.jpg?classes=caption,img-fluid "Making our way back down Cliffside trail.")

![](https://live.staticflickr.com/4727/24509721517_8c94359d23_c.jpg?classes=caption,img-fluid "Filtering some water from the creek.")

![](https://live.staticflickr.com/4732/27597458999_5e11f8c4d8_c.jpg?classes=caption,img-fluid "Setting up camp for the night.")

![](https://live.staticflickr.com/4679/27597463449_654e639b5b_c.jpg?classes=caption,img-fluid "Our campsite is ready.")

![](https://live.staticflickr.com/4728/24509722347_711d92e33b_c.jpg?classes=caption,img-fluid "Firewood is ready too.")

![](https://live.staticflickr.com/4636/27597461749_538e712ecd_c.jpg?classes=caption,img-fluid "Enjoying a warm fire on a cold winter night.")

![](https://live.staticflickr.com/4593/24509718457_1cd51f2918_c.jpg?classes=caption,img-fluid "Planning our hike out.")

### DAY 3

Our hike back to the cars would be easy because we camped only a short distance from SR 603. We got up and got some water boiling for coffee and breakfast. Breakdown was as quick and we got on the trail just before the sun came up. The hike was almost entirely down hill so we just cruised along the trail with no trouble. Again, we couldn't believe that we were the only ones out there. Even when we approached the road, we didn't see any other human tracks in the snow.

![](https://live.staticflickr.com/4693/27597463259_5b3724d30c_c.jpg?classes=caption,img-fluid "On the trail by first light.")

![](https://live.staticflickr.com/4732/27597459289_b67d7237f8_c.jpg?classes=caption,img-fluid "Looking back the way we came.")

![](https://live.staticflickr.com/4691/27597462989_98d738bfbe_c.jpg?classes=caption,img-fluid "Crossing a stream along the horse trail.")

## FINAL THOUGHTS

The route may have changed and we may not have gotten the distance we wanted, but it was a fantastic trip none-the-less. The Mount Rogers National Recreation Area never disappoints and every time I make a visit to the area, it's a great experience. The trails are well maintained, there are a ton of route options and in winter, there has consistently been snow and the trails are perfect for snowshoeing. We will definitely be coming back to this area again next winter!

![](https://live.staticflickr.com/4592/38496730515_2e2db518cf_c.jpg?classes=caption,img-fluid "The Mount Rogers National Recreation Area never disappoints. Especially when there is snow on the ground.")