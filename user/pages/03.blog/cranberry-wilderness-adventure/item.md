---
title: 'Cranberry Wilderness'
subheading: 'Exploring new territory. Braving the cold. Embracing the silence. Total wilderness immersion. 3 days scouting through the Cranberry Wilderness did not disappoint.'
miles: '3'
header_image: cranberry_title.jpg
published: true
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - 'West Virginia'
        - Winter
visible: true
date: '11-02-2020 15:00'
publish_date: '11-02-2020 15:00'
---

Let me start off by saying you shouldn't go to the Cranberry Wilderness. It's totally boring and would make a horrible destination for anyone looking to get outdoors for a weekend. If it pops up on your radar as a possible place to go for a backpacking trip, take a pass. Just don't do it. Not worth the drive.

## Actually, Do Go

Now that that's out of the way, let me say that I am just kidding. While I feel like I shouldn't be posting about such a magical place because I hope that it stays, well, magical and untouched, I can't help but share a little bit about it. At the risk of Google picking up this blog post and more people finding out about Cranberry, I'll record my thoughts and share some images we snapped on our trip. Ah, who am I kidding? No-one reads this blog. I'm a relative nobody on the internet.

Traditionally, my buddy Chris and I always try to organize a February trip with the hope of encountering some winter weather out on the trail. This year, we brainstormed a few ideas and narrowed our possible destinations to 2 locations–Mt. Pleasant, Virginia and Cranberry Wilderness, West Virginia. Both destinations were good winter options because of the amount of loop possibilities for route creativity and for bailing out if conditions deteriorate. We settled on Cranberry Wilderness because we always talk about going there but end up deciding on other destinations. Not this time!

## About Cranberry

Cranberry Wilderness is a 47,815 acre wilderness area in the Monongahela National Forest in West Virginia that was established in 1983. Elevations range from 2,400 to over 4,600 feet. The wilderness area is situated between the Highland Scenic Highway to the east, the Williams River to the north and the Cranberry River to the south and west. There are a lot of trails that traverse the wilderness but they are not blazed, as with all wilderness areas. The trails are maintained with a narrower tread than other forest trails and are marked only at junctions. A map, compass, and the skills to use them are critical to have for navigation in a wilderness area because of this. For more information about Cranberry check out the USFS website [here](https://www.fs.usda.gov/recarea/mnf/recarea/?recid=12368?target=_blank). The official USFS brochure and map are [here](https://www.fs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb5090665.pdf?target=_blank) and [here](https://www.fs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb5195772.pdf?target=_blank). Before visiting this or any wilderness area, it is crucial to plan ahead and let someone know where you are going. Find out more about responsible recreation in wilderness areas on the USFS website [here](https://www.fs.usda.gov/visit/know-before-you-go?target=_blank).

## Planning the Route

We use a combination of tools when planning a route that include GaiaGPS, AllTrails, CalTopo, Google Earth, Garmin Connect and just general research about an area. My preference is to use GaiaGPS because of their map layers, trail auto-follow feature on the website and their mobile app. AllTrails has similar features and is also a good tool. After creating a route, both GaiaGPS and AllTrails have the ability to generate a PDF of the map to print out. We usually export a .gpx or .kml file and look at the route in either Garmin Connect or Google Earth to determine the breakdown of the segments, breaking it up by distances between trail junctions or landmarks. Those distances get put into a spreadsheet which has a pace calculator built in to give us an idea of the overall timing of our route. It's important to know this information, especially in winter, given the fact that there is limited daylight and we like to be at our campsites well before sunset in order to get set up and gather firewood.

After all of that is said and done, we take screenshots of the individual assets, pull them into Photoshop to create a 2 sided printout, and we print it out on waterproof [Terra Slate paper](https://terraslatepaper.com/?target=_blank). We have all of this information on our phones but it's important to us that we have a paper copy as well because... phones break.

Check out the map of our route below. [Here's our printout for this trip.](/user/pages/03.blog/cranberry-wilderness-adventure/cranberry_plan.pdf?target=_blank)

![](/user/pages/03.blog/cranberry-wilderness-adventure/cranberry_map.jpg?classes=caption "The map of our route.")

The gist of our planned route went like this. Start at the North Fork/North-South Trail trailhead on the Highland Scenic Highway/150 side of the wilderness. Hike down North-South Trail to the Laurelly Branch intersection. Take Laurelly Branch down to the Cranberry River and find a campsite. Spend the night near the river and wake up early the next morning and hang a left on Middle Fork Trail and head west all the way out to County Line Trail. Take a right on County Line and head north and uphill to the top of the plateau and then work our way east on County Line all the way to District Line trail. Then head south and look for a campsite near a water source somewhere near the Big Beechy and District Line intersection. Spend the night then wake up early and have a relatively easy 4.5-ish mile hike out to the car. Total mileage would be 29. When we got there, we realized we would need to alter our plan somewhat but more on that later.

## Gear

Packing enough layers to stay warm because of the cold temperatures would be más importante. The forecast was calling for lows in the lower twenties and windchills in the upper teens. Cold AF. After the warmest January on record, winter finally decided to show up the weekend of our trip. Hey, maybe we'd see some snow! Snowy conditions call for warm provisions and I packed accordingly.

![](https://live.staticflickr.com/65535/49724392732_7bd604c0a4_o.jpg?classes=caption "Everything laid out on the floor.")

![](https://live.staticflickr.com/65535/49724075566_7f8ea33a22_o.jpg?classes=caption "Electronics for this trip aside from my camera gear.")

![](https://live.staticflickr.com/65535/49723541553_ef7328c653_o.jpg?classes=caption "Final check on the map and some Dale's Pale Ale.")

Following a recent mindset I've adopted (until I can afford to buy a better small camera), I decided to go with minimal camera gear. I've grown tired of lugging my large DSLR and for the past 2 or 3 trips, I decided to just rely on my phone and an old Sony Cybershot point-and-shoot we had lying around, collecting dust to take photos. While it's not the best gear, it is small and compact. The phone is multipurpose as I would bring it with me anyway for communication and to supplement navigation. The camera, while old, still takes decent photos (better than my phone) and fits in my chest pocket. And so I went with these 2 cameras again in the interest of being minimal and being more present in the moment rather than constantly thinking about shooting the perfect photos. Someday, I'll get an enthusiast level point-and-shoot.

## Day 1

### 8.74 Mi | 853' GAIN | 1,729' LOSS

As the trends of late go, this whole winter has been mild. In fact, we just had the warmest January on record. ~~But everything is fine. We don't need to curb our carbon emissions.~~ That said, winter decided to finally arrive the weekend of our backpacking trip. The Wednesday and Thursday before our grande depart up to WV a winter storm mowed over the east coast and dropped a little bit of snow and ice on Greensboro, from whence Chris would be driving up. This little bit of ice meant that Chris would leave a little later on Friday morning in the interest of safety which put us a tad behind schedule. We weren't complaining though. We were just happy to be getting out on the trails. We texted and agreed that we'd reassess our route once we got on North South Trail and got some perspective of how our pace was going. Chris made his way up to Roanoke and we collectively hit the road around 8:30 a.m.

The drive up to the trailhead was thankfully uneventful. After 3 hours, we arrived at the Highland Scenic Highway and as we ascended the mountain, we saw patches of ice lining the road and a dusting of snow covering the grass and trees (the park service does not plow or maintain the road in the winter). It was a gorgeous sight and after the cruise along the top of the plateau we made it to the trailhead eager to get our hike started.

![](https://live.staticflickr.com/65535/49720792043_69724c6d51_o.jpg?classes=caption "The view down the Highland Scenic Highway from the parking lot at the trailhead.")

![](/user/pages/03.blog/cranberry-wilderness-adventure/IMG_4966.jpg?classes=caption "Yours truly at the trailhead. Photo courtesy of Chris Dunst.")

After parking and stepping outside of the vehicle into the cold, we donned our boots and packs. It was then time to snap the obligatory "before" selfie and a moment later we were off into the woods. The first thing we noticed pretty soon after we entered the wilderness was how dense the forest was. The trees were so close together which created a very serine quiet as they seemed to absorb much the sounds around us. Another slight benefit of this density was that they blocked wind, too. We proceeded west along North South Trail which was mainly rolling downhill all the way to Laurelly Branch. The vegetation changed from typical east coast forest to what seemed like the boreal forests of Dolly Sods with large concentrations of tall pines and ground moss. It was really interesting to experience such changes in a small distance.

![](https://live.staticflickr.com/65535/49721642662_8db1d96bac_o.jpg?classes=caption "The first thing we noticed was how dense the forest was.")

![](https://live.staticflickr.com/65535/49721642987_1844e5508b_o.jpg?classes=caption "Lots of young pines growing up from the forest floor.")

![](/user/pages/03.blog/cranberry-wilderness-adventure/IMG_4985.jpg?classes=caption "This place is so cool. Photo courtesy of Chris Dunst.")

After hitting Laurelly Branch, we took a right (north east) and headed down the mountain along the trail. At first, the trail was similar to what we had been on but it eventually became very rocky and almost like a creek bed. Thankfully, everything was frozen because of the extremely low temps. There were many instances where our feet would have been soaked if things had been thawed out. Happy to have dry feet, we made our way down the frozen creek-bed-like trail and because of the fading daylight, we decided to find a campsite along Laurelly rather than proceeding all the way to the bottom of the valley to the river. Shortly after making the verbal announcement of our plan, we found an awesome established campsite very close to a water source with a nice fire ring.

![](https://live.staticflickr.com/65535/49721642682_4868f73d8c_o.jpg?classes=caption "Just before the intersection with Laurelly Branch.")

We set up the tents then Chris used his lumberjack skills to acquire firewood. I used my fire building skills to get things going and after ~~an eternity~~ about 2 solid hours of adding small bits of wood, fanning the coals, adding more small bits of wood, and yet more flame fanning, we finally had some semblance of a campfire. And good thing, too because we needed the fire to cook our brats and potatoes.

![](https://live.staticflickr.com/65535/49721643092_14fe253a81_o.jpg?classes=caption "Tents set up at an established campsite along Laurelly Branch.")

![](https://live.staticflickr.com/65535/49720792033_f20c32af0c_o.jpg?classes=caption "After a couple of hours of persistance because of the cold, we finally got the fire going.")

![](/user/pages/02.blog/cranberry-wilderness-adventure/IMG_5021.jpg?classes=caption "Dinner cooking on the fire. Photo courtesy of Chris Dunst.")

After dinner, we sat around by our feeble fire and endured the cold as long as we could. We threw in the towel around 9 p.m., hung up the bear bags for the night then got in our tents for some shut eye. Day 1 done.

## Day 2

### 14.98 Mi | 2,752' GAIN | 1,761' LOSS

I woke up to my wrist vibrating from my alarm at 6:30 a.m. and heard Chris stirring in his tent as well. We began the routine of getting stuff broken down and stowed away. As per usual, Chris had his entire backpack packed before I even got out of my tent. I think he has a magical backpacking gnome assistant that he carries with him because there's no way it's humanly possible to pack that fast. There's no way one man, by himself, can accomplish such a feat. Anyway... After the initial round of packing, we sat in our Helinox chairs, warmed up some water for coffee and oatmeal and then enjoyed the morning a bit. 

![](https://live.staticflickr.com/65535/49721643152_1443b169be_o.jpg?classes=caption "Coffee and oatmeal with pecans. The best backcountry breakfast.")

![](https://live.staticflickr.com/65535/49721643122_be8f74bafd_o.jpg?classes=caption "Chris getting his stove prepped for heating up water.")

![](https://live.staticflickr.com/65535/49720792078_93e9eed504_o.jpg?classes=caption "Coffee time.")

We started our hike at precisely 9:02 a.m. (2 minutes behind schedule... not bad). The plan for the day was to continue down Laurelly Branch to the river, brave the river crossing, hang a left (west) on Middle Fork Trail until we got to Big Beechy, hang a right (northeast) on Big Beechy, hike up to where District Line meets Big Beechy and look for a small foot trail that is on the USFS CalTopo map that leads down to the start of North Branch (creek) and look for a campsite. That was the plan. But how often do our trips really go according to plan? Maybe 75% of the time? 60%? Ah, who cares?! We were just happy to be out of doors.

![](/user/pages/03.blog/cranberry-wilderness-adventure/IMG_5050.jpg?classes=caption "Rock trolls. So he's a bit of a fixer upper, so he's got a few flaws. Photo courtesy of Chris Dunst.")

![](https://live.staticflickr.com/65535/49720822158_03ba811a16_o.jpg?classes=caption "On the way down Laurelly to the river. Everything was fro fro.")

![](https://live.staticflickr.com/65535/49721643317_73f3829635_o.jpg?classes=caption "You can just unfreeze it, right Elsa?")

Laurelly to the river was largely uneventful aside from lots of rock troll rocks (from Frozen, c'mon, duh!) a few frozen creek crossings, a 4 point dear antler, and some downed trees to climb over. We rounded a bend and started hearing a rushing sound. We both said, "That's either wind, or water... and if it's water, it sounds like A LOT." That transformed into, "That's gotta be wind in 1 very specific spot, or lots of water." And then eventually, when we could see it, "Wow. That's a lot of water." We had reached Williams River and it was pretty wide and flowing but crossable by foot if we absolutely had to. Our preference was to keep our feet and legs dry because it was so cold. We scouted upstream a bit and located a large tree that had fallen across the river and then decided to use it as a means to cross. Chris braved the log first and after some strategic footwork and a bit of shimmying, he made it across. Then it was my turn. I followed his footsteps and took my time and made it across ever so slowly. Booyah acheived.

![](https://live.staticflickr.com/65535/49721328656_d8fa4bbd81_o.jpg?classes=caption "Made it to the river. Now we just need to cross it.")

![](https://live.staticflickr.com/65535/49721328281_32f989da65_o.jpg?classes=caption "Much too cold to try and cross here.")

![](https://live.staticflickr.com/65535/49721328646_1fa5dd7b98_o.jpg?classes=caption "Least worst option.")

We stopped for a snack then examined the map. A little under 2 miles and we'd be at the Big Beechy intersection. We cruised along the river noting how cool it would be do bring our fishing rods next time catch fish for dinner but then remembering that the river was catch and release. As in, no keepy and eaty the fishies. Oh well. The river was still a sight to behold with super clear water that looked like it would very refreshing in the summer time.

The hike was a slow grade downhill and before we knew it, we found Big Beechy. Hanging a right, we started up the ascent to the top of the plateau. The elevation profile of this trail starts off very steep and eventually levels out then rolls along at the top. We made relatively short work of the climb section and as the elevation changed, so did the vegetation. We encountered groves of pine trees and moss covered forest floor, similar to what we found on North South Trail the day before. It was really interesting and only added to the sense that we were in true wilderness.

![](https://live.staticflickr.com/65535/49721328211_c4fc2535e7_o.jpg?classes=caption "Big Beechy goodness.")

![](https://live.staticflickr.com/65535/49721642902_680a78455e_o.jpg?classes=caption "Rhododendrons on the Big Beechy ridge top.")

![](https://live.staticflickr.com/65535/49720791833_a1e7450f5c_o.jpg?classes=caption "In many places, a carpet of moss covered the ground.")

We finally made it to the top of the climb and then cruised along the plateau a bit again noting how interesting the changes in foliage were. We passed District Line and started to look for the foot trail on the USFS CalTopo map. No trail could be found. So we altered the plan again and decided to hike to the Middle Fork and North Fork Trails intersection. Gaia had a tent icon at that intersection and we also noted that it was the headwaters of the Williams River so there was bound to be a water source. With the new plan hatched and limited day light remaining, we turned on the after burners and picked up our pace.

When we arrived, there was no campsite to be found. Nor was there a giant floating tent icon. Time for plan C. Like I said earlier... how often do our trips go according to plan A? So we decided to fill up our water bottles because there _was_ a water source. Then we'd continue hiking up the trail until either 1. we found a campsite or 2. got to the Jeep because we only had a few miles left to the loop at this point, and we'd look for a flat spot to set up our tents sans campfire. We ended up doing the ladder and fortunately there was a flat spot not 500 feet from the trailhead where the Jeep was parked. The only downside was that there was no campfire ring to be found. We decided to set up there and eventually elected not to do a campfire and just tough it out as long as we could and then call it a night.

![](https://live.staticflickr.com/65535/49720791898_ae2edf0b84_o.jpg?classes=caption "With day light fading and our plans changed, time to turn on the after burners.")

![](https://live.staticflickr.com/65535/49721328681_05b3c607cf_o.jpg?classes=caption "We found our campsite... 500 feet from the Jeep.")

![](https://live.staticflickr.com/65535/49720792213_bda67de3c1_o.jpg?classes=caption "It was pretty darn cold.")

![](https://live.staticflickr.com/65535/49721642952_6f6ed73df4_o.jpg?classes=caption "No campfire tonight.")

## Day 3

### 0.1 Mi | 0 GAIN | 0 LOSS

I set my alarm for 6:30 but my brain woke me up a bit earlier and I just laid there listening to the wind in the trees while enjoying the last little bit of cozy warmth from being in my sleeping bag. After a few minutes, I heard Chris stirring in his tent. We both started the process of getting our gear broken down and packed up, not really caring too much about how things fit into our packs because we didn't have far to walk to get to the car. The name of the game was just simply stuff everything in the packs so we could walk the 500 feet to the trailhead where Chris's Jeep awaited. We elected to forego coffee at the campsite in favor of having coffee at one of the overlooks to catch the sunrise which helped expedite the process of breaking down and after one final campsite check, we lumbered along the short stretch of trail towards the end of our trek.

We popped out of the woods and we crossed the Scenic Highway on foot for the last time and unlocked the Jeep to situate our packs and gear inside. We got into some clean clothes, started up the engine, then made our way down the Highway to the Big Spruce overlook to check out the views of the sunrise and have some much needed caffeine.

There happened to be a small shelter with a picnic table next to the parking lot and we utilized it to make our coffee so that we could stay out of the brutally cold wind. After boiling water and brewing up or Starbuck's Via, we walked down the boardwalk to learn about the CCC and how they restored the area after a massive logging operation and subsequent forest fire in the 1920's. At the end of the boardwalk was an awesome overlook of Big Spruce and we partook in the views and finished up our coffee. Trip complete. Time to head home.

![](https://live.staticflickr.com/65535/49723310433_2982385405_o.jpg?classes=caption "Sunrise from the Big Spruce overlook on the Highland Scenic Highway. This is where I wished I had brought the big boy camera.")

![](/user/pages/03.blog/cranberry-wilderness-adventure/IMG_5128.jpg?classes=caption "Coffee time.")

## Final Thoughts

Cranberry Wilderness is an amazing place. It's one of the more wild wildernesses that we've been to in the southeast and it left us with a sense of awe that will be marinating in our brains for a long time to come. There were several times when I just had to stop and admire the awesomeness and say, "This place is so cool," out loud. What's lacking in views for Instagram fodder is made up for in quiet solitude that only a wilderness area in the highlands of West Virginia can provide. This isn't the place to go if you're looking for sweeping vistas and rock outcroppings to watch the sunrise. No, it's a place to go if you are seeking a true wilderness experience where you have to rely on your backcountry skills and knowledge to navigate and stay safe (i.e. navigate unmarked trails, build a fire, not fall in a river, get eaten, etc.). It's also a place to go if you are seeking a mental health break from the hustle and bustle of life and the outside world. We need to make sure these places exist for future generations and I'm thankful that President Lyndon B. Johnson signed the Wilderness Act in 1964 so that my generation could know and experience places like Cranberry, Dolly Sods and Shining Rock, to name a few. Especially in today's time of the endless news cycle and constant firehose of information into our eyeballs. I hope that I can someday share this place with my 2 boys.

I'd like to go back to Cranberry in the future, for sure. If not to experience another section of the wilderness, but also to partake in the silence that exists there. The mental health break that Cranberry's silence provided was an especially big part of this trip. The forest up there is so dense and immersive that once you're in it, _you're in it_. I'm actually surprised we didn't see Bigfoot because of how impenatrable it is. Cranberry Wilderness provides a total immersion into the natural world and I hope to be back in it again soon.