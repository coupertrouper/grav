---
title: 'Dolly Sods Traverse'
media_order: 37069688413_e370591150_k.jpg
subheading: 'Autumn and backpacking go hand in hand. I always try to get a fall trip together and this year was no exception. For this trip, I brought my brother-in-law, Craig along. We deliberated for a few weeks between spending 2 nights in Shenandoah National Park or 2 in the Dolly Sods Wilderness.'
header_image: 37069688413_e370591150_k.jpg
date: '13-10-2017 09:00'
publish_date: '13-10-2017 09:00'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - 'West Virginia'
---

Autumn and backpacking go hand in hand. I always try to get a fall trip together and this year was no exception. For this trip, I brought my brother-in-law, Craig along. We deliberated for a few weeks between spending 2 nights in Shenandoah National Park or 2 in the Dolly Sods Wilderness. The scales were tipped in favor of Dolly Sods because of its physical location. Craig lives in Columbus, OH and I live in Roanoke, VA so the Sods made sense as a meeting spot. Every trip I've ever taken to Dolly Sods has been awesome (like this one or this one) and the terrain, unlike anything else on the east cost, keeps me coming back for more. This trip would prove to be one of my favorite excursions to the area.

## OUR ROUTE

The plan we came up with was to meet at the Bear Rocks trail head on FR 75, proceed up Bear Rocks trail, hang a left and head south on Raven Ridge trail, take a right on Dobbin Grade then a quick left on Upper Red Creek trails and continue south, then a right on Blackbird Knob trail and continue west on Harmon trail, then north on Rocky Ridge trail. We'd head east on Dobbin Grade, then north and east on Beaver View working our way back to Bear Rocks trail via Raven Ridge and then back to the cars. Night 1 would be spent somewhere on Bear Rocks trail and night 2 along Dobbin Grade at the Upper Red Creek trail crossing.

### DAY 1
#### Mileage: 1.62 | Gain: 315 ft | Loss: 286 ft

The drive up to West Virginia was gorgeous. As I headed north, the leaves just got more and more colorful. The misty and foggy conditions made the colors pop even more. After driving through rolling farmlands and valley back roads, I eventually found myself on the rough gravel climb up FR 75. 

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502973/37708398822_f8a1b342c0_k.jpg?classes=caption "FR 75 never disappoints.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502971/37691817226_af14db750d_k.jpg?classes=caption "More foggy as the elevation increased.") {.center}

When I got to the top, it was completely foggy and visibility was less than 10 yards. There were a ton of cars parked at the Bear Rocks trail head. We aren't the only ones who enjoy getting outdoors during the fall, it would seem. I got my gear together and last minute food items into my pack and waited for Craig to arrive. I finally saw the lights of his Subaru come around the final switchback at the top of the gravel road and he parked behind me. By then the fog was starting to break up. We got Craig's pack situated and took a "before" pic then started our hike. It was around 2:00pm when we got going and we didn't have a long hike at all to get to our planned campsite on Bear Rocks trail. As we descended into the wilderness area, the fog broke up even more and we could see ridgetops and treelines appearing. The trail followed the terrain down to a creek then back up to the a bald knob. On the way back up to the knob, the trail traverses a bog using a long boardwalk. This would not be the last bog we would encounter which is part of what makes the marshy alpine terrain so interesting.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502973/37069688413_e370591150_k.jpg?classes=caption "Bear Rocks Trail in the Dolly Sods Wilderness.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502973/23887452398_af3e2bbc3b_k.jpg?classes=caption "Boardwalk crossing one of the many alpine bogs of the Sods.") {.center}

We arrived at the spot that we had seen on Google Earth, a small bald knob that when viewed in the satellite images, had a few areas where possible campsites could be. Through my research I found several GPS markers for possible campsites in this area, too. So I had a general hunch that we'd find something. When we got above treeline, we started scouting around the bald knob and after a few minutes of trudging through the waist high vegetation, we found a small foot trail that led to a nice sized flat spot with an established fire ring. 

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502969/37069689513_83c5291f38_k.jpg?classes=caption "Craig scouts the bald area for our campsite.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502971/23887479728_3c9523bd3d_k.jpg?classes=caption "We set up camp and had some dinner and oat sodas around the fire.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502968/23887456288_31e6960b35_k.jpg?classes=caption "The view from our campsite.") {.center}

This would be our spot for the night. We set our packs down, got our shelters up and cracked open the beers that we brought. After some chatting over beers, we got a fire going and then our dinner. Craig brought hotdogs and I brought pork chops. Nothing beats cooking meat over a campfire while looking out over a valley cast in the light of a gorgeous sunset. We watched the sun go behind the ridge in front of us then took some pics of the stars. After some time, we headed to bed. We hit the hay early because the next day's hike would be challenging and we needed our sleep.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502966/37708418032_2cd6fa6e51_k.jpg?classes=caption "Beautiful sunset from our campsite on Bear Rocks trail.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502966/37482215380_58ae3de5e2_k.jpg?classes=caption "Checking out the stars. There was almost no light pollution.") {.center}

### DAY 2
#### Mileage: 10.56 | Gain: 919 ft | Loss: 1096 ft

We started our second day around 7. I groggily woke up, got my glasses on, shoes laced up and then headed down to the tree where we hung our bear bag. The sun was starting to illuminate the valley and a fog had rolled in. It made for some very interesting photos. I got some hot water going for coffee and oatmeal and we walked around the bald knob to capture some images of the sunrise. 

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502960/37482216860_06432d7898_k.jpg?classes=caption "The view from my front porch.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502968/23887465198_63c12d2712_k.jpg?classes=caption "Fog in the valley down below.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502966/37482218140_336eacc6b4_k.jpg?classes=caption "A small family of deer checking us out.") {.center}

We saw a small family of deer down in the valley and tried to photograph them from a distance. They just looked back up at us waiting to see what we would do next. After some time they trotted off into the trees on the next ridge. It was getting close to trail time so we broke down our camp, got our gear packed and eventually started our day's hike. My GPS watch let out a beep signaling that it had acquired a fix on the satellite and we were off to the races... well, not really. We had some distance to cover for our hike so we started out with a brisk pace. Along with our trek to the campsite for night 2, we had planned to do an additional ~6 mile day hike along Rocky Ridge trail. So we needed to make haste to ensure that we'd have enough daylight for our day hike.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502963/37482229060_c83d2843b3_k.jpg?classes=caption "Upper Red Creek trail cruising through alpine bogs and marshlands.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502972/23887478448_9eefe81548_k.jpg?classes=caption "Somewhere near Blackbird Knob in the Dolly Sods Wilderness.") {.center}

The trails from our campsite for the first half of the morning's hike were mostly downhill (Raven Ridge, Upper Red Creek, and Blackbird Knob trails) until Blackbird Knob crosses Upper Red Creek (the water feature, not the trail). Then it's a slow uphill grind up Harmon to Rocky Ridge. We were happy and chatty on the downhill and quiet and concentrated on the uphill. It was definitely a challenge to get up the hill with our loaded packs and this being Craig's second trip, he took it on like a champ. The quad busting slow grade finally came to an end after about a mile an a half. We reached Rocky Ridge and the views of the Canaan Valley down below were awesome. From there, it was an easy cruise along the ridge and then a quick descent down to our planned campsite where the trail crosses Upper Red Creek again. We were doing great on time and decided not to rush. Craig and I took in the views, snapping photos along the way and even stopping for a snack.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502963/37482223500_d73b6aabd1_k.jpg?classes=caption "Descending back down to Upper Red Creek to find our campsite.") {.center}

### DAY HIKE
#### Mileage: 6.23 | Gain: 620 ft | Loss: 602 ft

Because we were slated to arrive at our campsite around lunchtime, we had some time to go explore some. We planned to do a day hike from our campsite, up Harmon Trail, back to Rocky Ridge all the way to the northern boundary of Dolly Sods. We'd use Raven Ridge and Beaver View to get back down to our campsite from the ridge. Timing worked out perfectly and we were able to get the whole loop in and still have daylight to spare. The views from the top of Rocky Ridge near the northern boundary are spectacular. I can only imagine what the stars would look like up there. Next time we'll have to plan on spending the night up there instead of down in the valley.

![](http://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_webp,fl_awebp,w_1600/v1556502966/37482221180_dc20b3b372_k.jpg?classes=caption "The view from Rocky Ridge in the Dolly Sods Wilderness.") {.center}

![](http://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_webp,fl_awebp,w_1600/v1556502962/37691812716_7a565cd718_k.jpg?classes=caption "Back at the campsite to enjoy the sunset.") {.center}

![](http://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_webp,fl_awebp,w_1600/v1556502963/23887441488_72397fc30c_k.jpg?classes=caption "Enjoying the campfire after 2 days of heavy hiking.") {.center}

### DAY 3
#### Mileage: 4.46 | Gain: 686 ft | Loss: 476 ft

We started day 3 before the sun rose because we wanted to get a jump on our hike so that we could get back to the cars with enough time to go grab lunch and get home at a decent hour. Camp breakdown was done pretty quickly and efficiently. As camp was being broken down, I boiled some water for our breakfasts and coffee. We camped right next to a creek so I was liberal with my water and 3 cups of coffee total. We got our gear together and we were on the trail by just before 8:00 am. 

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1556502960/23887439428_73799b35a2_k.jpg?classes=caption "Hiking up Dobbin Grade in the morning.") {.center}

We hiked up Dobbin Grade and then took a left on Beaver View, heading back the way we had come yesterday on our day hike. Along the hike, we spotted a really interesting campsite that was at the edge of a treeline that overlooked the valley where we had just come. Next time this would definitely be on my list of campsites to consider. The trail crossed through grassy fields and then ducked back into the forest many times. Eventually we were at the top of the plateau again and hit Raven Ridge. The terrain at the top was more rugged and windswept. We made our way down the short section of Raven Ridge and soon hit Bear Rocks trail again. This would take us all the way back to the cars. The trip was coming to an end and we were both recharged from our time out in the wilderness but also looking forward to getting home to our families. We snapped photos along the trail noting the ridges and views that we couldn't see on day 1 because of the thick fog. Before we knew it, we were back at the cars. As it seems to have become a tradition, we discussed our lunch plan of heading down to Amelia's in the Canaan Valley to get a post trip meal. This was a successful trip all the way around from the initial planning to the end. I'll definitely be making my way up to Dolly Sods again soon.