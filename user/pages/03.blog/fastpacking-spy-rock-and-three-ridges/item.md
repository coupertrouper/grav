---
title: 'Fastpacking Spy Rock & Three Ridges'
subheading: 'The George Washington National Forest, located in Virginia, has quickly become my new stomping grounds and the draw of the quickly changing fall leaves pulled me in yet again for a solo autumn trip. 3 days of fastpacking between 2 wilderness areas was a perfect way to wrap up an awesome year of adventures. Read on to see how it went.'
published: true
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - Virginia
        - Fastpacking
        - Fall
---

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/map_spy-rock-priest.jpg?classes=caption " ")

Dates of this trip were November 12, 2021 to November 14, 2021.

## The Plan

The plan for this trip was basically derived from combining 2 routes that I had done previously with my friend Chris. Read about the specifics of these routes in more detail in my [Spy Rock](/blog/spy-rock) and [Mau Har(d)](/blog/mau-har-d) trip reports (and in Chris's trip reports: [Spy Rock](https://y2kemo.com/2016/05/redemption-in-the-priest-wilderness/?target=_blank) and [Mau Har](https://y2kemo.com/2014/11/backpacking-three-ridges-wilderness/?target=_blank)). In a nutshell, the plan was to park at the Appalachian Trail (AT) trailhead where it crosses VA SR 56 then hike out to Spy Rock and camp for night 1. Then hike all the way back to the car, resupply, then hike the AT/Mau Har/AT lollipop route, camp at the top of Three Ridges for night 2. Then head back down the mountain to the car on day 3. All of this would be done in fastpacking style with minimal gear moving as fast as possible. 

Below is an itinerary of this trip using a 1.5 mph pace which was intended to be conservative since I'd be fastpacking (jog hiking... jiking? Hogging?). Hoggin' some summits!

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/spy-rock-three-ridges_itinerary.jpg?classes=caption "Spreadsheet numbers are based on GPX data created in gaiagps then imported into Google Earth")

Below is the map for this trip. The blue/yellow color coding of the sections isn't really helpful aside from marking my intended track because Day 2 consists of hiking all the way from Spy Rock to the summit of Three Ridges. As it relates to Day 2, the demarkation doesn't mean anything aside from indicating where I'd be walking. Because I combined routes from 2 previous trips, it was easier to load in the GPX data this way rather than making 1 big master GPX file. A side benefit of this helped me break up the trip into 2 sections mentally since I'd be covering a lot of miles. Doing the resupply would also provide a nice bail out option should things not go according to plan on Day 1.

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/spy-rock-three-ridges_detailed_map.jpg?classes=caption "The blue and yellow color coded tracks are my route.") 

## Gear & Food

This would be my first trip using my new Ultimate Direction Fastpack 20 and I was pretty excited about trying it out for the first time. In theory, all of my stuff would fit. In practice? Well, we would soon find out. 2 weeks out the forecast called for temps in the 30's at night. 2 days out the forecasted lows at night changed to be in the upper 20's. I had to pack accordingly and the smaller 20 liter pack size was starting to become a game of Tetris. A slight problem that the lower forecasted temps presented was that my superlight and compressible sleeping bag is rated for 30°F. The lower temps at night would mean that I would be the antonym of comfortable. I packed an extra puffy jacket (in addition to the orange jacket pictured), a thermal blanket to use as my ground sheet (in addition to my NeoAir and Zlite), and extra wool baselayers. I wouldn't recommend packing a sleeping bag that doesn't match the forecasted low temps but I don't have $500 lying around to get a 0°F down bag that packs down to the size of a grapefruit. Hopefully the extra layers would make up for the lack of down in my bag.

The pic below is pretty close to the final result. I didn't bring the saw because I decided I wouldn't be building campfires since I'd be camping on summits with no means of dousing the coals when I left the campsites (i.e. no agua). Because only YOU (or me) can prevent forest fires (by not even having a fire). See? I'm smart! _Internal monolog:_ "But you just spent a whole paragraph talking about the cold and how cold you'd be." _Voice of reason:_ "I'd rather be cold than start a forest fire." Night 1, I used a tarp (pictured) but on night 2, I used my MSR Hubba (not pictured) in fly-only-pitch-mode. I was able to swap it out when I resupplied at the car on the return trip from Spy Rock.

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_gear_PXL_20211110_210417522~2.jpg?classes=caption "Most of the gear except for my MSR tent fly, which I swapped with my tarp at the resupply at VA SR 56.")

Food consisted of the usual odds and ends. Soba noodles + chicken + sriracha & soy sauce for dinners. Pro Bars + instant coffee + oatmeal for breakfasts. Gu Chomps + Clif Bars + trail mix + tuna + other random things for snacks. Tailwind for hydration. A chocolate bar buried in there somewhere.

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_food_PXL_20211110_203543749~2.jpg?classes=caption "All of the standard things I usually pack for food on these trips. And yes, that's a breadbag.")

## Day 1
### 9 MI | +4,239' GAIN | -1,394' LOSS

Got to the trailhead around 11 on Friday; got on the actual trail (AT) shortly thereafter. The mission was to get all the way out to Spy Rock for night 1. Along the way I'd ascend The Priest. The leaves were at peak in this area which was pretty amazing. Staring up at the leaves and taking photos along the way helped break up the challenge of climbing the massive Priest during the first part of the day's route. After Priest came a nice cruise along the ridge out to Spy Rock. I went up to Spy Rock and a nice couple from Columbus, OH offered to take my photo. Then went down to find a campsite and get warm because the temps were dropping. I stopped my watch right at 9 miles. Even with the temps going down, I wouldn't make a fire because there was no water source (I like to douse my fires when I leave a campsite). After sunset, I went back up to Spy Rock to stargaze and saw a shooting star. Pretty epic day.

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_164353308.jpg?classes=caption "Heading up the trail into The Priest Wilderness.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_165337563.jpg?classes=caption "Virginia's leaves showing off their fall colors.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_165341240.jpg?classes=caption "More leaves showing off.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_165356178.jpg?classes=caption "Show offs. I can't not stop and take tons of pics.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_171711194.PORTRAIT.jpg?classes=caption "My late grandfather's hat. Lately I've been wearing his hats on my trips as a tribute his love of the outdoors.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_174200472.jpg?classes=caption "First view on the scent to the top of The Priest.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_183700565.jpg?classes=caption "The top of The Priest. There were people at the main rock outcropping so I just snapped a pic here and moved on.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_190526422.jpg?classes=caption "Exiting The Priest Wilderness and onwards to Spy Rock.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_201351516.jpg?classes=caption "The approach trail to Spy Rock.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_201736972.jpg?classes=caption "A look at the guide cables along the trail. Volunteers recently installed these to keep hikers off of the rare vegetation up there.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_201804369.jpg?classes=caption "The spectacular view from Spy Rock.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_202924836.jpg?classes=caption "A nice couple from Columbus, OH offered to take my pic.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_213637457.jpg?classes=caption "Tarp setup for the evening.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_214542823.jpg?classes=caption "Nope. No fire tonight.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_215921937.jpg?classes=caption "Sunset from the summit area of Spy Rock.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_221400808.jpg?classes=caption "Boiling water for dinner.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211112_225120495.NIGHT.jpg?classes=caption "Small luxury of chocolate.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_1_PXL_20211113_022226022.NIGHT.jpg?classes=caption "Looking northeast towards a town... Wayensboro probably.")

## Day 2
### 16.95 MI | +5,417' GAIN | -5,285' LOSS

Woke up around 6, began breaking down camp then scrambled up to the Spy Rock summit area to catch the sunrise. Came back down after a few minutes to make coffee and finish breaking down. Reviewed the map and trip mileage spreadsheet and got on the trail around 7:30. Today's objective was the summit of Three Ridges and I had about 16.5 miles and close to 5k of elevation to gain to get there. On the way back along the AT I hit a quick wave of snow/sleet coming down off of Priest. Spy Rock/Priest were an out-and-back so I was able to stop at the car and resupply (new food bag, dropped off trash, and water refill... Also swapped shelters) then continue on the AT to Mau Har trail. The car stop felt like cheating. Mar Har was Mau Hard... Lots of rock scrambling up the steep slope and careful route finding next to the waterfalls and creek. Finally made it to the top just before sunset. I had planned to be hiking in the dark by this point so it was nice to be ahead of schedule. Found a nice campsite tucked away from the wind in some thicker brush. Objective achieved. Ramen/instant potatoes for dinner, dark chocolate for dessert. Went to bed around 7. It was a long but rewarding day.

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_111131572.NIGHT.jpg?classes=caption "View from inside the tarp.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_115731865.jpg?classes=caption "Sunrise colors being cast on the tree trucks.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_120119179.jpg?classes=caption "Boiling some water for breakfast and coffee.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_124503881.jpg?classes=caption "Coffee to go!")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_143407763.jpg?classes=caption "The view from the main rock outcropping on top of The Priest.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_151915304.jpg?classes=caption "Some snow and sleet blew through as I descended. It didn't stick but it was definitely coming down.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_165604979.jpg?classes=caption "Crossing the Tye River after resupplying at the car.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_165759911.jpg?classes=caption "Entering Three Ridges Wilderness along the AT.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_174906144.jpg?classes=caption "More amazing leaf color along Mau Har trail.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_183445203.jpg?classes=caption "Mau Har trail was mau hard to follow in places. Can you spot the trail? Neither could I.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_184122304.jpg?classes=caption "Stopped for some tuna lunch.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_194756835.jpg?classes=caption "Finally back on the AT after ascending the relentless climb of Mau Har.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_203648840.jpg?classes=caption "I look like I've just hiked 16 miles and gained about 5,000 ft.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_210343777.jpg?classes=caption "Shadow of the mountain being cast on the valley below.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_214027391.jpg?classes=caption "Swapped shelters for the MSR Hubba in fly-only-pitch-mode.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_221544666.jpg?classes=caption "Boiling some water for dinner.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_2_PXL_20211113_220552661.NIGHT.jpg?classes=caption "Looking up at the moon. I was in for a cold night.")

## Day 3
### 5.49 MI | +730' GAIN | -3,746' LOSS

Woke up around 4 because... Why not? Actually it was quite cold and I couldn't sleep very well. The thermometer read ~25 May as well get moving. Had coffee/breakfast in the tent then broke down and got on the trail around 5:40. Took out the bear bell because there were definitely some glowing eyes in the woods... Bear? Bigfoot? I'd rather not find out. I klinked my way down from my alpine camp on Three Ridges and made it to Chimney Rock just in time for sunrise. The sun rose beautifully and then my watch battery died. As did my will. OK, not entirely. I was starting to feel pretty drained and dehydrated because I ran out of water so I didn't take many photos... Just put my head down and focused on getting off the mountain safely and quickly. Finally found a small creek just before Harper's Creek shelter and refilled H2O. Then continued onward to the finish line... Still 2 miles away. There was a massive group camping at Harper's, passed them in haste then jogged the remainder of the trail downhill back to the Tye River bridge and subsequently, the car.

Trip complete. 

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_093508867.NIGHT.jpg?classes=caption "Woke up at 4 because... why not?")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_100412370.jpg?classes=caption "Mid-twenties at night. Super cold! Did not sleep well.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_093807709.NIGHT.jpg?classes=caption "Boiling water for some much needed hot coffee.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_094140872.jpg?classes=caption "Coffee in the sleeping bag is the best.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_110940164.NIGHT.jpg?classes=caption "Started the day's hike in the darkness. Carefully made my way down the mountain and saw eyes off in the distance...")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_113614663.jpg?classes=caption "Set the pack down at Chimney Rock to snap some pics of the sunrise.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_115131015.jpg?classes=caption "Totes amazing. Obvi.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_120318929.jpg?classes=caption "I'm totes tired. But happy.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_123452941.jpg?classes=caption "Closing in on Harper's Creek Shelter along the AT.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_130352504.jpg?classes=caption "Stopped at Harper's Creek for a quick refill of water.")

![](/user/pages/03.blog/fastpacking-spy-rock-and-three-ridges/day_3_PXL_20211114_140103390.jpg?classes=caption "Back at the Tye River bridge and shortly after, the car.")

## Final Thought

Getting out like this is a gift, not a privilege. Thankful for a supportive family and health/life circumstances to be able to have adventures like this.