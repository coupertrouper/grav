---
title: 'Thomas Knob Overnight'
subheading: 'Mt. Rogers National Recreation Area has so many options for everything from quick day hikes to 3 or 4 day long backpacking loops.'
header_image: sm_DSC_3849.jpg
date: '29-05-2015 09:00'
publish_date: '29-05-2015 09:00'
author: 'Scott Cooper'
---

Mt. Rogers National Recreation Area has so many options for everything from quick day hikes to 3 or 4 day long backpacking loops. A recent solo trip found me up that way, exploring the Mt. Rogers area again. I started out at the Fairwood trailhead along SR 603. I made a loop taking Fairwood to the Appalachian Trail to the summit of Mt. Rogers to Lewis Fork Trail back to Fairwood. I made it a 1 night trip, camping out on Thomas Knob with my tarp. Weather was hot and sunny during the day with highs in the upper 80s and cool and a little breezy at night with lows in the 60s. My total mileage didn’t amount to much; maybe 10 miles at most. But the point of this trip wasn’t to bust out miles. As with many of my solo outings, the point was to recharge and absorb all of the great things nature has to offer, take some pictures and get some exercise.

![](https://live.staticflickr.com/65535/48059002053_318056fc2e_o.jpg?classes=caption,img-fluid "One of my favorite signs.")

![](https://live.staticflickr.com/65535/48059002963_c06dbbe0db_o.jpg?classes=caption,img-fluid "Easy creek crossing just after the hike started.")

![](https://live.staticflickr.com/65535/48059049967_daa29aeff5_o.jpg?classes=caption,img-fluid "Entering the Lewis Fork Wilderness.")

![](https://live.staticflickr.com/65535/48059003748_7dce112105_o.jpg?classes=caption,img-fluid "Almost jungle like.")

![](https://live.staticflickr.com/65535/48059004343_06f65df37d_o.jpg?classes=caption,img-fluid "View from the top of the climb.")

![](https://live.staticflickr.com/65535/48058955376_b3b968869f_o.jpg?classes=caption,img-fluid "Entering the Grayson Highlands from the north.")

![](https://live.staticflickr.com/65535/48059054097_ef27e7c074_o.jpg?classes=caption,img-fluid "Looking up at Pine Mountain from the Scales area.")

![](https://live.staticflickr.com/65535/48059007463_020d6788b9_o.jpg?classes=caption,img-fluid "Looking back down from the top of Pine Mountain in the Rhododendron Gap area.")

![](https://live.staticflickr.com/65535/48058958576_f89a0ac25f_o.jpg?classes=caption,img-fluid "One of my favorite spots in the southeast. On the rock formation above Rhododendron Gap.")

![](https://live.staticflickr.com/65535/48059055727_fab4d0d4cb_o.jpg?classes=caption,img-fluid "Tarp set up and ready in summer mode.")

![](https://live.staticflickr.com/65535/48058959736_1f9431579e_o.jpg?classes=caption,img-fluid "The view from under the tarp.")

![](https://live.staticflickr.com/65535/48058960256_66560c2f21_o.jpg?classes=caption,img-fluid "Taking in the views and waiting for sunset.")

![](https://live.staticflickr.com/65535/48058961636_c98040e000_o.jpg?classes=caption,img-fluid "Sunset starting to happen.")

![](https://live.staticflickr.com/65535/48059011648_807832e999_o.jpg?classes=caption,img-fluid "Not much of a sunrise. But still thankful to be up there.")

![](https://live.staticflickr.com/65535/48058962701_fab1d4b84d_o.jpg?classes=caption,img-fluid "Summit? Sure, why not.")

![](https://live.staticflickr.com/65535/48058963071_534887a248_o.jpg?classes=caption,img-fluid "Proof.")

![](https://live.staticflickr.com/65535/48059060852_086f944ab5_o.jpg?classes=caption,img-fluid "Ponies on the hike out.")

![](https://live.staticflickr.com/65535/48058966226_53d3432eee_o.jpg?classes=caption,img-fluid "Uneventful jaunt down cliffside trail.")

## Final Thoughts

As with all trips to the Mount Rogers National Recreation Area, I came away feeling recharged and ready to get back to life off the trail. A much needed visit to one of my favorite spots on Earth, this trip will stand out in my mind for years to come. I will indeed be back and will bring a buddy next time.
