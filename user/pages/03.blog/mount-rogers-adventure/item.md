---
title: 'Mount Rogers Adventure'
subheading: 'This fall I was fortunate enough to squeeze not 1 but 2 backpacking trips into my busy schedule. After heading up to Dolly Sods a few weeks prior, I concocted a plan to head up to Mount Rogers to do some backpacking and trail running.'
header_image: 38411727342_d58fffe760_h.jpg
date: '17-11-2017 09:00'
publish_date: '17-11-2017 09:00'
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - Virginia
---

This fall I was fortunate enough to squeeze not 1 but 2 backpacking trips into my busy schedule. After heading up to Dolly Sods a few weeks prior, I concocted a plan to head up to Mount Rogers to do some backpacking and trail running. I wanted to see if I could do a long distance overnight loop circumnavigating and summiting Mount Rogers then a short trail run on Iron Mountain trail all in 48 hours. To do this, I would need to be as efficient as possible on the backpacking loop to save strength for the trail running loop.

On a somber note, the world lost Ueli Steck in April of this year. I first heard of Ueli in 2007 and 2008 when he hit the scene breaking speed ascent records of the Eiger North Face. I admired his approach to fast and light mountaineering and even though he was such a rock star in the climbing world, his attitude remained humble and down to earth. I never personally met him but he seemed like a great guy with an infectious enthusiasm for pushing the limits of what is possible not only with his feats in mountaineering but within himself. His "projects," as he would refer to them, always inspired me and I tried to bring the same level of creativity to my own backpacking trips. He was one of my heroes and in my own small way, I wanted to honor him with a "project" of my own, combining multiple modes of human powered travel while moving as fast and as light as possible in the mountains.

## The Route

And so I built a route that started near the Grindstone campground along Laurel Valley Rd., VA 603. The route ascends Rogers via Mount Rogers trail, the AT and the Mount Rogers summit spur trail. Then takes the AT to Pine Mountain trail to a campsite just after the Cliffside trail. Then descends Cliffside trail to the Lewis Fork Spur trail which attaches back to Mount Rogers trail and descends the mountain back to the parking lot. The trail running route I created ascends a trail which attaches to FR 828 then the spine of Grave Mountain then loops back around to 828 via an unnumbered forest road. I did some mountain biking in the Iron Mountains a few years ago and was vaguely familiar with the trail system but hadn't yet explored the trails that I would be running. Read on for more info about the trail run. Below is a map and photos from the backpacking portion of this trip.

### DAY 1
#### Mileage: 10.44 | Gain: 2680 ft | Loss: 1335 ft

I left Roanoke at a decent time and arrived at my destination around 9:00 am. It took me about 30 minutes to assess my layering, make some tweaks and situate my gear. I got on the trail by 9:45 and started up Mount Rogers trail. It was a pretty cold morning with temps in the upper 30's. 

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965771/24570616608_c82955afd9_k.jpg?classes=caption "Frost on the boardwalk at the start of the trail.") {.center}

I warmed up very quickly though as I got going because this trail is almost all uphill until you get to the AT. At first it's a long and steady grade but as the trail wears on, it's increasingly steep and rocky. After some short and technical rocky sections, the trail levels off and approaches Deep Gap. Then there is another steep section and it rounds off on the shoulders of Mount Rogers. There is a side trail that I took that leads to the boundary of a pasture with barbed wire fencing where I stopped for some brunch and a coffee. The views from this point were pretty spectacular and makes the perfect spot for a break.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965771/24570611708_2066f1c6b2_k.jpg?classes=caption "The long slow grade up to Deep Gap.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965769/37726865314_4cd0dbcce4_k.jpg?classes=caption "Vegetation changing over to pine forest.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965767/37726864204_aa8d2cda19_k.jpg?classes=caption "Quick brunch stop to check out the view.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965768/37555091755_35b6931e5e_k.jpg?classes=caption "Making some coffee and oatmeal.") {.center}

After my quick brunch stop, I got back on the trail to continue towards the summit of Mount Rogers. There had been a lot of rain the previous week and the creeks were flowing very nicely along the trail. I stopped to filter water after a few miles so that I could top off my bottles for the evening's water supply. I knew that my campsite would be dry from previous experience and wanted to ensure that I took advantage of the crystal clear flowing creeks while I had the chance.

The hike to the summit is interesting and includes some very nice views before you hit the spur trail. Once on the spur trail, the terrain changes from rolling highland vegetation to dense alpine forest as the elevation increases. If you've done this hike before, you know that Mount Rogers has the most anticlimactic summit of any peak on the east coast. When you reach the top, there are no views because the forest is so thick. Instead, there is a large rock with the summit medallion surrounded by thick pine forest. That said, it's still the highest point in Virginia and one should feel a great sense of satisfaction for reaching the top.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965766/37555085685_c813e30c0d_k.jpg?classes=caption "Getting ready to head up the spur trail to the summit.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965765/37555090265_8607969f6a_k.jpg?classes=caption "No view at the top but there is a summit medallion.") {.center}

This was my 4th successful summit of Mount Rogers from valley to peak. After the summit, I cruised down hill to Pine Mountain trail and across the ridge to where my campsite would be. I heard a large group of scouts ahead and noted that they were getting their packs back on in the clearing at Rhododendron Gap after stopping for a break. 

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965764/37555082375_bade0fe7bf_k.jpg?classes=caption "Looking up at the Wilburn Ridge rock formation.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965766/37555079035_5ab7f15502_k.jpg?classes=caption "Quick cruise down Pine Mountain trail.") {.center}

I slid past them and then bounded as quickly as I could down the trail in order to make ground on them so that I could claim the campsite that I had in mind. The trail made its way along the ridge through rhododendron thickets and quick rock scrambles. The landscape opens up just after the Lewis Fork Wilderness sign at Cliffside trail. Looking at the map and Google Earth, I was able to find a small knob where I thought might be a good spot to camp. Sure enough, after doing some searching through the open area I found a small fire ring just below the rocky knob.

Finally time to take off the pack and get the campsite set up. My MSR Hubba has a fast pitch option where it can be set up without the tent's inner nest. In the interest of going light, I chose to go with this setup and combine it with a bivy to keep any bugs that may still be wondering around off of my face. The bivvy would also protect my sleeping bag from rain splashes in case the weather turned sour. There were no bugs and no rain to worry about.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965764/38441889801_78bdaedf70_k.jpg?classes=caption "Set up the tent in fast pitch mode.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965759/38386052046_7edf844b11_k.jpg?classes=caption "Probably the best campsite I've found in a long time.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965761/38441888091_0c62d4ab92_k.jpg?classes=caption "Totally not a posed photo.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965769/37727924204_581416fd83_k.jpg?classes=caption "Amazing sunset.") {.center}

After snapping some photos of a gorgeous sunset, I got a fire going and ate some dinner. Then sat on the rocky knob to watch the stars come out. I went to bed around 9 knowing that I would need to get an early start the next day if I wanted to get my trail run in.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965762/38386045366_50f660a606_k.jpg?classes=caption "Headlamp light trails on their way to a sleeping bag.")

### DAY 2
#### Mileage: 4.37 | Gain: 443 ft | Loss: 1690 ft

The second day started before sunrise at about 5:30 am. I took my time getting myself warmed up and had a cup of coffee while still lounging in my sleeping bag. The joy had to come to an end though and I got out of my cozy bag and braved the cold morning air to go use the restroom. Then I began the process of breaking camp. After getting all of my gear back in my pack I took some pics of the sunrise and had another cup of coffee and a quick bite to eat. It was then time to head down the trail and get back to the car.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965762/37555086945_b1550c6b16_k.jpg?classes=caption "Awake before first light making some breakfast.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965760/38411726942_71619eca28_k.jpg?classes=caption "Not much of a sunrise but the clouds were interesting.") {.center}

I hopped on the Virginia Highlands Horse trail (VHHT) instead of taking Pine Mountain trail since they parallel each other. The VHHT is definitely more scenic and I wanted some more photos of the sunrise. The warm glow of the sun was quickly extinguished by a layer of grey clouds though. After a quick jaunt, the trail intersects Lewis Fork trail which leads to Cliffside trail. The descent down Cliffside is, as the name implies, steep and the trail basically goes straight down the mountain. It's the quickest way off of the ridge though.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965763/37726827874_a1b670e838_k.jpg?classes=caption "Back on the VHHT.") {.center}

Cliffside eventually hooks up with Lewis Fork again and when it did, I took a left to follow the topography back up hill a bit to Lewis Fork creek. Then the trail goes slightly uphill until it reaches the Lewis Fork spur that heads back up the ridge to Mount Rogers trail. At this point I was really trying to break a land speed record so that I could have as much time as I needed for my bonus trail run. 

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965763/38410301462_8ab14820a2_k.jpg?classes=caption "Hooking up with Lewis Fork trail back down in the valley.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,f_auto,w_1600/v1555965762/38411556822_6818e79bf9_k.jpg?classes=caption "Looking back down the boardwalk to the trailhead.") {.center}

The hike back down Mount Rogers trail was a breeze and pretty soon, I was back at the road and then the parking lot. My plan was to get back to the trailhead by 9:00 so that I could start my run by 10 and I was right on schedule.

### BONUS: TRAIL RUN
#### Mileage: 6.66 | Gain: 1198 ft | Loss: 1191 ft

Honestly, this is the part of the trip that I was most looking forward to. I had only once combined trail running and backpacking before on a trip to Black Balsam with my buddy Chris a few years ago and I wanted to do it again except better and for a longer distance. The last time I visited Mount Rogers, I noted some trails that went up the other side of the valley from the trailhead where I had parked and that peaked my interest. I looked at maps and created a loop that ascended Grave Mountain via trails and forest roads and then utilized the famous Iron Mountain trail to hook up with another set of forest roads that looped back around to the car.

The climb up to FR 828 utilized Flat Top trail and was short and steep. The forest road continued up the grade slowly until it reached the crossing of Iron Mountain trail (IMT). I followed IMT along the spine of Grave Mountain to where it crossed an unnamed forest service road that eventually led back to 828. Once back on 828 I followed the same route back to the car.

This trail running loop was just a teaser as to what's possible with this network of trails and forest roads. In the true spirit of Ueli, I am already brainstorming ideas for my next trip. Trails I could link up, possible places to camp, gear that I'll use for said adventure... so many possibilities all coming together in a plan to be executed in perfect alpine style. This weekend "project" capped off an especially crazy year and as I partook in my own selfish adventure, I reflected back on lessons learned and the loss of a hero then looked forward to things that I want to do better next year. There will be more adventures to come in 2018!
