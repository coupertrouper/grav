---
title: 'Blue Ridge Marathon'
subheading: 'By the time race day rolled around, I had logged 340 miles with 23,661ft. of elevation gain and a total of 62 and a half hours in the 16 weeks building up to this race. Would it be enough? I would soon find out.'
header_image: race_3935_photo_60413236.jpg
date: '10-08-2018 09:00'
publish_date: '10-08-2018 09:00'
author: 'Scott Cooper'
taxonomy:
    category:
        - Running
    tag:
        - 'Race Reports'
---

A monumental challenge and a new place. One of the reasons I do long distance endurance events is to challenge myself. But another one is to go to new places, within myself, that I haven't explored yet. This year’s Blue Ridge Marathon definitely checked both boxes. It was both physically and mentally one of the most challenging things I’ve ever done. If you’ve never heard of the Blue Ridge Marathon before, the race directors market it as America’s toughest road marathon because of its 7,430 feet of elevation change (gain/loss combined). It climbs and descends 3 of Roanoke’s more prominent mountains—first, Roanoke Mountain, second, Mill Mountain, and third, Peakwood. Along the way racers are treated to beautiful, sweeping views from the overlooks along the Parkway and at the tops of the major climbs. When the course isn’t climbing, it winds its way through neighborhoods and along the Roanoke River Greenway. It’s a really unique course and makes for a very scenic day of racing to take your mind off of the suffering.

This being my first road marathon (wait, what?!), I had nothing to compare it to. So I can’t vouch for the race's boastful marketing. But I can say that it was one of the hardest, most challenging things I’ve ever done under my own power. Of all of the endurance feathers in my cap, I would rank this somewhere in the realm of summiting Mount Rainier. In terms of the training, mental prep, strategizing and the actual event on race day, it definitely ranks high on the list.

## TRAINING

My training plan was pretty simple. Follow the basic rule of not running too much too soon and gradually add more and more elevation gain after establishing a good base of flat miles. I loosely followed the FIRST training plan developed by Bill Pierce and Scott Murr at Furman University back in 2003. I know there are probably newer, more involved training programs out there but I liked the approach of this one out of all the research I did way back in December when I was registering for the race. The core principles of this training method revolve around maximizing potential, training efficiently, avoiding injury, setting realistic goals and overall enjoying running and training. It is geared towards first time marathoners and can be tailored for folks who want to PR. I also have a 2 year old and a spouse who was simultaneously training for a different race so the 3 run per week training plan seemed like it would be a good fit for my life. By the time race day rolled around, I had logged 340 miles with 23,661ft. of elevation gain and a total of 62 and a half hours in the 16 weeks building up to this race. Would it be enough? I would soon find out.

### MILE 0 TO 2.7

The weather couldn’t have been better. Temps were in the lower 50’s and the sky was beautiful and clear, accompanied by a gorgeous sunrise. The gun went off at 7:35am and myself along with the giant throng of runners started our shuffle towards the gate. I positioned myself with the 4:45 pacer knowing that I would want to begin the first leg of the race conservatively. The shuffle turned into a jog as we made our way down Jefferson Ave. towards the first big climb of the day, Mill Mountain. The big group turned left onto Walnut Ave. and as the road started to ascend sharply, the pack started to thin out drastically. I settled into a nice 10 minute per mile pace as the slow grind up to the gap continued. I crossed over the checkpoint at mile 2.7 and was feeling really good after that initial warmup climb.

### MILE 2.7 TO 5.2

The full marathon course continued along Mill Mountain Parkway while the half course parted ways and headed up to the Mill Mountain Star. The pack had thinned out even more at this point and I was feeling solid with my pace and decided to stay in the 10 minute groove I had found. I could have cranked it up a bit but chose to stick to what my coach (a.k.a. My amazing and supportive wife) and I had discussed about pacing strategy. The Parkway’s rolling elevation profile continued on and I mentally steeled myself for the climb up Roanoke Mountain. I was holding steady with the 4:45 pacer and felt like it was still a casual pace, which I felt good about. As I approached the beginning of the Roanoke Mountain climb, my cheering section of my son and my wife were standing there clapping for me which was a very welcomed sight.

### ROANOKE MOUNTAIN LOOP: MILE 5.2 TO 8.9

In training I had climbed the notorious Roanoke Mountain several times so I knew what to expect and where to run vs. walk. The climb starts off pretty steep, levels off a few times then there is a false summit and then another short pitch to get to the very top. I mixed up my walking with jogging where appropriate all the while sticking with the 4:30 and 4:45 pacers. There was a water stop at the first overlook and I filled my my bottle, grabbed a banana slice and checked out the amazing view of the valley down below. Then the road leveled off at the false summit followed by the final pitch to the very top. After crossing over the true summit, I downed a Huma gel, grabbed a couple of pretzels and a splash of Scratch then began the long descent back down to the Parkway. Again, I kept my pace dialed back because of the length of the downhill section. I did not want to burn out my quads that early. A small group formed along the descent which helped to pass the time and before I knew it, we were back down to the Blue Ridge Parkway.

Back on the Parkway, the group began to break apart and the course looped back to Mill Mountain Parkway where my small cheering section was again waiting for me. I dropped off my arm warmers with my coach and her assistant then headed on, mentally fueled by the positive morale boost.

### CLIMB TO THE STAR: MILE 8.9 TO 12.6

Having conquered Roanoke Mountain with a comfortable pace, it was time to crank it up a bit and do some real work. I stopped at the aid station to fill up my bottle and then got down to business, slowly turning my pace up to 9:30, then 8. After holding 8 for a couple of miles I hit 7:30 going down the final descent before the beginning of the climb back up Mill Mountain. I tried to maintain a faster pace through this climb and settled in the 10:45-11:00 range… not fast by some standards but enough to pick off a few other racers on the way up. I focused on maintaining that pace and tapping out a rhythm with a shorter stride. Close to the top I found a bit of extra motivation when the Discovery Center came into view and hit 9:15 before stopping to refill at the Mill Mountain Star, then quickly got back to the task at hand--the long descent down the Old Road.

### MILE 12.6 TO 17

The summit of Mill Mountain is almost at the halfway point of the race and the long descent back down to Walnut provides a good chance to mentally regroup and make up a little time. I refueled with another gel and some Gu Chomps then began thinking about Peakwood while maintaining a 7:30-ish pace going down the mountain. Once at the bottom, gravity stopped working its magic and my legs said something to the effect of, “C’mon! We have to do work now?!” I was starting to feel the miles a bit. Even though this section was flat, the effort it would take to finish the race was sinking in. Better to go through a rough patch on a flat section than on Peakwood. I put my head down and just focused on the work of maintaining an 8:30-ish pace. The flat section came to and end and the course wound its way up Broadway and then onto Avenham, a long and steady upwards grade followed by a short downhill and then, Peakwood. After navigating through South Roanoke, the course hits the base of Peakwood Ave. and waiting there for me was my cheering section which was a much needed morale boost. I stopped an gave my son a kiss on the forehead and they cheered me on as I approached the aid station for a bottle refill and a banana. Then, the course shot straight up.

### MILE 17 TO 22.2

Peakwood. If you live in Roanoke or have run this race before, you know its name. It’s a beast of a climb with little letting up. Lots of switchbacks and steep sections and if you look up in some spots, you can see the road winding up the mountain which is definitely a rain on your parade if you’ve already run 17 or so miles at race pace. I focused on maintaining a short stride, tapping out the rhythm and mentally blocking out the distance of the climb. I kept telling myself that the discomfort would only last a little while longer and pretty soon, I was at the top. I handed my water bottle to the aid station volunteer for a refill, grabbed a peanut butter and jelly sandwich slice and then got back on course. What I thought would be the hardest part of the race was over.

I began the descent of Rosaline Ave. and tried not to go any faster than 7:30. When I got to the bottom, there was another aid station which had small shots of pickle juice. Needless to say I partook of 2 mini-chalices of the holy green liquid, knowing that my body might need the sodium boost. My hamstrings were definitely starting to tighten up and I needed to focus on hydration and taking on sodium and electrolytes to avoid cramping.

After the pickle juice aid station, the course followed Jefferson St. to Highland Ave. In my planning, I thought this section would be a place where I could really make up time but my legs were definitely not motivated to make up said time. I was just focusing on not cramping and trying not to yo-yo my pace too much. I would go back and forth between 8 and 9:30 and decided to just back off completely and settle into a nice 10:00 groove. Better to slow down than bonk from bouncing back and forth. At the turn onto Highland, my son and wife were standing and cheering me on--again a much needed morale boost.

### THE HOME STRETCH: MILE 22.2 TO 26.5

What I thought would be the easiest part of the course turned out to be the hardest part. Mainly because of the mileage I already had in my legs and the complete lack of shade. The temperature was rising and the oppressive sun didn’t help. I tried to stay out of a mental rut by focusing on putting one foot in front of the other at a reasonable pace. The long straight section of Highland finally ended and the course made its way over Ghent Hill, then back through the Wasena neighborhood. There were a few hills in this section and I started to power walk them. After a few run-walk-run cycles, the 4:30 pacer glided past me. Dang.

Now it was a battle to just hang on to 4:30. I dug in deep and summoned my inner Yuki Kawauchi and tried to maintain. A small group of us formed and we rounded the corner of 6th and Luck. 9:15. I could see the next turn onto 1st Street. 8:20. On 1st, the course has a slight incline then turns onto Bullitt, the home stretch. 7:40. I could hear the finish line and when it came into sight I could also see my son and wife cheering me in. I crossed the finish line at 4:32’56 feeling exhausted but also very satisfied with my result.

## POST RACE THOUGHTS

I’m very satisfied of my finishing time for my first marathon. I will definitely be coming back in the future to run this race again with the goal of breaking 4 hours. I may try to do another marathon somewhere else but it will be hard to top this event.

### Lessons learned after my first Blue Ridge Marathon:

* Hydrate early and often (including a couple of days before the race).
* Set realistic goals for your first marathon finish time.
* Plan for a lot of elevation gain.
* Enjoy the scenery along the course.
* Have fun and thank the volunteers!

The Blue Ridge Marathon has spoiled me, I think. The awesome course with epic climbs, amazing scenery and a huge amount of volunteer support will be hard to beat. If you’re ever considering signing up for this race, stop considering it and just sign up. You definitely need to get some good elevation training in but if you’re able to find some hills and can put in the time, this race will be a hugely rewarding experience.
