---
title: 'Training Log: Week 8'
subheading: 'Figuring out the magic formula for life, work, training balance.'
header_image: training_wk8.jpg
published: false
---

## 8/19-8/25

## Goals:
Balance. Maintain. Make easy days easy and don't go too big with the final run of this training block. Get to bed by 11 each night.

#### MONDAY
Rest.<br>
**Exercises & Stretching:** Stretch. Pushups (2x15). Toe/calf raises (3x20 of each).

#### TUESDAY
Easy greenway run | 48:00 | 4.8 mi.<br>
Burpees after run | 3 x 10<br>
**Exercises & Stretching:** Stretch. Pushups (2x15). Toe/calf raises (3x20 of each).

#### WEDNESDAY
Rest<br>
**Exercises & Stretching:** Stretch. Pushups (2x15). Toe/calf raises (3x20 of each).

#### THURSDAY
Rest<br>
**Exercises & Stretching:** Stretch.

#### FRIDAY
Lunch greenway run | 40:00 | 3.7 mi.<br>
Temps were in the upper 70's so I decided to do a lunch run.<br>
**Exercises & Stretching:** Stretch. Pushups (2x15). Toe/calf raises (3x20 of each).

#### SATURDAY
Rest.<br>
**Dinner:** Chipotle chicken burrito.

#### SUNDAY
Big run | 16 mi. | 10-ish pace | 1475 ft. of gain<br>
Felt awesome! I had more gas in the tank at the end and was able to push the pace on the flat greenway section of my route. I used a 1L bladder and a handheld bottle as my hydration gear and it seemed to work very well. I liked using the bladder instead of having 2 bottles up front. It meant that I only had to fill up 1 bottle at the top of Mill Mountain and the bladder lasted 14 miles. I can see this being my race hydration strategy but I will need to get a bladder that's more easily refillable (I just attached my Platypus hose to a small Platypus bottle normally used for backpacking).

## Take Aways

#### Total Mileage: 24.5 mi.

This week went really well from a life/work/trianing balance standpoint. It really helps when the weather is slightly cooler as it opens up the possibility of a lunch break run instead of trying to fit it in after work. I put in big mileage this week so next week will need to be easy to provide my legs some much needed rest. We managed to get to bed at a reasonable time most nights this week and hope to continue the trend. Sleep. Is. Good.