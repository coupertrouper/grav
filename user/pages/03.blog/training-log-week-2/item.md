---
title: 'Training Log: Week 2'
media_order: training_wk2.jpg
subheading: 'Second week of training for MMTR.'
header_image: training_wk2.jpg
published: false
date: '15-07-2019 12:34'
publish_date: '15-07-2019 12:34'
visible: true
---

## 7/8-7/14

## Goals:
Continue to build fitness and adjust to adding a third run into my routine and find balance between fitness goals and life responsibilities. Stay healthy. Get good sleep. Deal with ankle and knee issues.

#### MONDAY
Rest.<br>
**Exercises & Stretching:** Pushups (2x10), normal stretching routine<br>
Walked on the greenway during lunch (2 miles).

#### TUESDAY
No run today. Broken sleep affected my motivation/ability to wake up for an AM run. I suck at waking up early. Maybe someday I'll be a morning person.<br>
**Exercises & Stretching:** Pushups (2x10), normal stretching routine<br>
Walked on the greenway during lunch (2 miles).

#### WEDNESDAY
5:30 am run | 20:00 in the neighborhood easy jog<br>
Saw Dr. Bolin and got my right ankle and knee adjusted. He used the "A" word... as in arthritis, to describe what might be going on with my ankle. He ordered X-rays to investigate further.<br>
**Exercises & Stretching:** Normal stretching routine

#### THURSDAY
5:30 pm run | 37:00 on the greenway easy pace | 3.63 mi<br>
**Exercises & Stretching:** Normal stretching routine<br>

#### FRIDAY
Rest. Got X-rays done on my right ankle at Insight. Time to wait and see...<br>
Chipotle for dinner!<br>
**Exercises & Stretching:** Pushups (2x10), Normal stretching routine, Warrior 2 and 3

#### SATURDAY
Rest and house projects.<br>
**Exercises & Stretching:** Pushups (2x10), Normal stretching routine, Warrior 2 and 3, lying side scissors L & R

#### SUNDAY
7 miles | easy pace | 160' Gain<br>
**Long Run:** Awaiting news about my ankle, I chose to take it easy and keep things flat. The run was hard and I felt a little discomfort in my ankle. Dr. Bolin did say that it might be uncomfortable for a few days. So, not too concerning yet. Felt tired early in the run. Probably because of the burger I ate the night before. I've never done well with red meat as fuel the day before.

## Take Aways

Mentally dealing with the possibility of arthritis in my ankle is tough. I've got so many questions but need to wait patiently to hear back from the doc and not go down the Google wormhole. In the mean time, I'll continue to take it easy on the ankle and keep things flat, staying mostly off the trails. May be time to work more cycling into the mix.