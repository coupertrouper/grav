---
title: 'Training Log: Week 6'
subheading: 'No ankle arthritis. Full speed ahead! Within reason.'
header_image: training_wk6.jpg
published: false
date: '12-08-2019 12:32'
publish_date: '12-08-2019 12:32'
visible: true
---

## 8/5-8/11

## Goals:
Finish up the first block of training with a big effort. Get word back from doc about ankle. Work/Family/Training balance.

#### MONDAY
Rest. Easy greenway hike.<br>
**Exercises & Stretching:** Stretch.

#### TUESDAY
Rest.<br>
**Exercises & Stretching:** Stretch.

#### WEDNESDAY
Doc called back... NO ARTHRITIS! Party!<br>
Easy greenway hike | 1.4 mi.<br>
Greenway run | 45:00 | 4.5 mi.<br>
**Exercises & Stretching:** Toe raises. Calf raises. Stretch.

#### THURSDAY
Rest.<br>
**Exercises & Stretching:** Stretch.

#### FRIDAY
Easy greenway run | 45:00 | 4.37 mi.<br>
**Exercises & Stretching:** Toe raises. Calf raises. Stretch.

#### SATURDAY
Hardcore rest.<br>
**Dinner:** Quinoa bowl with grilled chicken.

#### SUNDAY
Big run | 13.75 mi. | 1128 ft. of gain<br>

## Take Aways

Big news is that my ankle, at least structurally, is in great shape! No arthritis. It seems to be something muscular or biomechanical that can hopefully be corrected with orthotics. I purchased some Sole inserts and will have them heatmolded this week at the doc office. Everyone is healthy so training schedules for both my wife and I were easy this week. We both managed to fit in all of our workouts. Also... Quinoa bowls are an awesome fuel source.