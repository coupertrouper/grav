---
title: 'Training Log: Week 5'
subheading: 'Long runs in Washington D.C. and a backpacking trip.'
header_image: training_wk5.jpg
published: false
date: '05-08-2019 12:33'
publish_date: '05-08-2019 12:33'
visible: true
---

## 7/29-8/4

## Goals:
Sustain fitness levels and maybe increase strength training. Keep up the morning stretch and exercise routine. Be a good dad and husband.

#### MONDAY
Rest. Still in D.C. for the AEA conference.<br>
**Exercises & Stretching:** Stretch.

#### TUESDAY
Big run on D.C.'s Arlington Trail!<br>
6:30 pm | 2:01 easy pace with stops at monuments | 10.7 mi.<br>
**Exercises & Stretching:** Stretch.

#### WEDNESDAY
Rest. Drive home from D.C.<br>
**Exercises & Stretching:** Stretch.

#### THURSDAY
Rest.<br>
**Exercises & Stretching:** Stretch.

#### FRIDAY
Rest.<br>
**Exercises & Stretching:** Pushups (2x10), stretch.

#### SATURDAY
Backpacking Three Ridges | 10.76 mi. | 3,461 ft. of gain<br>

![](https://live.staticflickr.com/65535/48489679702_94288d5eba_k.jpg?classes=caption "Group shot at the trailhead.")

![](https://live.staticflickr.com/65535/48489509811_97aa5bc443_k.jpg?classes=caption "At the overlook after the summit on Three Ridges.")

![](https://live.staticflickr.com/65535/48489509946_71f5a7502a_h.jpg?classes=caption "Three Ridges Wilderness boundary sign.")

#### SUNDAY
Backpacking Mau Har | 4 mi. | 1512 ft. of loss<br>

## Take Aways

Life is getting busy and I'm grateful to have a supportive partner that helps make it all possible. A conference and a backpacking trip all within 1 week is a bit tough for everyone but ~~we~~ my amazing wife managed to make it work. Ankle seems to be holding steady. Still waiting on word from the doc about my x-rays.