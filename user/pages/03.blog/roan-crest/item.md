---
title: 'Roan Crest'
subheading: 'A collection of photos from a recent trip to hike the Roan Crest, a section of the Appalachian Trail that traverses the expansive massif of the Roan Highlands.'
header_image: 39343933222_c5d445b148_h.jpg
date: '11-11-2013 09:55'
publish_date: '11-11-2013 09:55'
visible: true
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - 'North Carolina'
---

5 backpacking buddies, 1 shuttle ride to the trailhead, some whiskey, endless epic views, 1 tent with hundreds of tiny little ember holes, 3 days and 2 nights, 15 miles, lots of coffee, and lots of good memories. A collection of photos from a recent trip to hike the Roan Crest, a section of the Appalachian Trail that traverses the expansive massif of the Roan Highlands.

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345432/25504406958_21f31117d5_h.jpg?classes=caption 'Group shot before the traverse of the Roan Crest.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345436/39373394641_d82d423878_h.jpg?classes=caption 'Asending Roan High Knob, we found the frost line.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345436/25504405618_5d1f9cdb8f_h.jpg?classes=caption 'Mark checking out the views from Roan High Bluff.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345437/25504404478_dde404058c_h.jpg?classes=caption 'Descending the A.T. into Engine Gap.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345433/39373389961_131d77f10d_h.jpg?classes=caption 'Sunset from Grassy Ridge Bald. Almost no need for a caption.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1600/v1557345436/25504403088_c7a4ea9424_h.jpg?classes=caption 'Our tents set up on Grassy Ridge. If you know, then you know.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345431/25504402388_cdd68f56b7_h.jpg?classes=caption 'Sunrise from the campsite. The profile of Grandfather Mountain is visible.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345431/39373388951_c79a6a442b_h.jpg?classes=caption 'Emerging from my tent to prepare some coffee.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345432/39373388621_96259f8a93_h.jpg?classes=caption 'Back on the trail heading towards Big Yellow Mountain.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345432/39373386971_11da1da7ba_h.jpg?classes=caption 'Fisher making it look easy.') {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/v1557345432/39373385841_c35c4f3977_h.jpg?classes=caption 'Group photo at the beginning of day 3 just before getting back on the trail to head out.') {.center}