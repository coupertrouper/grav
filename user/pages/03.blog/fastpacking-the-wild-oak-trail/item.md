---
title: 'Fastpacking The Wild Oak Trail'
subheading: 'After a few months of lockdown, it goes without saying that we were itching to get out and do some kind of responsible and socially distant fastpacking trip. Wild Oak is the perfect venue for such a trip.'
header_image: twot_header.jpg
visible: true
taxonomy:
    category:
        - Fastpacking
    tag:
        - 'Trip Reports'
        - Fastpacking
        - Virginia
date: '13-09-2020 14:00'
publish_date: '13-09-2020 14:00'
---

2020 was going to be _THE YEAR_. We were going to get some good family camping trips in, maybe do some additional travel, I was going to train for [Mountain Masochist](/blog/mountain-masochist-trail-run-50-miler) again, maybe even fit some more races in there. It started off great with family visits through the holidays and then a really good [backpacking trip to Cranberry Wilderness](/blog/cranberry-wilderness-adventure) with my buddy Chris who I've run a couple of ultras with. Just as we were talking about doing another backpacking trip, Covid-19 happened. At first we thought it would only last a few weeks but boy were we wrong. At my house, we retreated from our lives for months and reassessed the situation on a weekly basis. Weeks turned into months and now, here we are. One of the few good things about all of the Covid mess is that you can still get outside (following social distancing guidelines, of course).

After a few months of lockdown, it goes without saying that I was itching to get out and do some kind of responsible and socially distant backpacking trip. In early August, Chris, who has run a 10K every day since around the beginning of the pandemic (!!!), texted and asked "If we went fastpacking at least 10K each day would that keep the run streak alive?" My virtual ears perked up... fastpacking?! Yes, absolutely it would. I suggested we fastpack Wild Oak Trail since we had A) Never fastpacked together before and we'd [been to Wild Oak previously](/blog/wild-oak-trail-adventure) so it would be a safe first attempt and B) It was a great loop with enough ups and downs to ensure some quality [Type 2 fun](https://www.rei.com/blog/climb/fun-scale?target=_blank).

## Packing

Fastpacking is trail running mixed with backpacking. Gear selections revolve around the core principle of being as light as possible so that you can move quickly (i.e. run) but also be as safe as possible (i.e. necessary warmth). The staple of a good fastpack gear list is the backpack itself. I have dabbled in ultralight gear so I've got some experience with going light and fast. The pack I chose for this trip was the Osprey Talon 33. If I did more fastpacking (which I definitely plan to), I'd invest in a smaller pack like the Ultimate Direction Fastpack 25 or 35 (which Chris used for this trip). Being an older design, the Talon 33 doesn't have the shoulder strap bottle pouches which I've grown to love on my UD Jurek race vest. To alleviate this access to water problem, I chose to go with a hydration bladder so that I could have water on demand. More on that later.

Shelter and sleep systems are also big components for fastpacking and for this trip I elected to go with a tarp bivy combo. I used the 10x8 Sanctuary Siltarp from Paria Outdoor and a Mountain Laurel Designs ultralight bivy. For sleeping, I have a Western Mountaineering HighLite bag rated to 35° F and paired that with a Sea to Summit liner to keep the bag cleaner and provide extra warmth should I need it. The sleeping pad I chose was the Z-lite pad from Cascade Designs. I probably should have/could have chosen to use my Nemo torso length inflatable pad but I was aiming for a more simple kit with less points of failure. You can't go wrong with a foam pad but you definitely sacrifice comfort. The shelter/bivy combo packs down to about the size of a footlong submarine sandwich and the sleep system packs down to about the size of a cantaloupe.

I went as minimal as I could while maintaining as much safety as possible with my other gear choices. For clothing, I had 1 set of running clothes and 1 set of camp clothes. My gear list in the form of photos is below.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/gear_1.jpg?classes=caption "All the things!")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/gear_2.jpg?classes=caption "Close up of random gear item.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/gear_3.jpg?classes=caption "Snacks. Lots of snacks.")

## Planning

Because we had basically done this same route before, we were already armed with the research and maps necessary to ensure our success. Chris plugged the data into GAIA GPS, Garmin Connect and AllTrails and came up with a map (pictured below). I printed the map and plan out on TerraSlate paper using a laser printer, something we always do for every trip incase our GPS devices (my phone with the GAIA GPS app and Chris's watch) fail us. It's also useful to give this information to loved ones so they know exactly where you are at any given moment.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/wild-oak-091220_tripMap-1-1.jpg?classes=caption "The Wild Oak Trail route with a plan to dip down to the river to camp and complete the loop on day 3.")

I probably should have started all of this with talking about the forecast. The weather ultimately decides what gear choices you make when packing. Given that our trip was scheduled for mid-September, we could plan to have warm temps during the day and cool temps at night. Ten days out, the forecast called for rain all day Saturday with a slight chance of T-storms Sunday evening. Temps were predicted between mid fifties at night and low eighties during the day. As we got closer to the trip weekend, the rain chances diminished slightly and we rejoiced ever so little, knowing that the weather could still change with little notice.


## Day 1
### 10.79 Mi | +3537' GAIN | -2772' LOSS

So, my adventure on day 1 started at the house. In my basement to be exact, right before I was about to get in the car to leave. As I flipped on the light switch to my basement, happy with how efficient the morning routine had transpired, I discovered my fully packed backpack in a puddle of water. My 1.5L hydration bladder had completely emptied itself all over the floor and probably into aforementioned fully packed backpack. First, thank goodness for vinyl plank flooring. Second, "DANGIT!" Efficient morning: derailed. I furiously unpacked my bag to assess the damage. Thankfully, my sleeping bag didn't get that wet but my down pillow was completely soaked. I reassessed and decided to use my down puffy coat (which I originally hadn't packed) as a pillow. I also re-reassessed and put my sleeping bag in a dry bag (which I probably should have done to begin with). Lesson learned, knucklehead. I just grabbed everything else and threw it into the car and plopped my sleeping bag on the front passenger seat hoping the car's A/C would dry it off by the time I reached the trailhead. Crisis somewhat averted.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/totalchaos.jpg?classes=caption "Total chaos.")

Then it was time to get on the road and head to Wild Oak. I said my goodbyes to my 2 boys and my wife and then I hit the road. I stopped in Staunton, VA at a delicious eatery known as the King of Burgers and munched on a chicken biscuit (or, [chicken musket if you're a Brian Regan fan](https://youtu.be/KtjH8HojEBY?target=_blank)) and then moseyed on up to the trailhead. I arrived a little earlier than our agreed upon meet up time which gave me a few extra minutes to re-pack from my aqua adventures earlier.

Chris arrived a short while later and we took our requisite before photo then got on the trail at 11:30am. We began jogging along the trail and given that it was pretty wet from all the rain, we had our heads down paying attention to the rocky path so we didn't trip. But because we were paying attention to the ground and not looking up, we missed our first turn. After a few minutes we realized our mistake, turned around and found our turn then began the long ascent up Little Bald.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_1.jpg?classes=caption "National Forest sign at the trailhead.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_1a.jpg?classes=caption "The fun before the fun begins. Photo by Chris.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_2.jpg?classes=caption "Righting our wrong.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_3.jpg?classes=caption "The start of the first big climb of the day.")

After a really long time climbing into the low clouds, we made it to the top. The summit was completely socked in and visibility was about 100 feet. We walked down the forest road at the top where there is normally a nice view only to see more cloud. We couldn't complain though. The temps were cool and the humidity in the air was refreshing after a long climb. We had a snack and some fluids then it was time to make our way down the mountain to Camp Todd, our destination for the evening. Shortly after starting the downhill section, we bumped into a trail runner headed up the mountain the opposite way. We ducked to the side to maintain our distance of socialness and continued downward. After a long descent, we reached the forest road and then jogged our way to Camp Todd. We scouted around a bit and found the least worst campsite at the muddy/trashy campground between a few fire rings and several piles of horse pooh. 

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_4.jpg?classes=caption "Climbing higher into the cloud.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_5.jpg?classes=caption "Sign at the summit of Little Bald")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_6.jpg?classes=caption "Our non-existant view of the non-existant view.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_7.jpg?classes=caption "On the way down to Camp Todd. Photo by Chris.")

I set up my tarp/bivy and Chris set up his hammock and just as we finished, it started to rain. Not cats and dogs. More goldfish and hamsters. We settled in under his front porch (a.k.a. hammock tarp) and boiled some water for our probably not-very-sustainably produced, ungourmet dinners of dehydrated backpacking food from a company who's name rhymes with Fountain Mouse. Given the fact that it was raining and there wasn't much else to do, we decided to hang our bear bag and call it an early night. Day 1 done.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_8.jpg?classes=caption "Tarps and hammock set up at Camp Todd. Photo by Chris.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day1_earl.jpg?classes=caption "Earl the stump watching over us.")

## Day 2
### 15.31 Mi | +3163' GAIN | -3770' LOSS

We awoke at sunrise and started the morning routine of getting the bear bag unhung, disassembling our campsite, and most importantly consuming coffee and breakfast. Chris waved his magic wand and he was instantly packed. I on the other hand, fumbled about like a toddler stuffing things into things. After a few sips of coffee and some oatmeal, I stuffed more things into things and I was packed. Then it was time to hit the ground running. Literally. We jogged up the gravel road a bit until we found the trail intersection, then began the long ascent of Big Bald.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_1.jpg?classes=caption "Oatmeal and coffee breakfast.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_2.jpg?classes=caption "Up, up, and up. Photo by Chris.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_4.jpg?classes=caption "Nearing the top of Big Bald.")

Like the previous day, temps were cool and it was very humid. Perfect climbing weather. The ascent was steep at first, leveled off a bit, became more steep and eventually turned into a nice slow grade uphill to the top. The vegetation changed along the way as we increased elevation. Near the top there were a ton of alpine ferns. Amongst said ferns, we saw a perfect campsite (if camping at the summit is your thing). Though, there was scant a water source nearby. So that'd be a minor issue depending on one's logistical situation. Anywho.

We noted the summit and then it was time to make our way downhill to Braley Pond Road. Along the way, we discovered a side trail that led to a creek that may or may not be a reliable water source depending on the rainfall situation. We had planned on tanking up along Braley Pond Road but decided to fill up at the small creek instead. After getting some H with 2 O's (not 1), we proceeded down the magic carpet ride to the road. OK, on paper it was a magic carpet ride. My knees were reminding me that I am not 25 years old anymore so it was more like an unpleasant kibbled plunge. Just kidding. This is fun!

At the road, we had a brief snack and rehydrated a bit then got back on the trail to climb Hankey Mountain. Every time I see the word Hankey, I can't help but remember Mr. Hankey from South Park. #memories Thankfully it was still cloudy and cool and there was no Mr. Hankey. Instead, the fog was lifting and occasionally we saw some brief patches of sun as we made our way up the mountain.

After ascending 1.75 miles from Braley Pond Road, we approached the trail sign  for what seems like the intersection of Wild Oak and Dowell's Draft. Last time we were here, we made the mistake of going straight which took us straight up and over the mountain. It's a weird intersection because it isn't on any map we could find. Chris said "Let's go right, I'm not making that mistake again. If I'm wrong, I'll take full responsibility." And so we went right and eventually came upon the actual Wild Oak/Dowell's intersection. Chris was right! And our legs were eternally grateful.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_3.jpg?classes=caption "It seemed counterintuitive but we went right here. Photo by Chris.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_3b.jpg?classes=caption "Dowells Draft intersection sign.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_3a.jpg?classes=caption "Looking the opposite direction from the previous photo of the sign.")

We ascended another bit of trail to get to a rock outcropping near the top of Hankey and stopped for a brief snack and to send a text/call our families to check in. Visibility was not that great but we did get a peak of the valley down below which, until that point, had been shrouded in white. After that brief respite, we got back to the task of climbing the final bit of Hankey and from the summit, it was 5.5-ish miles of down hill to the campsite for night 2.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_6.jpg?classes=caption "Sending a proof of life text to our families.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_7.jpg?classes=caption "The view from the rock outcropping on Hankey.")

As we descended, the trail eventually turned into crappy jeep road then we hit a gate and the trail turned into a sloppy gravel/muddy road complete with people out enjoying said sloppy gravel/mud with their 4WD trucks. After more elevation loss and much more slop, we eventually made it to the intersection with FR425. Speaking of slop, I was starting to feel the slop of a bonk coming on because I hadn't ingested enough calories. Chris gave me some Clif Bloks to nurse the bonk. He led the way down FR425 and finally we arrived at the bottom where it intersects with FS95. We jogged a bit and located North River Trail which bypasses North River Campground and follows the river. After 2 river crossings we found our planned campsite and triumphantly dropped our packs to stake claim to our home for the night. I set up my tarp/bivy and Chris found some trees to set up his hammock. Once we got the shelters configured, we soaked our legs in the anti-inflammatory goodness of cold river water for the better part of an hour. It had been an amazing day of fastpacking.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_5.jpg?classes=caption "Ridgeline hiking in Appalachia.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_8.jpg?classes=caption "Muddy jeep road.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_9.jpg?classes=caption "Muddy feet jeeps. Feeps. Or Jeets.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_10.jpg?classes=caption "Chris pulling ahead.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_11.jpg?classes=caption "My Paria Siltarp.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_12.jpg?classes=caption "Chris's Kammock tarp/hammock.")

After soaking it in, we had some dinner, made a fire and played Jose's (Chris's son) homemade backpacking games. One was a collapsible mini-cornhole game and the other was a shelter building challenge where he provided 2 mini-paper people and we had to build a shelter for the people that would survive the night.

We enjoyed the games (the mini-cornhole game was very challenging, btw) and the fire and eventually, the sun began to set and we hung the bear bags and headed to bed. Chris calculated that in order to keep his run streak alive, he would need to run 1.8 extra miles in addition to our hike/run out to the car on Day 3. He announced that he'd be waking up early to do that and I said I'd make my decision in the morning based on how my legs/chafing issues were doing. Time to call it a night. What a day!

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_13.jpg?classes=caption "Nice firering at this campsite.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_14.jpg?classes=caption "Mini-Cornhole courtesy of Jose. Photo by Chris.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_15.jpg?classes=caption "Build-a-Shelter courtesy of Jose. Photo by Chris.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day2_16.jpg?classes=caption "Enjoying the fire before heading to sleep.")

## Day 3
### 4.39 Mi | +75' GAIN | -292' LOSS

I heard Chris stirring in his hammock around 6, getting ready for his morning run. He called out and asked I wanted to join and I said that I would refrain. I definitely wasn't feeling it. The night before, Chris measured the distance between the 2 creek crossings near our campsite and determined he'd have to run back and forth until he reached 1.86 miles. He did that while I got the bear bag down and started the morning routine.

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day3_1.jpg?classes=caption "Proof that Chris got his extra miles in.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day3_2.jpg?classes=caption "Boiling water for coffee and breakfast.")

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day3_3.jpg?classes=caption "Breakfast by the river.")

Chris finished up his run and then we had coffee and breakfast, all the while, breaking down the campsite and getting things packed up. Around 8:20, we got back on the trail to enjoy the mostly downhill jog to close the Wild Oak Trail loop we had created. Along the way, we had 8 more river crossings to contend with. It was a great way to start the morning and the cold water helped with inflammation from the previous day's adventure. After the last creek crossing we picked up our pace and quickly made our way to the suspension bridge that crosses the river about 0.5 miles from the cars. We made our way over the bridge and through the woods to the end of our route. A few moments of dodging rocks and roots later, we made it back to the trailhead where we started. 

![](/user/pages/03.blog/fastpacking-the-wild-oak-trail/day3_4.jpg?classes=caption "1 of 8 river crossings.")

## Thoughts & Lessons

Fastpacking is very rewarding because of the amount of terrain you can cover compared to backpacking. Don't get me wrong, backpacking is awesome. But fastpacking is in its own special category of Type 2 fun with a big payoff at the end. We managed to keep a 19:46 pace (about 3 mph) with stops and a 16:19 pace without stops (closer to 4 mph). I'm pretty happy with that.

Along with his [write-up](https://y2kemo.com/2020/09/fastpacking-wild-oak-trail/?target=_blank), Chris put together a video.

<div class="resp-container">
<iframe class="resp-iframe" style="margin: 50px auto;"width="1136" height="639" src="https://www.youtube.com/embed/aLzNuL9mMA0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

### SOME LESSONS LEARNED:

1. Sharing gear is the way to go. When hiking with a friend/group, splitting up gear really cuts down on weight and redundancy. We did that for things like the water filter and the stove and it helped us be more efficient with our packing and our weight.
2. Plan to be wet. Constantly. Our trip's cool and damp weather was partly to blame for this. But if it were hot out, I would have been sweaty and would have had the same set of problems. I wish that I had packed running shorts, socks, and underwear that dried quicker because I suffered some pretty bad chafing. I'll be investing in some different materials for the next fastpacking trip as well as bringing the BodyGlide.
3. Be in good physical shape. I'm glad I had ramped up my mileage and elevation gain prior to going on this trip. After a summer of mainly cycling, I picked up my running game a bit to be prepared for this trip and it proved very beneficial to get up and down those mountains with a weighted pack. If I did it again, which I will, I'll be sure to train for it so I can be in even better shape.

Fastpacking is so fun and I will definitely do it again. Maybe even do Wild Oak Trail again since it's such an easy place to access for me. There are a lot of other trails in the area that would be fun to explore like Ramsey's Draft or even Reddish Knob. Lots of possibilities there. In the mean time, if you're interested in this area, check out the [North River Ranger District's website](https://www.fs.usda.gov/recarea/gwj/recarea/?recid=77723?target=_blank) for more info.