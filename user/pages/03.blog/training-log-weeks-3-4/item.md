---
title: 'Training Log: Weeks 3-4'
subheading: 'The challenges of balancing life, work, and training.'
header_image: training_wk3-4.jpg
published: false
date: '29-07-2019 12:33'
publish_date: '29-07-2019 12:33'
visible: true
---

## 7/15-7/28

## Goals:
Keep building fitness. Figure out ankle issues. Back off a bit on hard trail runs and use cycling to maintain cardiovascular fitness while the ankle stuff gets sorted out.

#### MONDAY
Rest.<br>
**Exercises & Stretching:** Pushups (2x10), stretch<br>
Walked on the greenway during lunch (2 miles).

#### TUESDAY
After work greenway run | 45:00 easy pace<br>
Used Superfeet on right foot. Felt strange.<br>
**Exercises & Stretching:** Pushups (2x10), stretch<br>

#### WEDNESDAY
Rest.<br>
Lunch greenway walk (1 mile).<br>
**Exercises & Stretching:** Stretch

#### THURSDAY
Rest. Sick kiddo.<br>
**Exercises & Stretching:** Pushups (2x10), stretch<br>

#### FRIDAY
Rest.<br>
**Exercises & Stretching:** Pushups (2x10), stretch, yoga

#### SATURDAY
Cycling | 34.35 miles, 4110 ft. of gain<br>
**Exercises & Stretching:** Stretch

#### SUNDAY
Neighborhood jog | 20:00<br>
Superfeet on right shoe.

#### MONDAY
Rest. Sick day. Got whatever my son had.<br>
**Exercises & Stretching:** Stretch

#### TUESDAY
Rest.<br>
**Exercises & Stretching:** Stretch

#### WEDNESDAY
Greenway run | 40:00 easy pace<br>
Superfeet on right foot continues to feel strange. Going to stop wearing them.<br>
Lunch greenway walk (1 mile).<br>
**Exercises & Stretching:** Stretch

#### THURSDAY
Rest.<br>
Lunch greenway walk (1 mile).<br>
**Exercises & Stretching:** Pushups (2x10), stretch<br>

#### FRIDAY
Rest.<br>

#### SATURDAY
Rest.<br>
**Exercises & Stretching:** Stretch

#### SUNDAY
D.C. run | 10 mi. | 2:01 easy pace<br>
Did not wear Superfeet and ankle felt normal.

## Take Aways

Switching over to cycling seems to be good for my ankle. Wearing Superfeet either helps correct my biomechanical issues and it takes time to get used to the feeling of using them or they are doing more harm than good and I should not wear them. Either way, I'm still waiting on word from the doc about my ankle and I should probably wait to see what he says about things. Family ultimately comes first and we all got some kind of bug so that got things out of whack a bit. Back to the normal routine after a few days of rest.