---
title: 'Virginia Triple Crown'
subheading: 'In my search for big fastpacking loops around the valley, I discovered a 3 day, 37-ish mile backpacking loop north of Roanoke. I was immediately curious and intrigued.'
date: '15-09-2013 23:00'
publish_date: '15-09-2013 23:00'
author: 'Scott Cooper'
taxonomy:
    tag:
        - Virginia
        - Fastpacking
---

In my search for big fastpacking loops around the valley, I discovered a 3 day, 37-ish mile backpacking loop north of Roanoke. I was immediately curious and intrigued. I wanted to see if this route could be done in 24 hours or less as a fast & light overnighter. Having run sections of each trail that make up this loop, I was pretty confident I could come close.

Months were spent pining over maps and testing out gear combinations. I purchased a tarp from Oware and began practicing setting it up while out on runs. I ramped up my miles, essentially following a half marathon training program. I frequented hillmap.com and studied the heck out of USGS topo maps, getting familiar with the trails, fire roads, landmarks and mileages between them. Now all I needed was a date to make all of this come together.

The last weekend in September was targeted. A video shoot for work was subsequently scheduled for that same weekend that I couldn’t get out of and I decided to move the trip 2 weekends earlier to mid-September. That might work out better. The date was set.

September 14th finally came around and after much anticipation about whether or not conditions would be right, the weather gods seem to have been smiling down on the Roanoke Valley. With a perfectly clear forecast and temps in the low seventies during the day and mid forties at night, conditions could not have been better. It would be absolutely perfect for a trip like this.

Friday evening before the trip, gear was strewn about the apartment living room. Such a big production for a 6L pack. I put my final packing list together and began checking things off. It was very minimal. The most ultralight pack list I’ve ever put together. Sub ten pounds without water. I got everything organized and stuffed into my Mountain Hardwear Fluid 6 and settled down for the evening.

Early to bed, early to rise.

## START

6:45am. Go time! I clambered out of bed and put the water on while prepping the French Press with coffee grounds. Popped my contacts in, got dressed and collected my gear in the living room as the water began to boil. A quick cup of coffee, a last check of the gear list and a final glimpse at the route. I kissed my wife, she wished me good luck and I was out the door.

On my way down to the Dragon’s Tooth trailhead I stashed a jug of water on Rt. 779 at the start of Andy Lane trail. I stashed another jug of water behind a guardrail where the A.T. crosses 311. These would be my backup water sources if streams were dried up (Siege Style).

I reached the Dragon’s Tooth trailhead at about 7:30am. After getting the pack on and snapping the trekking poles together I was on the trail. My strategy for the day would be to hike fast up the hills and jog along the ridges and the flats. I followed a mountain bike trail that became increasingly overgrown as I advanced along the spine of North Mountain. Approaching mile 9 I was cresting a small rise in the ridge and heard clumsy rustling coming from the top of a tree. Out of the clump of leaves and branches emerged a black silhouette. A bear!

In the 4 or so years that I have been backpacking I’ve never seen a bear. This one was about the size of a small dog. Not really threatening at all. Kind of amusing really. I could have taken him. I just yelled, “Hey bear!” and clacked my trekking poles together and he sauntered off down the mountain.

I waited a few minutes and then resumed walking. About 30 minutes later–another rustling in the trees. Another black silhouette. This one a bit bigger. It clambered out of the tree and hit the ground with a thud. I yelled at it and it ran down the mountain away from me. This one probably could have taken me. Two within one hour!

Walking again I approached the junction of North Mountain trail and Catawba Valley trail, my route down the mountain to my first water stop. As I approached the intersection I heard yet another rustling in a tree not 20 feet from the trail. I paused and another black bear about the size of a medium sized Chow Chow made its way down the tree. This one was a mama. Her cub quickly followed her lead and ran down the mountain away from me. I yelled and clapped, being sure to thoroughly scare both of them away and make my presence known.

4 bears within 3 miles of one another. Unbelievable. I wish I had been lucky enough to snap a photo.

Running on adrenaline and almost laughing at what had just happened, I quickly made my way down Catawba Valley trail. At the bottom I stopped and filled up my water bottles, had a brief snack and chatted it up with a couple that reported to be hung over from the Willie Nelson concert the night before. They offered me a beer but I reluctantly passed and resumed hiking.

## FIRST CROWN

On my way up to Tinker Cliffs, the first of the 3 crowns, I battled a bit of nausea. I was approaching the 14 mile mark having averaged about 21 minutes per mile. Making really good time but starting to feel it. I needed some lunch.

At the top of Tinker I stopped for a photo and a packet of tuna. Feeling better now I got back on the trail and headed down the A.T. along the ridge of Tinker Mountain on the way to McAfee Knob, the second crown. There was a big descent coming down off of the top of Tinker mountain and then the trail rolled up and down for a while along the rocky ridge. Then it flattened out and turned into some nice double track that made its way up to McAfee.

## SECOND CROWN

As I climbed, the crowds got bigger. Many people from around the valley were out enjoying this autumn like afternoon. Something else was getting bigger–the void in my water bottles. I had run out of water. Luckily I had timed water consumption so that I’d be passing a water source near Campbell shelter, just below McAfee Knob. The side trail for the spring was a short distance away from the shelter but when I reached the end of the side trail I was disappointed to find a dried up creek bed. No water here. I didn’t waste any time because I knew I had water waiting at 311 and should continue to move before I got too bad off.

At the top of McAfee the crowds were bigger than I had ever seen. Lots of college kids taking in the views. I snapped a quick photo and had a bite to eat and headed down the mountain, feeling a bit woozy from the lack of hydration over the past hour. For the descent off of McAfee I took the fire road instead of the A.T. It would get me down to my jug of liquid relief quicker.

After a few miles of easy double track, I happily reached 311 and located my water. Sitting there for a few minutes I gulped almost half of the jug and then filled up my water bottles, this time also filling up my 1L Platypus to be used for cooking and coffee in the morning. My Ambit was reaching the end of its battery life so I shut the GPS off to conserve juice and use it as my clock and compass should I need it.

After a few more sips it was back to the trail and onward to Beckner Gap, my intended camping spot for the evening. At about mile 25, I decided it was time for some dinner. There was a great spot to watch the sunset and enjoy some Ramen noodles just before Beckner Gap. I boiled some water, rehydrated and enjoyed my ultralight minimal dinner then it was back to the trail for some more hiking in the twilight.

After a couple more miles it was time for some rest. I set up my tarp, bivy and sleeping bag and settled in with the moon beginning to rise. It was a cool night with a bit of a breeze. A perfect night to be up in the hills.

## THIRD CROWN

The next morning I woke at about 5:30 and got some water boiling for a quick cup of Via. I sipped on it as I did some stretches and dismantled my low impact campsite. Then it was back to the trail as the sun was rising. 1 more crown and 10 miles to go and I’d be wrapping this project up.

Making my way down Beckner Gap and into some cow pastures I saw a couple deer and a grouse. The trail opens up and crosses a few cow pastures on its way to the base of Cove Mountain. After cruising over the rolling pastures, I stopped at a stream and filtered some water into my water bottles in preparation for the final climb up to Dragon’s Tooth.

This one was a doozy. I started off strong making good time at the bottom of the ridge that ascends to the top. But I miscalculated my food intake and as I always tend to do, under packed on calories. I had 2 gels left and a beast of a climb yet to conquer. About half way to the top while scrambling over some rocks I started to get that clammy, cold sweat feeling of hunger and exhaustion after a long effort. I took a swig of water, sucked down my final gel and thought that as long as I maintained my water intake, I’d be OK.

After a long and arduous climb with the occasional pausing to avoid tossing my cookies, I made it to the top. The third crown had been conquered! I took a photo to commemorate the moment,  sipped some water and began my descent.

The out and back Dragon’s Tooth trail is a pretty technical rock scramble. Each movement took concentration given my current state of distracted hunger. I safely made it to the A.T. junction and began heading down the trail to the car, jogging at times with excitement and a huge sense of accomplishment.

At about 11am I reached my car. I snapped a pic of the time on my watch since the GPS had killed the battery the previous day and all I had left was enough juice to keep the watch going. It felt great arriving at the Dragon’s Tooth trailhead coming from the opposite direction that I had set off in the previous morning. I soloed my big summer project, the Virginia Triple Crown, 37 miles in 27 hours. I will make another attempt at doing this route in less than 24 hours in the spring. I know that I can do it much quicker next time.