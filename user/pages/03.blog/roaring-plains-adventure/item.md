---
title: 'Roaring Plains Adventure'
subheading: 'This area is what I imagine Dolly Sods was like 10 years ago. A few words and pictures to describe this awesome trip with my buddy Chris and bro-in-law Craig.'
header_image: DSC_7620_sm_0.jpg
date: '10-04-2017 09:00'
publish_date: '10-04-2017 09:00'
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - 'West Virginia'
        - Winter
---

West Virginia has some amazing backpacking trails along the Allegheny Front in the Dolly Sods area. My buddy Chris and I have done a fair amount of exploring in the northern section of the Sods and in doing research about the area, Chris discovered the Roaring Plains. The Plains lie just south of Dolly Sods and extend down to where the Allegheny plateau comes to a point making for some very interesting terrain. For this trip, our plan was to use South Prong trail to get to the top of the plateau, navigate Hidden Passage trail then hike out on the Canyon Rim Trail. To get back to the cars, we'd use Tee Pee trail, Roaring Plains trail and then descend Boar's Nest trail.

### DAY 1

Mother Nature tends to throw the most curve balls in the winter. We were dealt a major one near the beginning of this trip before we even got to the trailhead. My brother-in-law, Craig, was meeting us in Harmon, WV at a library just off of the main road. With about 30 minutes to go to the meeting spot, we hit a massive snow storm and traffic was backed up because of a semi-truck that was stuck along one of the steep mountain roads. We were finally able to get around the truck and get into Harmon but cell reception was very spotty and we weren't able to communicate very well. After driving around, trying to send each other telepathic GPS coordinates, we finally met up near Laneville and proceeded to the trailhead. 

The weather was cold and a snow storm had just blown through the previous week. We were prepared for harsh conditions but when we arrived, we were all immediately struck by how bad the weather was. The wind was howling through the trees, snow was blowing, the cold was only made worse by the driving wind. Looking at the forecast though, we knew that all we needed to do was survive the first night because the harsh conditions would melt away and we'd be left with sunny and warm for the rest of the weekend.

Chris shot a video of the conditions at the trailhead.

<div class="resp-container">
<iframe style="margin: 50px auto;"width="1136" height="639" class="resp-iframe" src="https://www.youtube.com/embed/OgXfFWVpWwE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
    
This was Craig's first backpacking trip so I helped him get some gear into his pack that I loaned him. Once we all got situated, we locked the cars and headed down South Prong trail. 

![](https://live.staticflickr.com/65535/48058936132_064a4e8503_h.jpg?classes=caption,img-fluid "Hitting the trail on a freakishly cold and snowy April morning in West Virginia.")

We were soon greeted by a very loud and very swollen South Fork of Red Creek. We tried to find a way to cross it but the water was so high. We went up the banks a bit to see if there was a better place to cross but we couldn't find anything. There was a tree that had fallen across the creek which was about 8 inches in diameter but it seemed too risky to shimmy across given the cold conditions. Below is a video that Chris shot of the creek.

<div class="resp-container">
<iframe style="margin: 50px auto;"width="1136" height="639" class="resp-iframe" src="https://www.youtube.com/embed/VwZ_HIWtM5Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

Um. Nope. 

On that note, we opted not to cross the creek. We decided that we should camp low. Even if we were able to cross the creek, with temps so low and wind being such a factor, camping low would be a wiser choice than at the top of the plateau. So we decided to hike back to the car and head down Boars Nest to see if there were any options to either cross and do our route backwards or just set up camp somewhere along the way. It was also getting a little too late for comfort. As we made our way down Boars Nest, we hit a switchback that seemed to take the trail straight down the gulley back to the swollen Red Creek. We peered down through the trees and didn't see any areas worth trying to cross. As we were scoping out the creek, we came across a large fire ring with a grate and some wood already collected. We took this as a sign to stop and we immediately got our tents set up, a fire going and dinner cooking.

After some time yucking it up by the fire and telling stories, we decided it was time to hit the hay. We threw some snow on the fire and soon realized how frigid the night time temps were and quickly got the campsite cleaned up for a quick departure in the morning. We said our goodnights and headed to our respective tents.

### DAY 2

Our ad hoc plan B was to drive up to the other South Prong trailhead along FR 75 at the top of the plateau. We began our quick trek up Boar's Nest back to the cars around 8:00 am. The weather was nice and peaceful, quite a difference from the ominous conditions of the previous day.

![](https://live.staticflickr.com/65535/48058839386_e8ece97b4a_o.jpg?classes=caption,img-fluid "Much nicer conditions and a quiet sunrise.")

![](https://live.staticflickr.com/65535/48058937587_9cf697fe71_o.jpg?classes=caption,img-fluid "Ice on the windsheild from the arctic conditions the day before.")

The drive up to the trailhead was a little icy but our AWD cars handled it like rockstars. We got to the trailhead and we were pleasantly surprised to find fresh white virgin snow all to ourselves at the top along the trail. We made our way out along the ridge of the South Prong trail with the intention of finding a campsite near Hidden Passage trail then taking a day trip out to the pipeline trail.

![](https://live.staticflickr.com/65535/48058889238_864ed5014e_o.jpg?classes=caption,img-fluid "The calm after the storm gave us an awesome ridgeline hike with great views.")

After a really nice hike along the ridge with some great views, we found a good place to set up camp near the intersection of Hidden Passage and South Prong trails conveniently located right next to a creek. Once tents were up and the campsite established, we checked out our maps to finalize our plan to hike out Hidden Passage trail to the pipeline. Hidden Passage was definitely a challenge to navigate and I'm glad Chris and I both had pre-loaded the route onto our GPS watches. We were able to use map and compass some but having the route on our watches definitely helped out. I would highly recommend anyone checking out this trail to do the same unless you are a map and compass ninja or don't mind bushwacking some.

![](https://live.staticflickr.com/65535/48058889618_62b6958336_o.jpg?classes=caption,img-fluid "We started our day hike after finding a campsite.")

![](https://live.staticflickr.com/65535/48058896533_b91ca65215_o.jpg?classes=caption,img-fluid "Great views along the pipeline.")

![](https://live.staticflickr.com/65535/48058846781_f63df5b138_o.jpg?classes=caption,img-fluid "Crazy kids.")

![](https://live.staticflickr.com/65535/48058854506_1dc69aebcb_o.jpg?classes=caption,img-fluid "Craig checking out the views along the canyon rim.")

We checked out the views, had a quick snack and then headed back the way we came. The snow that had fallen the night before was melting as we were on our hike out. By the time we were heading back, much of our foot steps had melted away and we had to rely on GPS watches. I was again thankful we took the time to pre-load our watches. We got back to the campsite and started a fire, refilled water bottles and prepared dinner. After enjoying our dehydrated meals, we sat by the fire and watch the stars come out. The lack of light pollution made for some amazing views of the stars. After a while it was time for bed. We doused the fire and hit the hay.

![](https://live.staticflickr.com/65535/48058937112_d75a218b4c_o.jpg?classes=caption,img-fluid "Campfire goodness.")

![](https://live.staticflickr.com/65535/48058844111_a4d2021915_o.jpg?classes=caption,img-fluid "Starry night sky.")

### DAY 3

Day 3 started at about 7:00 am. We got up, broke camp and prepared breakfast. The hike out was an easy 2.41 miles that consisted of hiking north on South Prong trail then hanging a right on FR 70 and another right on FR 75 to loop back around to the cars. It was mostly downhill except for some short rolling sections along FR 70 then a short steep climb back up FR 75 to the South Prong trailhead.

![](https://live.staticflickr.com/65535/48058845716_9ca5cee02e_o.jpg?classes=caption,img-fluid "A nice easy and mostly downhill hike back to the cars.")

## FINAL THOUGHTS

This area is what I imagine Dolly Sods was 10 years ago. Hidden Passage trail felt really secluded and wasn't worn down at all, which I really enjoyed. I would imagine that the Canyon Rim trail and Tee Pee trail are both in the same condition and I'd love to go back sometime to complete our originally planned route to check them out. 

![](https://live.staticflickr.com/65535/48058846271_d336f3eb31_o.jpg?classes=caption,img-fluid "Great trip. Even greater company.")