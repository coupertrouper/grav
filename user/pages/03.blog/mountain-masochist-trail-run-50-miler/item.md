---
title: '2019 Mountain Masochist Trail Run'
subheading: 'Gravel roads, creek crossings, single track, tons of elevation gain, and gorgeous fall colors. This race has it all.'
header_image: mmtr_title.jpg
published: true
date: '03-12-2019 16:13'
taxonomy:
    category:
        - Running
    tag:
        - 'Race Reports'
        - 'Ultra Running'
---

A couple of weekends ago I completed the Mountain Masochist Trail Run. This race traverses 50 miles of gravel forest roads, doubletrack, singletrack and jeep roads from Monroe to Montebello, Virginia. If you've never heard of the MMTR, check it out over [at Eco-X Sports](https://eco-xsports.com/events/mountain-masochist/?target=_blank) and if you're feeling brave, give that registration button a click (opens in May). Though I will say that this race is not for the faint of heart as it has over 9,600 ft of elevation gain. That's a lot of climbing! You have been warned.

In 2018, I completed the Pilot Mountain to Hanging Rock 50 miler and had an awesome experience. I wanted to target another ultra for 2019 and that race was definitely on the table again but there were a couple of others I had been batting around in my brain. One of those races was MMTR. The distance was attainable for me and the familiar terrain peaked my interest. After texting my buddy Chris about it, we both tinkered around with the idea for a few days then registered along with a few other friends from the Greensboro/High Point area. Below is a map of the course to give an idea of where this race is located.

![](https://res.cloudinary.com/wscottcooper/image/upload/v1574373321/mmtr_overview_map.png?classes=caption "MMTR Course Map")

## Training

We'll say that I started my "official" training in June of this year but I had been building a solid base since March. Back in the early spring, I knew that I wanted to target an ultra in the fall but just wasn't sure which race I'd sign up for. 

As far as my official workouts went, I modified a traditional 6 month marathon training plan a bit with several big build ups and tapers that spanned from June to Nov. 2, race day. I planned my longest run to be 22 miles and factored a bunch of big elevation gain into my weekend sessions. Basically, I ran. A lot. Up hill. Mountains even. I'm lucky enough to live in an area with varied terrain and I can get an easy 1,000 feet of elevation gain within a 15 minute drive from my house.

In practice, I was able to stay pretty close to my plan with the exception of a 3 week stint in September where I dealt with ~~the plague~~ a terrible cold that turned into acute bronchitis. Thankfully, that crud occurred early enough in my training that I could bounce back strong in time for the race. It did mean that I had to alter my plan a bit and I was only able to build up to an 18 mile long run. But from past experiences, I know that it's better to go into an event under trained than injured. So I played it conservatively which always seems to be the right choice for me when it comes to running longer distance events. Quality over quantity.

All told, I put in a little over 300 miles, climbed about 20,000 ft., and did some ungodly amount of pushups and calf raises. I tried to spend as much time on my feet as possible leading up to the race. I also focused on core strength this time around. Oh, and I dealt with some ankle issues along the way. Thinking I had arthritis, then getting the good news that I didn't have arthritis and just needed custom orthotics to help with arch support. Not to mention the bubons that I dealt with. Would all of the miles and literal ups and downs pay off? We would soon find out.

## Gear

My strategy for gear was to be as minimal as possible but have enough stuff on me to be self-sufficient. In training, I used a 1L bladder and 1 soft flask for my hydration which seemed to work really well for me. I used the soft flask for Tailwind and the 1L bladder for water. The 1L bladder could last me about 15 miles or 3 hours, whichever came first, and gave me something to sip on in between flasks. My thinking was that I could be more efficient at aid stations having to fill up only 1 flask at each stop and hopefully, only have to fill the bladder up 1 time during the race. Something else I considered was the very long section of climbing in the middle of the race. Having a bladder to sip on during these long climbs would be good as I knew I would probably deplete my flask pretty early on in the climbs. I also wanted to make sure I had enough layers because of the cooler fall temps. Forecasted lows at the start were in the mid-twenties and highs were in the mid to upper fourties. From head to toe, my gear list consisted of the following.

* Territory Run Co. Trucker Hat
* Julbo Dust Sunglasses
* Neck Buff
* Ice Breaker Sleeveless Shirt
* Smartwool Arm Warmers
* Garmin Fenix 5
* Salomon Running Shorts
* UA Synthetic Boxer Briefs
* Point6 Merino Wool Crew Socks
* Hoka One One Speedgoat 3 Shoes
* UD Ultra Vest 2.0
* 1 Soft Flask
* 1L Hydrapak Bladder
* Possibles Pouch (Batteries, Spare Contacts, Duct Tape, etc.)

![](https://res.cloudinary.com/wscottcooper/image/upload/v1574703609/mmtr_gear.jpg?classes=caption "My gear selections for MMTR which ended up being a little on the heavy side.")

I'm not sure about the weight of all of that, but it was definitely on the heavy side considering the amount of climbing to be done. At any rate, going into the event I felt equipped for whatever the day might throw at me.

## Nutrition

Not knowing what the drink of choice would be at aid stations and because I had been training with it, I opted to bring small packs of Tailwind to be my main source of calories and used Stinger Waffles to supplement every hour. I also planned on eating light stuff at aid stations like peanut butter and jelly sandwiches and bananas. It turned out that they had each aid station stocked with Tailwind but we didn't know that until the pre-race meeting. More on that in a few. Knowing that meant that I could carry less nutritional stuff and rely on the aid stations more which was a bonus.


## Lodging

My buddies from NC and I all went in together on a cabin rental with Montebello Camping and Fishing Resort which happened to be a 2 minute drive from Camp Blue Ridge, the location of all of the pre-race activity and more importantly, the finish line. This was super convenient because we had to catch a shuttle bus that departed at 4:15 am from Camp Blue Ridge that would take us to the start at the KOA in Monroe, VA. Being so close made it easy to get up and out the door on time to catch our ride. Camp Blue Ridge also allowed tent camping on their property for racers and their crews but we opted for the cabins, given the obvious benefit of... not tent camping. More info about the cabins [can be found here](http://www.montebellova.com/?target=_blank).


## Pre-race

We met at the cabin on Friday and had some dinner while discussing race strategy. And by "race," I mean how much jogging vs. walking we would be doing and by "strategy," I mean we listed off the gear we would be bringing. After scarfing down our homemade dinners, we drove over to Camp Blue Ridge for a pre-race meeting. The race director went over details about the course and how wet conditions would be because of the weather that had rolled through two days prior. The Parkway had been closed because of heavy rains and high winds which would have affected our aid station placement but thankfully, the Park Service was able to lift the closure and we could stick to our original. He said that the creeks were all full and that our feet would definitely get wet. He also said a bunch of other stuff about things and then it was over. After the meeting we headed back to the cabin and called it an early night as we'd need to wake up super early.

## The Race

Our alarms went off at 2:30 am. Yes, really. Two. Thirty. I put my race clothes and gear on, got some coffee going and had some instant oatmeal with a banana (breakfast of wannabe champions). Around 3:30, our friend Jody, who stayed in the next cabin over, popped in and we mentally prepared ourselves for the cold. At 3:40 we sauntered out into the 25°F darkness and piled in Chris's Jeep and headed over to Camp Blue Ridge to catch our shuttle to the start. We found an important looking person with a clipboard, checked in and got on a bus. At 4:15 sharp, the convoy of buses departed Camp Blue Ridge and headed to the start at the KOA. After our 1 hour commute on dark and curvy mountain roads, we arrived and the driver welcomed us to stay on the warm and cozy bus as long as we needed. At the very last possible moment, we left the comfort of the bus and headed out into the bleak cold morning to find the starting line down the hill.

The massive group of 180 runners assembled behind the line in the cold, the only light source being big work lights illuminating the check-in table where the race director stood giving last minute information via megaphone. Chris, Jody and I chatted for a bit, wishing each other a good race as the countdown to the start ticked down. 5... 4... 3... 2... 1... GO!

And we were off. A giant pack of runners made their way around the pond next to the starting line and ducked into the woods along a dark and squishy trail. After meandering around in the woods on the KOA property for a short distance, the trail popped out onto Pera Rd. Jogging up Pera Rd. the pack of racers thinned out into a giant string of bobbing headlamps in the darkness and after about 1.5 miles, we took a left onto FS 318. Everything was soaked from the previous days weather and not 2 miles into the course we hit our first creek crossing at Otter Creek. The water was only about ankle deep and there weren't any rocks to hop which meant we had to go straight through. Nothing like starting out a 50 mile run with wet feet. That would be the first of many creek crossings. 48 miles to go!

![](https://res.cloudinary.com/wscottcooper/image/upload/v1574703609/mmtr_1st-half-start.jpg?classes=caption "Crappy pre-dawn photo just after the first creek crossing.")

After the creek crossing and going through a tunnel under the Parkway, which was full of water BTW and induced some weird "THE FLOOR IS MOVING!!" vertigo, FS318 climbed up to Peavine Gap. The sun was beginning to come up at this point and I was able to turn off my headlamp. At about mile 5.6, the course went down Cashaw Creek Trail (FS318B) until it crossed Dancing Creek at aid station 1. I filled up my soft flask, grabbed a banana and jogged while Chris filled up 1 of his bottles. After an efficient pit stop, we got back on course along FS317A which snaked along next to the Parkway for a bit. 

![](https://res.cloudinary.com/wscottcooper/image/upload/v1574703609/mmtr_1st-half-sunrise.jpg?classes=caption "Lots of runnable doubletrack during the first half of the race.")

Before we knew it we were at aid station 2 at mile 11.38. After another quick and easy pit stop, the course cruised along FS1881 until it hit Robinson Gap Rd. (SR 607). Along the way there was another tunnel full of water at the crossing of Enchanted Creek. There was no enchantment though. Just more saturation for my foot parts. From the intersection to Robinson Gap was the first legitimate climb of the course going from 1400 ft. to 2100 ft. in just a short distance. And I jogged it. Chris yelled out "Hey, this is just one of SIX more climbs..." And yet I continued jogging it. I was passing people like crazy. Probably burning a match too early. I was feeling good though, so who knows? YOLO.

At the top of the climb lived aid station 3 (mile 13.64). I filled up my soft flask, grabbed a mini peanut butter sandwich, thanked the volunteers and continued with the race. I saw Chris about 50 yards down the climb and give him a Fist of Glory into the air and then thought... "You know, he was probably right. I probably shouldn't have jogged that climb." Anyway... I was feeling good. YOLO, right?

The course turned right onto FS311 and immediately went down. Down down down. Like, really down. For 2.5 miles the course descends 867 ft. and we basically lost what we had gained on the climb to aid station 3 and then some. I coasted down the hill letting gravity do some of the work and looked down at my pace and it read 6:37. Ok, time to dial it back some. Aid station 4 was at the bottom of this descent and I stopped to refill my flask and then grabbed a banana. I was about 3:00 in at this point and was still feeling really good. So I decided to scale my pace back a bit as I knew the next section would be relatively flat and I could compose myself for the major climbs that would arrive soon. The course wrapped around the banks of the Lynchburg Reservoir and with the sun shining down through the colorful autumn trees, glinting off of the water I thought to myself, "It doesn't get any better than this."

While the profile looked like it would be relatively flat in this section, there were still some short punchy climbs to contend with. I power hiked many of these and noted that the runners around me who seemed more experienced had some major power hiking skills. Even with my long legs, I was getting passed by people going up these little climbs. The course made its way to the northern part of the reservoir where aid station 5 resided. I stopped to fill up my bottle, grabbed a few more snacks than prior aid stations to refortify my gut because the major climb section had arrived.

![](https://res.cloudinary.com/wscottcooper/image/upload/v1574703609/mmtr_2nd-big-climb.jpg?classes=caption "Start of the major climb up Long Mountain.")

This section of the course traversed Long Mountain and as the name suggests, it is a long mountain. The elevation change is roughly 1,000 ft. as it goes from about 1100 ft. to 2100 ft. over the course of 4 miles. It was a stout climb and I power hiked most of it. Another runner (power hiker at this point) crept up behind me and I could hear clicking sounds. Trekking poles! I was wishing I had those at that moment. If I do this race next year, I will definitely be bringing mine. After putting my head down to do some work, I looked up and the course had started to level out. We had reached the top of this section! The course meandered a bit through rolling farmland and eventually, popped out onto Route 60 where aid station 6 resided. This is where our drop bags awaited us.

![](https://res.cloudinary.com/wscottcooper/image/upload/v1574703609/mmtr_1st-half.jpg?classes=caption "Thumbs up at the top of Long Mountain. Photo courtesy of Exo-X Sports")

Because the drink of choice for this race had been Tailwind, I was able to carry less nutritional stuff. So I actually dropped off a few Tailwind packs I had been carrying along with my toboggan which I never put on. The temps had been perfect up to this point and I was fine with just a trucker hat. I zipped up my bag then walked over to the aid station to get some snacks and refill both my flask and my bladder which was about empty at that point. As I was leaving, I saw Chris arriving. I gave him a fist bump and a "Good job!" and headed out. More climbing to come.

Now I wouldn't say this next section was a "low" point for me. But I would say that I definitely had to keep myself in check. From aid station 6, the course climbed about 1,400 ft. in 3 miles to aid station 7 along FS520. The climb seemed to go on forever. And it did. 3 miles is a long time to be going up. Thankfully, aid 7 had pickles and potatoes! I partook in that salty goodness and filled up my flask with Tailwind then got back on track. This next section would cruise along the top of Buck Mountain a bit then drop down to FS48. I was about 5:30 into this race and my mind was starting to wander a bit. The climbing had definitely done a number on my morale. I kept looking for aid 8 around the next bend but it never came and I began to get discouraged. After psyching myself up a bit and concentrating on the music in my headphones, aid station 8 finally emerged and I could hear folks cheering through the forest. At the aid stations I quickly filled up my Tailwind flask and got back on course to proceed to the Mt. Pleasant loop.

The section I had been dreading for a few weeks had finally arrived. I knew it would be challenging to go from steady gravel roads to technical trail. I grabbed a banana to go at aid 9, I got into a jogging groove and entered the loop. There was a long flat section followed immediately by the climb to the almost-top of Mt. Pleasant on rocky single track. I power hiked this whole ascent. I chatted with a runner who happened to also be from Roanoke and we eventually made it to the top of the climb. On the other side was the steep and rocky descent back down to aid station 10. The rocks were slippery from the rain we had and I had to take it easy. But finally, after much down, we made it to aid 10 and I let out a sigh of relief to be done with that section. Onward and... downward?

Aid 10 was a quick stop and I was all business at that point. No dilly dallying. I grabbed some snacks to go and proceeded down the 1.6 mile descent to the South Fork of Piney River crossing along FS48 to make my way to Salt Log Gap. I had slowed down a lot and tried to keep my pace around the 13:00 mark but it was a challenge. So I decided to just walk-run-walk-run a bit to recompose myself. After a bit of that and a bit of day dreaming about hearing my name through my headphones, I looked back and saw Chris's red hat through the trees and realized that he had been calling my name. Seeing him was a much needed morale boost and once he caught up with me, we decided we'd walk-run-walk-run the rest of the race together. The short stint of walk-running gave way to some jogging and eventually we made it to aid 11 well before the 4:00 cutoff. We had successfully beat all of the cutoff times! Well... except for 1. The 12:00 finishing time limit.

![](https://res.cloudinary.com/wscottcooper/image/upload/v1574703609/mmtr_2nd-half.jpg?classes=caption "Satisfied that we had made our cutoffs, we proceeded to walk-jog-walk-jog up the course. Photo courtesy of Exo-X Sports")

After S.L.G., we took FS1176 for about a mile and then hung a right on FS246. Aid station 12 teased us through the trees but we had to go around a giant loop that eventually circled back around to the aid station and consisted of FS246 then FS1176A. A small-ish pack of runners which included us traded places passing each other through this section as it was mostly cruising downhill along 246 then uphill along 1176A to aid 12. To get to the aid station, we had to pass Lovingston Springs trail (our turn) then go up to the gate where the aid station resided. They had warm broth, pickles and bananas along with many other sugary treats. After a quick stop, Chris and I headed back down the 200 or so yards to our turn on Lovinston Springs. This trail cuts along the county line between Nelson and Rockbridge counties, traversing the main ridgeline that divides the two.

From the turn, the trail shot straight up the contours of the mountain until it reached the top of the ridge. From there, it cruised along a bit until another short and steep climb to what I believe was the highest elevation of the entire course at about 4200 ft.. As we progressed on this trail, it appeared to be less and less frequently travelled as it was hard to navigate at times. That or we were just really tired. The sun was on its way down and we were starting to lose daylight. All of this only added to the sense that we were skirting dangerously close to the 12 hour time limit. At some point the course was supposed to drop down to the Montebello Fish Hatchery. The drop eventually came and our loosely traveled foot path eventually morphed into a fire road, which eventually morphed into a gravel road. Down down down we went. 5 miles go. The gravel road (3454?) eventually arrived at a gate with a trailhead for what I guess was for Spy Rock because there were a ton of day hikers looking at us like, "WTF?" Then the gravel road eventually became paved at the fish hatchery. Down down down some more to SR 690. 1 mile to go. 

Run walk run walk. We crossed Crabtree Falls Hwy (Rt. 56) onto a trail on the Camp Blue Ridge property that led to yet another creek crossing–this one with ropes to hold onto while crossing. We crossed, rounded a bend, saw tents, heard people cheering, saw the main access road for the camp, then saw those glorious red glowing numbers of the clock at the finish line which read 11:40 as we approached the line.

We were done.

We gave each other a handshake and a high five as we crossed the finish line of the Mountain Masochist Trail Run 50 miler. Man! What a day!

## Take Aways

Running 50 miles isn't easy. Like... at all. But "run" rhymes with "fun". I knew that as long as I stayed positive, I could finish. It also helped that I put in gobs of training miles with even more gobs of elevation gain. If I do this race next year, I will add yet more gobs of elevation gain and probably sprinkle in some power hike workouts. I noticed that the more experienced racers seemed to be able to motor up the climbs by power hiking. 

As for gear considerations, I was definitely over prepared for this race and didn't need my race vest. I probably would have been OK with 2 hand held bottles and a belt to carry trekking poles, an extra layer and extra nutrition. The aid stations were so well stocked and positioned so frequently that a belt to carry supplemental stuff like waffles and gels would have sufficed. 

MMTR is definitely a climber's race. If one expects to finish within the 12 hour cut off time, it's important to know that going in and train appropriately. With that disclaimer about the elevation gain, I highly recommend this race to anyone who is trained up and ready for it. Eco-X put on an awesome event with top notch support, pre-race communication, and a well marked course. I thank the race director for such a great day out in the mountains and more importantly, the volunteers at the aid stations. More importantly, a big thanks to my buddy Chris for being up for this race and for pulling me through the final third of the course. But most importantly, my wife/family for putting up with my ridiculous running schedule and being OK with me going out to run 50 miles in the mountains. It wouldn't have been possible without their support. What a great experience! If the stars align in my favor again, I will be back next year!