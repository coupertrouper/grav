---
title: 'Training Log: Week 1'
media_order: training_wk1.jpg
subheading: 'First week of training for Mountain Masochist Trail Run.'
header_image: training_wk1.jpg
published: false
date: '08-07-2019 12:34'
publish_date: '08-07-2019 12:34'
---

## 7/1-7/7

## Goals:
Build fitness and work a third run into my normal routine. Gradually work in strength and conditioning exercises. As with most things fitness oriented, too much too soon leads to bad results. My goal this week (and this month, really) is to get my body used to a bit more activity than what I've been doing since PM2HR.

#### MONDAY
Rest. Because the best training plans start with a rest day.<br>
**Exercises & Stretching:** Pushups (2x10), normal stretching routine<br>
Walked on the greenway during lunch (1 mile).

#### TUESDAY
6:00 am run | 30 mins at easy pace<br>
Hard to get out of bed but motivated to get training started off properly.<br>
**Exercises & Stretching:** Pushups (2x10), normal stretching routine<br>
Walked on the greenway during lunch (0.6 mile).

#### WEDNESDAY
Travel to Ohio to visit family. Does driving count as rest?<br>
**Exercises & Stretching:** Normal stretching routine

#### THURSDAY
Intervals workout on Columbus, OH greenways.<br>
4.95 mi | 1 2 3 4 4 3 2 1 intervals with 90 sec. breaks<br>
**Exercises & Stretching:** Normal stretching routine<br>
**Independance Day!**

#### FRIDAY
Rest. Still in Ohio spending quality time with the fam.

#### SATURDAY
Drive back to Virginia. "Rest?"<br>
**Exercises & Stretching:** Normal stretching routine

#### SUNDAY
8 miles | easy pace | 1200' Gain<br>
**Long Run:** Planned to do the Standard Loop but changed the route based on how tired I was feeling. Ran around the base of Mill Mountain with trails, took the Parkway down to the greenway and finished out the 8 miles on my training plan. It was a struggle but I got 8 in. I'll chalk it up to adding a speed workout and travelling the day before.

![](long-run_7-7.jpg?classes=caption "Sunday Long Run around base of Mill Mountain then greenway to finish it up.")

## Take Aways

Adding a third run, especially a speed workout, during the week proved to have an impact on my long weekend run. That and spending 6 hours in the car the day before. Switched to an AM run on Tuesday which was good. The speed workout was a nice change up and look forward to more of those. The long run was a struggle but I got the job done.