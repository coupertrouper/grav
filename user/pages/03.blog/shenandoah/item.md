---
title: Shenandoah
subheading: 'Four somewhat soggy days in this national treasure only left me wanting more and I will definitely make another trip there. In the meantime, here are a few words to describe this recent adventure with my buddies Chris and Mark.'
header_image: IMG_0274_hero.jpg
published: true
date: '13-08-2018 09:00'
publish_date: '13-08-2018 09:00'
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - Virginia
---

Even though I’ve lived in the southeastern United States for all of my life, I’ve somehow managed to avoid visiting Shenandoah National Park. We moved 100 miles closer to Shenandoah when we relocated to Roanoke from Greensboro 6 years ago and for some reason my willful ignorance persisted. For this backpacking trip, we decided not to overlook Shenandoah any longer. I guess you could say we saved this trip for a rainy day. Yuk yuk.

Four somewhat soggy days in this national treasure only left me wanting more and I will definitely make another trip there. Perhaps in the form of a 100+ mile thru-hike of the entire national park. In the meantime, here are a few words to describe this recent adventure with my buddies Chris and Mark. [Check out Chris's post about this trip here.](https://y2kemo.com/2018/07/backpacking-shenandoah-patches-of-blue/?target=_blank)

## OUR ROUTE

The general concept of our route was to create a loop in the northern portion of Shenandoah. The exact mix of trails consisted of Nicholson Hollow Trail, Hot Short Mountain Trail, Hazel Mountain Trail, the A.T., Corbin Cabin Cutoff Trail, Nicholson Hollow Trail, Indian Run Trail, Meadows Horse Trail, White Oak Canyon Trail, Weakley Hollow Road/Trail, then climb Old Rag via Saddle and Ridge Trails. We also loosely planned to do a day hike to Stony Man from our night 2 campsite. The final part of the plan included a summit attempt of Old Rag on day 4 if the weather held out. The weather did not entirely hold out. In fact, it could more colloquially be referred to as a sizzly sod-soaker.

### DAY 1
#### 7.97 miles | +2,005′ GAIN | -666′ LOSS

We parked at the Old Rag boundary access parking lot. Instructions for paying weren’t exactly easy to interpret. One section of the National Park website states that backpackers can park for free at the Old Rag access but a $15 entry fee must be paid. Another section states that a vehicle must pay $30 to enter the park. I finally emailed the backcountry park ranger’s office and got a straight answer. If one train leaves San Francisco at 3:00pm going 60 mph, another train leaves Chicago going 80 mph, and the final train leaves New York going 55 mph, you’ll pay $15 each. If 3 trains head in the same direction going 35 mph from Roanoke, Virginia, you’ll pay $30. Got it? Cool. Let’s go! (In all seriousness, Backcountry Ranger Kaleb did answer my question quite clearly stating that if each of us arrived in separate cars, we’d each pay $15. But if we all arrived in the same car, we’d pay $30. Thanks Kaleb! Now, about those website instructions...)

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101984/43754077302_8d227d2c70_k.jpg?classes=caption "On our way to the trail head.") {.center}

Anyway… we got our gear situated, bug spray slathered on, brand new shiny Garmin GPS watches synced, and then headed up Nethers Rd. to the start of Nicholson Hollow Trail. We headed north, crossing both Brokenback Run and then Hughes River. Nicholson Hollow Trail continued along Hughes River for 2.12 miles and at mile 2.12 we took a right to head north on Hot Short Mountain Trail. After 2-ish miles we hit the junction with Hazel Mountain Trail then took a left (north). We continued on Hazel Mountain Trail but somehow took a wrong turn because, you know, reading trail signs is hard. Of course we only realized what we had done after we had descended a few hundred feet in elevation. Oh well. At least we could badly sing Pearl Jam to scare away bears as we retraced our steps, climbing back up the ridge to get to the correct trail. Is it Getback day yet?

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555340784/41993349690_1b8009552f_k.jpg?classes=caption "These trail signs required way too much effort to read!") {.center}

Thanks to Chris’s new Garmin Fenix 5X with its fancy schmancy pre-loaded topo maps, we easily corrected our mistake and got back on the intended route. After making our way around a short bend in the trail, we found and crossed the Hazel River. On the northern bank of the river we found a very nice set of pre-established campsites tucked way back behind a rocky area and we set up shop for the evening.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101984/41993385140_d5e1b2f77f_k.jpg?classes=caption "Stoveless camping is great for summer backpacking!") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555340812/41993359040_58f0f2361c_k.jpg?classes=caption "A nice campsite tucked away off of the trail.") {.center}

I got my Talenti jar out and poured in the couscous with just enough water to rehydrate it for dinner. I screwed the top back on to let it sit while we all went over to the tent area to set up our sleeping quarters. About an hour later, we made our way back over to where we had set up our kitchen/eating area and partook in our dinners. The couscous was really good, surprisingly. The weather was hot and humid and cold couscous really hit the spot more so than I had anticipated. We sat around sans-campfire (which was really strange, btw) for a couple more hours then hung our bear bags then went for a spider walk in the darkness. A spider walk basically consists of walking around in the dark looking for shiny glints of light. The glints of light are spiders’ eyes. After our tangent spider expedition, we returned to our campsite and called it a night.

### DAY 2
#### 14.75 miles | +3412 ft GAIN | -3385 ft LOSS

We woke up around 6:30 and slowly started our routine. After some breakfast and coffee, we broke camp then got on the trail by 8:30. The day’s route started at the campsite at Hazel River then climbed up 700 ft. in elevation to Skyline Dr. at mile 1.6. Once at Skyline, we hung a left, walked down the pavement for a few short yards then looked for Meadow Spring Trail. We located it then turned up Meadow Spring for half a mile or so, then hit the AT junction. We had been climbing consistently for about 2.25 miles and we had yet more up to go. Heading north on the AT we continued climbing towards Mary’s Rock, our turn around point. The hope was to check out the views at Mary’s Rock but hopes were dashed as we ascended into the clouds. The day started off overcast but only got grayer and grayer as we gained elevation. The humidity was so thick I could taste it on my tongue if I opened my mouth for a few seconds.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555341241/42898678855_3369a65bc5_k.jpg?classes=caption "Normally, the views are awesome. Not so great today though.") {.center}

We made it to Mary’s Rock but there were no views. Only dense fog and visibility was less than 10 yards. After taking some pictures of white nothingness, we turned around and headed back down the AT for 4.23 miles until the intersection of Corbin Cabin Cutoff. Along the way, we brainstormed an alternate route of heading out to Stony Man, doing our planned day hike route while on the ridge rather than descending Corbin Cabin then ascending Nicholson Hollow after setting up camp. Doing the route that way would save us some time and energy. Plus, we’d get to check out more white nothingness at Stony Man!

The cruise along the AT was a much welcomed flat-ish change from the relentless ascent of Mary’s Rock we had just done. Just before the Pinnacle, we passed Byrd’s Nest Shelter No. 2, a shelter that we had investigated as a possible campsite. It was a large shelter complete with a fireplace that would be a great place to hunker down if the weather was really bad.

After passing the shelter, patches of blue sky began to appear as the clouds opened up. The patches were short lived though and a few minutes later, fog rolled back in and only thickened as we climbed higher. At the Corbin Cabin Cutoff intersection, we stopped briefly so that I could hydrate my lunch which consisted of Ramen and spicy tuna. I knew we had about an hour left to hike before stopping which would be plenty of time to cold soak the noodles.

On the approach to the top of Stony Man the fog was extremely thick and visibility was less than 50 feet, even worse than at Mary’s Rock. At the summit, there is a large rock outcropping and under clear conditions the views are incredible but we would have no such luck. The thick clouds did start to melt off a bit as we arrived but gave us only brief glimpses of the valley down below. At any rate, we sat down on the large rock outcrop and enjoyed our lunches while imagining we had perfect weather and amazing views.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101986/43108986305_cc9ee06efe_k.jpg?classes=caption "Good views of the boulder field.") {.center}

Lunchtime was over and it was time to get back on the trail. Now satisfied after refortifying our guts, we descended Stony Man’s ridgeline and Chris turned on the afterburners while Mark and I struggled to keep up. I think he even morphed into a mountain goat at one point. We arrived at the intersection of the spur trail that would eventually take us to Skyline and Chris was there waiting for us, in human form. 

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555341241/28866379697_7acb929ce4_k.jpg?classes=caption "Mark and I trying to keep up with Mountain Goat Dunst.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101983/43967536012_4b339b93b3_k.jpg?classes=caption "Our only real glimpse of the sweeping vistas of Shenandoah.") {.center}

Making our way onto Skyline, the clouds opened up for a brief period allowing us to snap some rare photos of the sweeping vistas that Shenandoah is known for. We took it in for a few seconds then got back on the trail, turning down the mountain on Nicholson Hollow. After a long descent, we found Corbin Cabin and then started scouting for campsites. We searched for a while and finally found a good spot nestled about 50 yards of the main trail, behind a flat rocky area that we ended up using for our cooking/eating zone.

Again, sans-fire, we sat around chatting for a while and eventually had dinner. All the while noticing that the sky looked more and more like it would start raining. Weather ultimately did move in and forced us to retreat to our tents for an early night before the sun had even set. It was a downpour but nothing my tarp set up in half-pyramid mode couldn’t handle. When weather like that moves in, it’s always a good chance to catch up on sleep.

### DAY 3
#### 13.51 miles | +2372 ft GAIN | -3576 ft LOSS

We woke up around 6:00 to begin day 3. After the usual routine of breakfast, coffee and camp breakdown, we started the hike along Nicholson Hollow Trail for 0.19 miles until the intersection of Indian Run Trail. We took a turn south (and uphill) on Indian Run for 1.98 miles until it hit Old Rag Fire Road. This entire section was a long climb and the weather repeated the same pattern as the previous day. It started off sunny but gradually became more foggy and eventually drizzly as we gained elevation. All the while the air maintained near 99% humidity.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555341397/43966682222_6410ee4ba5_k.jpg?classes=caption "A little morning sun followed by a lot of afternoon cloudiness.")

We took a right on Old Rag Fire Road and hiked for 0.7 miles to Meadows Horse Trail. The rain jackets eventually came out as the fog turned into drizzle. As we descended towards White Oak Canyon, we heard the sound of rushing water getting louder and louder. It was the sound of upper White Oak Falls. We paused for a few moments to check out the falls along White Oak Canyon and the Robinson River.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101983/29077953007_9149f1705c_k.jpg?classes=caption "Mark checking out the falls above White Oak Canyon.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101983/43966685622_050358d7f0_k.jpg?classes=caption "Yours truly posing for the camera.") {.center}

We continued the steep descent of White Oak Canyon Trail for 2.75 miles until it hit Weakley Hollow Rd. where there is a parking lot. We stopped and dumped our trash in the trailhead’s trash bins, grabbed a quick snack then got back into hiking mode.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101986/43966736962_f0d7b2a356_k.jpg?classes=caption "Descending into White Oak Canyon.") {.center}

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101985/29077955767_28d6433278_k.jpg?classes=caption "One final outcropping before the trail descended into the canyon.") {.center}

At this point, we took a left on Weakley Hollow Rd and hiked for 0.6 miles until the Berry Hollow trailhead. This part of the route was all uphill and had me questioning my route making abilities. The paved climb gave way to gravel climbing where the road stopped and Berry Hollow began. We each put our heads down and just focused on getting up to the gap. After yet more climbing on Berry Hollow, we finally arrived at the intersection of the Saddle Trail, which we had planned on using for our Old Rag summit attempt on day 4. From this point, it was all downhill to where we were banking on there being a campsite.

As it would turn out, the bank was empty. There was a large campsite in a perfect location with access to water but another group had already claimed it. They even had speakers. Pff… glampers. With the wind knocked out of our sails, we proceeded onwards looking for campsites. We built the route in a way that would give us an easy bail out option if Old Rag didn’t work out and to continue to look for campsites meant that we were basically following the bail out option. Given that we were only 1.5 miles from the car and that distance was only closing, as was our luck with campsites, we democratically elected a new president of car camping, a campfire, brats and cold beverages. This peaceful transfer of power was about to take place when suddenly, the perfect campsite appeared. It was right next to water, had perfectly arranged rock furniture, and there was a plethora of space for tents and tarps alike. Basically all of Maslow’s hierarchy of human needs. What was a group of tired, weather beaten, soggy, hungry, and thirsty backpackers to do? Well… the choice was pretty easy at that point. Beer and Brats.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101986/43967060152_6d88093ec4_k.jpg?classes=caption "Walk of shame out of the woods. But there were beers and brats!") {.center}

Considering the fact that we were so close to the car, and that the weather had been wet and the forecast was only calling for more wet (it is highly inadvisable to climb Old Rag when the weather is wet), we called it for the sake of safety. Like a troupe of happy dwarfs emerging from the fields of battle, we plodded our way back to the car.

### My grand totals for this portion of our trip:
## 36.23 miles | +7789 ft GAIN | -7627 ft LOSS

Night 3 was a lavish extravaganza of a few cold beverages and brats next to a nice warm campfire at Sherando Lake campground. We noted that we had broken our 3 day distance record. While we were setting up, the camp host came by warning us of the fact that there was a very active population of bears that had been invading the campground. Luckily, we had no bear encounters during our stay. After an epic downpour and crazy thunderstorms that night, we woke up super early and were on the road by 7:00 am.

![](https://res.cloudinary.com/wscottcooper/image/upload/c_scale,w_1500/v1555101983/30146734238_b482c9899d_k.jpg?classes=caption "Beers, brats AND campfire. I'm glad we bailed.")

## FINAL THOUGHTS

Shenandoah National Park truly is a gem in the southeast and I can’t believe it’s taken us this long to explore it. We will be back for sure. One future project that we have in mind is a thru-hike of the A.T. that runs the length of the park. 3 lessons I learned on this trip:

1. If you try to make a loop in Shenandoah, be prepared for a lot of elevation gain/loss. Loops require you to go up to a ridge, then descend, then climb back up, and repeat that pattern until a loop is made.
2. There are not a lot of established campsites in the northern area of the park. If I were to go back, I would consider investing in a hammock/tarp setup.
3. Stoveless cooking, otherwise referred to as cold soaking, is definitely something I want to continue to explore. It forced me to be more creative with my food choices and probably more healthy, too.
4. I decided not to bring my big boy camera in the interest of keeping my kit minimal. I shot all photos on my crappy Moto e4 Plus. While the pics turned out terrible, I found myself enjoying the trip more because I wasn't so focused on trying to shoot the perfect image. This will be a consideration for future trips.

For more information about Shenandoah, [check out the National Park website](https://www.nps.gov/shen/index.htm?target=_blank). More specifically, [here is a good description of the Old Rag hike](https://www.nationalparks.org/connect/blog/boulder-dash-ultimate-guide-hiking-old-rag-mountain?target=_blank) complete with all of the disclaimers about hiking in wet conditions.