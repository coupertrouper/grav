---
title: 'James River Face Wilderness'
subheading: 'Snow covered trails, technical rock scrambles, an adventure trail run, bitter cold and 1 amazing sunrise. This adventure checked all of the boxes as far as the perfect winter trip.'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - Virginia
        - Winter
published: true
date: '02-08-2021 10:00'
publish_date: '02-08-2021 10:00'
---

This past winter my adventure pal, Chris and I took a trip to the James River Face Wilderness near Snowden, Virginia. I'm posting this a long time after the actual trip. Words about our experience are scarce in my mind. Not sure how accurately I can depict sleeping in 10° F conditions as it's now summer and 90° F outside. That said, I'm finally getting around to posting about this trip and while the words for storytelling are scant, the pictures and good memories remain concrete in my mind and in pixel form. [Chris wrote about this trip, too.](https://y2kemo.com/2021/02/backpacking-james-river-face-wilderness-frozen-wonderland/?target=_blank)

## The Plan

With the possibility of snow on the horizon, we had 3 different route options to give us flexibility and safety. A long option (no snow/good weather/very committed), a short option (some snow/fair to crappy weather/less committed), and a snow option (different trail head on a well travelled road/low commitment/use of A.T. shelters for campsites). The long and short option used Piney Ridge trail to form various loops, one of which passes by Devil’s Marbleyard. These options have longer distances between possible bail out points. The snow option was an out-and-back that started/ended at the A.T. foot bridge that crosses the James River near Snowden, Virginia. The trail head is on VA Rt. 501 which is a major road and should be plowed if the snow conditions are bad. The final route is pictured below in our map which we printed out on waterproof Terraslate paper. The route and trip plan were generated using cross referenced data from Gaia GPS, AllTrails, Google Earth, and Garmin Connect.

![](/user/pages/03.blog/james-river-face-wilderness/devils-marbleyard-printout-1.jpg?classes=caption "All routes (including the snow route) mapped out.")

## Gear &amp; Food

![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_3996.jpg?classes=caption "Food selections for a 2 night trip.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_3999.jpg?classes=caption "On the left, my main meal bag. On the right, my easy access snack bag.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4006.jpg?classes=caption "Sorting out the gear and double checking I have everything.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4012.jpg?classes=caption "Bag all packed and ready to go. Yes, those are shoes strapped to my pack. More on that later.")

## Day 1
### 7.94 MI | +2,021' GAIN | -2,005' LOSS

![](/user/pages/03.blog/james-river-face-wilderness/compressed_IMG_6341.jpg?classes=caption "Obligatory 'before' photo. Photo by Chris.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4023.jpg?classes=caption "Starting out the hike just before Piney Ridge Trail.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_IMG_6353.jpg?classes=caption "On the climb up Piney Ridge. Photo by Chris.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4051.jpg?classes=caption "Looking back at Chris as we head up the mountain.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4061.jpg?classes=caption "More and more downed trees as we ascended.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4047.jpg?classes=caption "Serious cold vibes going on here.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4064.jpg?classes=caption "Ice covered everything and we got wet going under all of this.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4083.jpg?classes=caption "Serious 'we could get hypothermia if we don't take this sh*t serious' serious vibes here. Serious.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4074.jpg?classes=caption "More, more, and more ice.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4084.jpg?classes=caption "And some more ice. We occasionally saw a lone set of human tracks through this section.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4090.jpg?classes=caption "Trail intersection indicating we were close to where we would start our descent.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4109.jpg?classes=caption "Looking out over Devil's Marbleyard. I wished we could have spent more time here but we needed to move.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_IMG_6381.jpg?classes=caption "Soaking in a quick view and then getting back on the trail. Photo by Chris.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_IMG_6376.jpg?classes=caption "Back on the trail. Photo by Chris.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4114.jpg?classes=caption "Below Devil's Marbleyard.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4121.jpg?classes=caption "Racing the sun, descending as safely and as quickly as we could to get to the campsite.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4123.jpg?classes=caption "Campsite for night 1.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4135.jpg?classes=caption "Chris making a stick for hot dogs.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_hotdog.jpg?classes=caption "Hot dogs on a cold night. Photo by Chris.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4143.jpg?classes=caption "Oh yeah. It was cold!")

## Day 2 (Pt. 1)
### 6.7 MI | +2,376' GAIN | -1,036' LOSS

![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4157.jpg?classes=caption "Twas a cold morning. But there was hot coffee and oatmeal.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4155.jpg?classes=caption "Coffee stayed surprisingly warm in the Nalgene.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4168.jpg?classes=caption "Chris crossing the bridge at Petites Gap Rd.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4175.jpg?classes=caption "Sometimes we have to hike on roads.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4177.jpg?classes=caption "Finally the sun came out and started to warm things up.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4196.jpg?classes=caption "As we ascended, it got colder and colder. At the top, it was super windy.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4202.jpg?classes=caption "Familiar trend of blowdown from recent storms.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4210.jpg?classes=caption "Patagonia Houdini jacket to the rescue.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4220.jpg?classes=caption "Beautiful but dangerous.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4226.jpg?classes=caption "Photogenic luminous.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4233.jpg?classes=caption "Is this the top? Are we there yet?")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4245.jpg?classes=caption "Nope. STILL not at the top.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4231.jpg?classes=caption "Maybe the top. Hopefully.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4248.jpg?classes=caption "Here's the top!")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4238.jpg?classes=caption "The view from the top of another top.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4251.jpg?classes=caption "Kold with a K.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4254.jpg?classes=caption "Descending off of the ridge into our campsite at Marble Spring.")

Chris recorded some video of the descent from the top of Highcock Knob. The snow had a nice crust on the top of it near the top. But as we descended and moved into the mountain's shadow, the crust turned to ice and I was glad that I had brought my microspikes. Any time conditions are like this, microspikes are a necessity.

<div class="resp-container">
<iframe width="1110" height="624" class="resp-iframe" src="https://www.youtube.com/embed/wj-Ij28FnUo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
    
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4258.jpg?classes=caption "Campsite for night 2.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_IMG_6457.jpg?classes=caption "Improvised shelter to create a wind break.")

## Day 2 (Pt. 2)
### 6.38 MI | +1,105' GAIN | -1,135' LOSS

![](/user/pages/03.blog/james-river-face-wilderness/compressed_IMG_6449.jpg?classes=caption "We ran a 10K out-and-back after setting up camp down Sulphur Springs Trail. Didn't use the shoes that I brought but ran in boots.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_10K.jpg?classes=caption "Chris keeping his run streak alive. This marked 308 consecutive days of running a 10K! Note that he also ran in boots.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4288.jpg?classes=caption "After a run, it was time for dinner by the fire.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4301.jpg?classes=caption "Crappy attempt at astrophotography.")

## Day 3
### 6.38 MI | +1,105' GAIN | -1,135' LOSS

![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4323.jpg?classes=caption "Another cold start to the day.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4313.jpg?classes=caption "Frost on the tent.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4318.jpg?classes=caption "Maybe this sun will warm things up.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4324.jpg?classes=caption "Nope. Still cold. But hey, there's coffee.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4337.jpg?classes=caption "Time to get back on the trail.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4339.jpg?classes=caption "Long, straight section of the AT before Sulphur Springs intersection.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4348.jpg?classes=caption "As we dropped elevation, the temps rose.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4372.jpg?classes=caption "Less and less snow down below.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4388.jpg?classes=caption "Chris building a snowman.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4390.jpg?classes=caption "This snowman will probably have a short life.")
![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4367.jpg?classes=caption "Final view as we descended back down to the Jeep.")

## Final Thoughts

![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4360.jpg?classes=caption "Frozen pine cone along the trail.")

We were humbled by the conditions and had a successful trip. Others weren't so lucky that weekend and our thoughts go out to the hikers who had to be rescued and the SAR teams that got them out. I am certainly glad I brought microspikes and used them when we were traversing the ridge, way up high on the mountain where the ice had formed a slippery crust on top of the deep snow. It almost felt like we were traveling on a glacier in some places. 

![](/user/pages/03.blog/james-river-face-wilderness/compressed_DSC_4352.jpg?classes=caption "Microspikes. Mine are from the brand Hillsound and performed well.")

If there was 1 takeaway from this experience, it would be to always bring [microspikes](https://www.rei.com/product/890608/kahtoola-microspikes-traction-system?target=_blank) when there is a potential for ice. Being confident in my footing on steep and slippery, ice covered slopes was definitely worth the extra little bit of weight.

Here's a little bit more about the [James River Face Wilderness](https://www.vawilderness.org/james-river-face-wilderness.html?target=_blank). If planning any kind of winter trip on these trails (or any trail), be sure to read up on how to do it safely. [REI has a great article](https://www.rei.com/learn/expert-advice/cold-weather-hiking.html?target=_blank) with tips for hiking in cold-weather. [The U.S. Forest Service is also a great resource](https://www.fs.usda.gov/visit/know-before-you-go?target=_blank) with lots of information about hiking in all conditions and how to prepare.
