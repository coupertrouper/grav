---
title: 'Training Log: Weeks 13-16'
subheading: 'You get what you get and you don''t throw a fit.'
header_image: training_wk13-16.jpg
published: false
date: '20-10-2019 16:00'
publish_date: '20-10-2019 16:00'
visible: true
---

## 9/23-10/19

## Goals:
Get some hard workouts in after fully recovering from bronchitis. Balance training with life.

#### MONDAYS
Rest/easy greenway hike | ~ 2 mi.<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### TUESDAYS
Easy pace evening runs | 40-45:00 | ~ 4.5 mi.<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### WEDNESDAYS
Rest and lunch hikes | 20-35:00 | 1-3.5 mi.<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### THURSDAYS
Rest<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### FRIDAYS
Rest or easy greenway run | 30-45:00 | 3-4.5 mi.<br>
**Exercises & Stretching:** Stretched. Pushups (2x15). Toe/calf raises (3x20 of each).

#### SATURDAYS
Rest day.

#### SUNDAYS
Long run day.

## Big Run Highlights

During this block of training, my long runs were almost all exclusively on Mill Mountain with the exception of doing the Triple Crown with my buddy Chris. More on that in a different post soon. The route I've come up with on Mill has a good mix of trail with pavement and I can link up trails in a way that is flexible. If I want a day with 2000 ft. of elevation gain, I can easily do that. If I want to go a little lower, that can be done as well. It's also conveniently located and doesn't take a lot of driving time to get to, which is key when trying to balance family/house responsibilities with running. I'm lucky to live in an area with this kind of possibility.

## Life

Our Honda Accord's starter stopped working towards the end of this block of training which put a strain on our schedules a bit and made it a challenge to fit runs in because I elected to do the repair myself in the interest of saving money. The repair was relatively easy but waiting on parts and working out logistics of life stuff was not. Thankfully, my family is pretty adaptable and we made it work.

## Take Aways

"Life is just one damned thing after another." I think Mark Twain said that. DIY'ing a car repair is rewarding when it works. As my son's teacher at daycare says, "You get what you get and you don't throw a fit." The life stuff outside of running definitely put a strain on our schedules (and sanity) but we tried our best and made it work and both my wife and I managed to stay on track with our training. I'll head up to Ohio this weekend to cheer my wife on as she runs the Columbus Half Marathon with her sister. And I'll be focusing on staying well over the next 2 weeks as I count down to MMTR!

### 14 DAYS UNTIL RACE DAY!