---
title: 'Dolly Sods Northern Loop'
subheading: 'For this February trip, we had high hopes of breaking out our snow shoes to traverse the Dolly Sods Wilderness. Conditions wouldn''t be in our favor for snow shoeing though.'
header_image: northernloop.jpg
date: '17-02-2017 09:00'
publish_date: '17-02-2017 09:00'
author: 'Scott Cooper'
taxonomy:
    category:
        - Backpacking
    tag:
        - 'Trip Reports'
        - 'West Virginia'
---

For this February trip, we had high hopes of breaking out our snow shoes to traverse the Dolly Sods Wilderness. Conditions wouldn't be in our favor for snow shoeing though. On the bright side, we couldn't really complain about the weather. The weather was perfect for backpacking, almost spring-like.

## ROUTE

Planning a route weeks in advance is always a challenge when there is a potential for snow. For this trip, Chris, Ken and I batted around lots of different options and factored in different trails to use as bail outs in case conditions were really bad. I enjoy this part of the trip planning. It's a great way to get familiar with an area on paper and really memorize the different features of the landscape. After lots of emails and back and forth, we settled on a 15-ish mile route that involved a main loop and a day hike. Our main route would start along FR80, head up Rocky Ridge trail, head down Dobbin Grade trail, down Upper Red Creek trail, Blackbird Knob trail then Rocky Ridge back to FR80. For night 1, we'd camp where Dobbin Grade crosses the Left Fork of Upper Red Creek. Night 2 would be spent where Blackbird Knob trail crosses the Left Fork of Upper Red Creek. Our day hike would consist of a loop using Harmon Trail, Rocky Ridge and Blackbird Knob trails. In the map below, the day hike is the portion of the route that doubles over itself at Harmon Trail.

### DAY 1

We met at my house in Roanoke. Chris and Ken picked me up since Roanoke is on the way up to West Virginia. I stuffed my gear into his Jeep and we got on the road. 4 hours later, we were at the trailhead and noticed a sign that said we needed a permit to park overnight. The permit, of course, needed to be obtained back down the mountain at the Canaan Valley Visitor's Center. So... we drove all the way back down FR80 to the visitor's center. Luckily, the drive wasn't too long and getting the permit was easy. We got our overnight parking permit and headed back up FR80 and we could finally start our hike. There was minimal snow at the trailhead so we elected to leave our snow shoes in the car, sadly. Conditions were more spring-like and we couldn't complain.

![](https://live.staticflickr.com/4688/38492299855_88b8b64fee_k.jpg?classes=caption 'Easy but long drive up to West Virginia.') {.center}

![](https://live.staticflickr.com/4601/38492296825_95c063be09_k.jpg?classes=caption 'Starting the route on Rocky Ridge trail.') {.center}

![](https://live.staticflickr.com/4727/25500798318_f10a91b06f_k.jpg?classes=caption 'The views along Rocky Ridge trail.') {.center}

![](https://live.staticflickr.com/4592/24505391417_28c93bf87a_k.jpg?classes=caption 'Dobbin Grade trail feels more like Canada than West Virginia.')

There were no surprises on our day's hike. Everything, except for the lack of snow, went according to plan and we ended up camping exactly where we had intended. We enjoyed the views along Rocky Ridge trail then had an easy hike down Dobbin Grade to our campsite along the creek. I found an almost idyllic place to set up my tent right beside the creek.

![](https://live.staticflickr.com/4733/27593047109_3a1014982d_k.jpg?classes=caption 'Camped next to Upper Red Creek. I need a ND filter.') {.center}

![](https://live.staticflickr.com/4686/24505404497_d04be54c28_k.jpg?classes=caption 'Looking down the creek from the campsite.') {.center}

![](https://live.staticflickr.com/4681/24505404047_2db26758c1_k.jpg?classes=caption 'Campfire cooking.') {.center}

Behind our campsite, there was a large pond created by a beaver damming up the creek. Along the banks of the pond the sky was completely open and this spot made for some epic stargazing. After enjoying some campfire cooked meat and viewing the stars, we headed to bed.

### DAY 2

As always, we got up before sunrise, got some water boiling for coffee and breakfast then broke down camp. It's the same routine with every trip but it doesn't get old. We got on the trail shortly after sunrise and made a miscalculation with the route proceeding straight when we should have taken a right turn to continue on Dobbin Grade. There was a worn path that looked like the trail but after some time, we realized it was just a shortcut. The terrain was very interesting though and I'm glad we took this little side excursion because it made for some interesting pictures. The trail eventually found it's way back to the main trail and we were back on route. The day's hike would pass an active beaver dam, through alpine marshlands and over Blackbird Knob.

![](https://live.staticflickr.com/4732/25500792858_de2ca0fe34_k.jpg?classes=caption 'Sunrise along Upper Red Creek.') {.center}

![](https://live.staticflickr.com/4688/24505402457_54e714a09c_k.jpg?classes=caption 'Chris filtering water for coffee.') {.center}

![](https://live.staticflickr.com/4692/39340170462_d6f2d6ddb5_o.jpg?classes=caption 'Off trail navigation to find Dobbin Grade trail.') {.center}

![](https://live.staticflickr.com/4731/38661901734_7016f0f6c6_k.jpg?classes=caption 'Crossing Upper Red Creek again.') {.center}

We found a perfect campsite tucked a little ways back from the main spots where it looked like people commonly set up. There was a really well designed campfire ring complete with rock furniture. How could we pass this campsite up? We brewed a quick cup of coffee after getting our tents set up then started our day hike up Harmon Trail.

![](https://live.staticflickr.com/4679/24505399327_fea9d16e7d_k.jpg?classes=caption 'Making our way to the campsite just after Blackbird Knob.') {.center}

![](https://live.staticflickr.com/4690/39340167812_1e21d6332f_h.jpg?classes=caption 'Ken hiking up the hill just before our campsite.') {.center}

![](https://live.staticflickr.com/4597/24505395587_4e1699fb74_h.jpg?classes=caption 'We crossed Upper Red Creek to get to our campsite for night 2.') {.center}

![](https://live.staticflickr.com/4734/38661898884_50e0618ba8_h.jpg?classes=caption 'Glorious campfire furniture already arranged for us.') {.center}

I really like this new approach we seem to be taking with doing day hikes after setting up camp. It's a great way to cover more terrain and get some miles in without a weighted backpack. Harmon trail ascended to Rocky Ridge and it was interesting to see the vegetation change as we gained elevation. It went from pine forest to alpine marsh over the course of a few miles and a few hundred feet of elevation gain. 

![](https://live.staticflickr.com/4597/24505395147_e119b764a3_h.jpg?classes=caption 'The start of our dayhike on our way up to Harmon trail.') {.center}

Then back to pine forest as we made our way back down Blackbird Knob trail to the campsite. As the afternoon wore on, the clouds crept in and it almost looked it was going to rain. So we got our gear stowed away for the evening but stayed up to enjoy some campfire time. No stars for night 2 as there were only clouds and fog.

### DAY 3

The fog had set in and there would be be no epic sunrise to take pictures of. We got our wet gear together and had a quick cup of coffee then we got on the trail. The hike out would be an easy one as we just needed to climb Blackbird Knob trail to Rocky Ridge then head back to the car.

![](https://live.staticflickr.com/4680/24505393287_465d4248b9_h.jpg?classes=caption 'A foggy start to day 3.') {.center}

![](https://live.staticflickr.com/4636/24505391737_f32d30e778_h.jpg?classes=caption 'Making our way up to Rocky Ridge trail in the fog.') {.center}

![](https://live.staticflickr.com/4636/24505391627_ac16748500_h.jpg?classes=caption 'Conditions started to crappen. Rain began to happen.') {.center}


## FINAL THOUGHTS

Dolly Sods is always a great area and I really enjoy exploring its unique terrain. I have yet to make a summer trip there but I would definitely like to experience it in warmer weather to get the full spectrum of what the area has to offer. For this trip, one of my main takeaways is to check parking regulations before going somewhere. Another take away is that you can never plan too much. The time spent on getting the route together was well worth it and was a major factor with the success of this trip. I hope we get to go back up there soon!

![](https://live.staticflickr.com/4588/27593031839_9aec5b6953_b.jpg?classes=caption 'Ken, myself, and Chris after an awesome trip.')