---
title: ABOUT
subheading: 'Problem Solving Through Teamwork'
header_image: topo_web_1.jpg
media_order: about_fam-photo.jpg
---

<div class="content-block general">
	<img class="circle-photo" src="/user/pages/04.about/about_fam-photo.jpg">
</div>
<div class="content-block general half-width">
	<p>A designer/developer hybrid, my area of expertise resides at the overlap of visual design and front-end code and I am capable of building web products from the ground up. My goals are to be as helpful as possible to my organization, be accountable to my team, and build products that meet business goals and exceed customer expectations.</p>
    <p>Currently, I am a UI/UX Developer at Carilion Clinic where I design, build and maintain web products.</p>
</div>
<div class="social-links">
	<ul>
		<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
		<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
		<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
		<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
	</ul>
</div>