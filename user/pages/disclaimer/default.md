---
title: DISCLAIMER
media_order: home-header-bg.jpg
subheading: 'Terms of Use'
header_image: topo_web_1.jpg
---

<div class="content-block general">
    <h2>Common Sense</h2>
	<p>All users of this website must assume full responsibility for their own actions and personal safety while hiking, running or biking on any trail. I am not responsible for any injury you may occur by using the information on this website. While I try my best, I cannot guarantee the absolute correctness of all information on this website. Use common sense while on the trail and hike, run, or bike within your abilities.</p>
	<h2>Your Data</h2>
	<p>I do not collect data on this website. I do not use cookies to track site visitors via services like Google Analytics. I am not selling a product or a service so that information does not concern me. The main goal of this website is to provide background about my experiences with the intention to share knowledge with others that might have similar interests.</p>
</div>