---
title: 'W. SCOTT COOPER'
subheading: 'Front-End Development & Digital Design'
header_image: topo_web_1.jpg
body_classes: 'title-center title-h1h2'
media_order: 'header_home.jpg,work_square_cc.jpg,work_square_curiosity.jpg,work_square_kit.jpg,work_square_lp.jpg,work_square_migration.jpg,work_square_unconline.jpg,work_square_values.jpg,work_square_vc.jpg,work_square_wellness.jpg,icon_design.svg,icon_develop.svg,icon_maintain.svg,logo_large_color.svg'
visible: false
---

<div class="hero-block">
    <div class="hero-block--logo">
        <img src="/user/pages/01.home/logo_large_color.svg" alt="W logo">
        <h1>W. Scott Cooper</h1>
        <h2 class="desktop">UI/UX Developer</h2>
        <h2 class="mobile">UI/UX Developer</h2>
     </div>
</div>
<div class="content-block centered">
<p>A designer/developer hybrid, my area of expertise resides at the overlap of visual design and front-end code and I am capable of building web products from the ground up. My goals are to be as helpful as possible to my organization, be accountable to my team, and build products that meet business goals and exceed customer expectations.</p>
</div>

<div class="content-block icon-list">
    <h2>Services</h2>
	<ul>
        <li class="item">
            <img src="/user/pages/01.home/icon_design.svg" alt="Design icon">
            <h3>Design</h3>
        </li>
        <li class="item">
           <img src="/user/pages/01.home/icon_develop.svg" alt="Develop icon">
            <h3>Develop</h3>
        </li>
        <li class="item">
            <img src="/user/pages/01.home/icon_maintain.svg" alt="Maintain icon">
            <h3>Maintain</h3>
        </li>
    </ul>
</div>

<div class="content-block work-list">
     <h2>Recent Work</h2>
    <ul>
        <li class="item">
            <a href="/work/carilionclinic">
            	<ul>
                	<li><h4>Carilion Clinic</h4></li>
                	<li><h5>UI/UX &amp; Development</h5></li>
                	<li><h4>2021</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_cc.jpg" alt="Carilion Clinic">
            </a>
        </li>
        <li class="item">
            <a href="/work/carilioncosmetics">
            	<ul>
                	<li><h4>Drupal 8 Migration</h4></li>
                	<li><h5>Development</h5></li>
                	<li><h4>2021</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_migration.jpg" alt="Migration">
            </a>
        </li>
        <li class="item">
            <a href="/work/carilionwellness">
            	<ul>
                	<li><h4>Carilion Wellness</h4></li>
                	<li><h5>UI/UX &amp; Development</h5></li>
                	<li><h4>2020</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_wellness.jpg" alt="Carilion Wellness">
            </a>
        </li>
    </ul>
</div>

<div class="cta-banner">
    <a href="/work"><h2>View More</h2></a>
</div>

<div class="social-links">
	<ul>
        <li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        <li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        <li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        <li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    </ul>
</div>