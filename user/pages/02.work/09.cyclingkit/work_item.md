---
title: 'Cycling Kit'
body_classes: work
media_order: cc-vtc-cycling-kit.jpg
---

<div class="work--sidebar">
    <h2>Cycling Kit</h2>
    <h3>Apparel Design</h3>
    <h4>Graphic Design</h4>
    <p>This cycling kit was designed for Bike to Work Month and celebrates the partnership between Carilion Clinic and the Virginia Tech Carilion School of Medicine and Research Institute as certified bike friendly businesses. The kit is designed with the commuter in mind with fluorescent green on the traffic side and an eye catching argyle pattern on the curb side. We used high contrast colors and patterns to increase the visibility of the cyclist wearing this kit.</p>
    <div class="social-links">
		<ul>
        	<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        	<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        	<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        	<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    	</ul>
	</div>
</div>
<div class="work--samples">
    <ul>
        <li><img src="/user/pages/02.work/09.cyclingkit/cc-vtc-cycling-kit.jpg"></li>
    </ul>
</div>