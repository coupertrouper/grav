---
title: 'Carilion Clinic'
body_classes: work
media_order: 'cc_home_mobile.jpg,cc_home.jpg'
---

<div class="work--sidebar">
    <h2>Carilion Clinic</h2>
    <h3>UI/UX, Development</h3>
    <h4>Drupal, CSS, Customer Support</h4>
    <p>Carilion Clinic is a not-for-profit health care organization serving nearly one million people in Virginia through hospitals, outpatient centers and primary care practices. This website is fully decoupled utilizing Drupal's back-end capabilities with an Angular front-end for optimized performance. A large team works on this website but my primary responsibilities on this project are front-end development (HTML/CSS), site building, assisting with configuration, building landing pages, performing visual QA, and supporting our stakeholders' content needs.</p>
    <a class="external-link" href="https://carilionclinic.org/" target="_blank">Visit this Website</a>
    <div class="social-links">
		<ul>
        	<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        	<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        	<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        	<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    	</ul>
	</div>
</div>
<div class="work--samples">
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/03.carilionclinic/cc_home.jpg"></li>
    </ul>
</div>