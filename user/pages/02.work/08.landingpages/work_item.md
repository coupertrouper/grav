---
title: 'Campaign Landing Pages'
body_classes: work
media_order: 'cc_kts.jpg,cc_ent.jpg,cc_be-fast.jpg,cc_weightloss.jpg'
---

<div class="work--sidebar">
    <h2>Campaign Landing Pages</h2>
    <h3>UI/UX, Development</h3>
    <h4>Drupal, Angular, HTML, CSS</h4>
    <p>We developed a component that would allow us to quickly spin up landing pages that can scale for different types of ad campaigns. In some cases, a phone number CTA is the objective while in others, getting users to fill out a form after watching a set of videos is the goal. These landing pages have met the needs of our customers' campaigns and in several cases have resulted in measurable revenue generated for the organization.</p>
    <div class="social-links">
		<ul>
        	<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        	<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        	<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        	<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    	</ul>
	</div>
</div>
<div class="work--samples">
    <h3>Know the Signs</h3>
    <a class="external-link" href="https://carilionclinic.org/know-the-signs" target="_blank">Visit this Website</a>
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/08.landingpages/cc_kts.jpg"></li>
    </ul>
    <h3>Ear, Nose, &amp; Throat</h3>
    <a class="external-link" href="https://carilionclinic.org/ent" target="_blank">Visit this Website</a>
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/08.landingpages/cc_ent.jpg"></li>
    </ul>
    <h3>Be Fast: Stroke Awareness</h3>
    <a class="external-link" href="https://carilionclinic.org/be-fast" target="_blank">Visit this Website</a>
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/08.landingpages/cc_be-fast.jpg"></li>
    </ul>
    <h3>Bariatrics Campaign</h3>
    <a class="external-link" href="https://carilionclinic.org/weightloss" target="_blank">Visit this Website</a>
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/08.landingpages/cc_weightloss.jpg"></li>
    </ul>
</div>