---
title: 'Carilion Wellness'
body_classes: work
media_order: 'cw_home.jpg,cw_memberships.jpg'
---

<div class="work--sidebar">
    <h2>Carilion Wellness</h2>
    <h3>UI/UX, Development</h3>
    <h4>Drupal, HTML, Twig, CSS</h4>
    <p>Carilion Wellness is a gym and physical fitness center owned and operated by Carilion Clinic. We redesigned this website from the ground up after a deep dive into user behavior, analytics data and competitor analysis. We wanted to capitalize on lessons learned from the previous iteration of the website, which we also designed in-house, and ultimately build a better product. We handled all aspects of this website redesign from initial wireframes all the way to its launch.</p>
    <a class="external-link" href="https://carilionwellness.com/" target="_blank">Visit this Website</a>
    <div class="social-links">
		<ul>
        	<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        	<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        	<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        	<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    	</ul>
	</div>
</div>
<div class="work--samples">
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/05.carilionwellness/cw_home.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/05.carilionwellness/cw_memberships.jpg"></li>
    </ul>
</div>