---
title: 'Curiosity Campaign'
body_classes: work
media_order: 'curiosity_nurse.jpg,curiosity_foerst.jpg,curiosity_sarabia.jpg'
---

<div class="work--sidebar">
    <h2>Curiosity Campaign</h2>
    <h3>Branding &amp; Identity</h3>
    <h4>Graphic Design, HTML, CSS</h4>
    <p>We developed a set of concepts for an image capaign that dealt with one of Carilion's core values: Curiosity. The double exposure effect was used to convey a sense of wonder and depth. These pieces were used across both print and web on ads at Roanoke Regional Airport and on a landing page created for the campaign.</p>
    <a class="external-link" href="https://curiosity.carilionclinic.org/" target="_blank">Visit this Website</a>
    <div class="social-links">
		<ul>
        	<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        	<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        	<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        	<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    	</ul>
	</div>
</div>
<div class="work--samples">
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/07.curiosity/curiosity_nurse.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/07.curiosity/curiosity_foerst.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/07.curiosity/curiosity_sarabia.jpg"></li>
    </ul>
</div>