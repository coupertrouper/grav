---
title: 'Drupal 8 Migration'
body_classes: work
media_order: 'co_home.jpg,co_provider.jpg,co_services.jpg'
---

<div class="work--sidebar">
    <h2>Drupal 8 Migration</h2>
    <h3>Development</h3>
    <h4>Drupal, HTML, Twig, CSS</h4>
    <p>The goal of this project was to migrate an existing Drupal 7 website to Drupal 8. We used the opportunity rebuild Carilion Cosmetics from the ground up with a better template system, more intuitive content types, site configuration, and a more efficient custom theme. Since the goal was to simply match the new site 1:1 with the old site, it meant that UI/UX decisions were already made and we could focus solely on development. It may not be a glamorous new website design, but we're proud of this project.</p>
    <a class="external-link" href="https://carilioncosmetic.com/" target="_blank">Visit this Website</a>
    <div class="social-links">
		<ul>
        	<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        	<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        	<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        	<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    	</ul>
	</div>
</div>
<div class="work--samples">
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/04.carilioncosmetics/co_home.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/04.carilioncosmetics/co_provider.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/04.carilioncosmetics/co_services.jpg"></li>
    </ul>
</div>