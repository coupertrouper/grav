---
title: WORK
---

<div class="content-block centered-sub">
<p>A rare designer/developer hybrid, my area of expertise resides at the overlap of user interface design, user experience creation and front-end development. I am capable of building products from the ground up beginning with wireframes and ending in a final product that meets business goals and exceeds customer expectations.</p>
</div>

<div class="content-block work-list">
    <ul>
        <li class="item">
            <a href="/work/carilionclinic">
            	<ul>
                	<li><h4>Carilion Clinic</h4></li>
                	<li><h5>UI/UX &amp; Development</h5></li>
                	<li><h4>2021</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_cc.jpg" alt="Carilion Clinic">
            </a>
        </li>
        <li class="item">
            <a href="/work/carilioncosmetics">
            	<ul>
                	<li><h4>Drupal 8 Migration</h4></li>
                	<li><h5>Development</h5></li>
                	<li><h4>2021</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_migration.jpg" alt="Migration">
            </a>
        </li>
        <li class="item">
            <a href="/work/carilionwellness">
            	<ul>
                	<li><h4>Carilion Wellness</h4></li>
                	<li><h5>UI/UX &amp; Development</h5></li>
                	<li><h4>2020</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_wellness.jpg" alt="Carilion Wellness">
            </a>
        </li>
        <li class="item">
            <a href="/work/velocitycare">
            	<ul>
                	<li><h4>VelocityCare</h4></li>
                	<li><h5>UI/UX &amp; Development</h5></li>
                	<li><h4>2019</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_vc.jpg" alt="VelocityCare">
            </a>
        </li>
        <li class="item">
            <a href="/work/values">
            	<ul>
                	<li><h4>Values</h4></li>
                	<li><h5>Concept &amp; Visual Identity</h5></li>
                	<li><h4>2019</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_values.jpg" alt="Values">
            </a>
        </li>
        <li class="item">
            <a href="/work/curiosity">
            	<ul>
                	<li><h4>Curiosity Campaign</h4></li>
                	<li><h5>Branding &amp; Identity</h5></li>
                	<li><h4>2018</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_curiosity.jpg" alt="Curiosity Campaign">
            </a>
        </li>
        <li class="item">
            <a href="/work/landingpages">
            	<ul>
                	<li><h4>Campaign Landing Pages</h4></li>
                	<li><h5>UI/UX &amp; Development</h5></li>
                	<li><h4>2018</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_lp.jpg" alt="Landing Pages">
            </a>
        </li>
        <li class="item">
            <a href="/work/cyclingkit">
            	<ul>
                	<li><h4>Cycling Kit</h4></li>
                	<li><h5>Apparel Design</h5></li>
                	<li><h4>2017</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_kit.jpg" alt="Cycling Kit">
            </a>
        </li>
        <li class="item">
            <a href="/work/unconline">
            	<ul>
                	<li><h4>UNC Online</h4></li>
                	<li><h5>Graphic Design</h5></li>
                	<li><h4>2016</h4></li>
            	</ul>
            	<img src="/user/pages/01.home/work_square_unconline.jpg" alt="UNC Online">
            </a>
        </li>
    </ul>
</div>
<div class="social-links">
	<ul>
		<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
		<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
		<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
		<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
	</ul>
</div>