---
title: Values
body_classes: work
media_order: 'values_dots_1.jpg,values_molecules-03.jpg,values_molecules-04.jpg,values_molecules-05.jpg,values_molecules-06.jpg,values_periodic-table_1.jpg,values_periodic-table_2.jpg,values_periodic-table_3.jpg,values_periodic-table_4.jpg,values_dots_2.jpg,values_dots_3.jpg,values_dots_4.jpg'
---

<div class="work--sidebar">
    <h2>Values</h2>
    <h3>Concept &amp; Visual Identity</h3>
    <h4>Graphic Design</h4>
    <p>We were tasked with coming up with new concepts for the visual identity that Carilion Clinic uses internally to represent the organization's core vlues. The entire creative team participated in this exercise and my work was in the mix with 4 other designers. Ultimately, my concepts weren't chosen but I'm still proud of them and they show my versatility as a designer. I contributed 3 concepts to the overall effort, each summarized here.</p>
    <div class="social-links">
		<ul>
        	<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        	<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        	<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        	<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    	</ul>
	</div>
</div>
<div class="work--samples">
    <h3>Anthropomorphic Dots Concept</h3>
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_dots_1.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_dots_2.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_dots_3.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_dots_4.jpg"></li>
    </ul>
    <h3>Atom/Molecule Concept</h3>
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_molecules-03.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_molecules-04.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_molecules-05.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_molecules-06.jpg"></li>
    </ul>
    <h3>Atom/Molecule Concept</h3>
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_periodic-table_1.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_periodic-table_2.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_periodic-table_3.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/06.values/values_periodic-table_4.jpg"></li>
    </ul>
</div>