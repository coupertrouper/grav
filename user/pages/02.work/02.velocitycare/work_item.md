---
title: VelocityCare
media_order: 'vc_home.jpg,vc_location-detail.jpg,vc_location-list.jpg'
custom:
    coverImage: {  }
body_classes: work
---

<div class="work--sidebar">
    <h2>VelocityCare</h2>
    <h3>UI/UX, Development</h3>
    <h4>HTML, Sass, Twig, Drupal</h4>
    <p>VelocityCare by Carilion Clinic provides urgent care medical services for non-life threatening illnesses and injuries. After a long round of gathering requirements, researching user behavior, doing competitive analysis, creating wireframes, building prototypes, and assembling content we redesigned and rebuilt this website from the ground up. We handled all aspects of the product from initial kick-off meeting to deployment and continue to maintain the site to make improvements and provide general maintenance support.</p>
    <a class="external-link" href="https://velocitycarebycarilion.com/" target="_blank">Visit this Website</a>
    <div class="social-links">
		<ul>
        	<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        	<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        	<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        	<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    	</ul>
	</div>
</div>
<div class="work--samples">
    <ul>
        <li><img class="img-fluid" src="/user/pages/02.work/02.velocitycare/vc_home.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/02.velocitycare/vc_location-detail.jpg"></li>
        <li><img class="img-fluid" src="/user/pages/02.work/02.velocitycare/vc_location-list.jpg"></li>
    </ul>
</div>