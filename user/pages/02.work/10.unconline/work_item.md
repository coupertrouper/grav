---
title: 'UNC Online'
body_classes: work
media_order: unc-infographic-2.jpg
---

<div class="work--sidebar">
    <h2>UNC Online</h2>
    <h3>Infograpics</h3>
    <h4>Graphic Design</h4>
    <p>UNC Online contacted me to produce an infographic showcasing data about enrollment trends during the 2014-15 academic year. I combined the brand color palette and typography with the data that the client provided to produce an easy to read infographic that flows from data point to data point. My goal was to use the elements of shape and balance to move the viewer through the data from top to bottom. This project was a pleasure to work on and I always enjoy getting to flex my design skills.</p>
    <div class="social-links">
		<ul>
        	<li><a href="https://www.behance.net/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_behance.svg"></a></li>
        	<li><a href="https://dribbble.com/wscottcooper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_dribbble.svg"></a></li>
        	<li><a href="https://www.linkedin.com/in/w-scott-cooper/" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_linkedin.svg"></a></li>
        	<li><a href="https://www.drupal.org/u/coupertrouper" target="_blank"><img src="/user/themes/blog-folio-2021/images/icon_drupal9.svg"></a></li>
    	</ul>
	</div>
</div>
<div class="work--samples">
    <ul>
        <li><img src="/user/pages/02.work/10.unconline/unc-infographic-2.jpg"></li>
    </ul>
</div>