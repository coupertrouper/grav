---
title: Lab
media_order: 'series_1_B_display.jpg,series_1_B.jpg,series_1_A_display.jpg,series_1_C_display.jpg,series_1_C.jpg,series_1_A.jpg'
---

<div class="content-block general half-width">
	<p>Remember when the internet was fun? OK. It's still fun. But remember the early 2000's when websites were built on Flash and were more like works of art than interfaces used for the sole purposes of selling products or services? (I'm showing my age here.) Alright—there are arguably dozens of reasons why we should never ever (ever) go back to Flash based websites, the main obvious reason being that Flash died a long and slow death a long time ago... but the web can be and still is a fun place. There is just more unfun stuff to sort through.</p>
    <p>As I've moved through my career, my work has drifted farther and farther away from genuine creative output to more of a formulaic approach where decisions are based on data and the output generally follows a similar formula from product to product. <em>And that is fine</em>. Data driven decisions certainly have their place especially in the world of UX and can make or break the success of a product. Because of this shift, I've lost a big creative outlet and that initial spark for what brought me to the world of web design in the first place.</p>
    <p>So here we are. I've always wanted to have a section of my portfolio site devoted to <em>play</em> where I try out new ideas that I've been tinkering with or share things that I've been working on that wouldn't be a good fit for Dribbble or Behance. That's what this creative Lab section is all about. A place for secret projects and elite side missions. The content here will seem random but will have one common thread—digital creativity.</p>
</div>
<div class="content-block general">
    <h2 id="wallpapers">Wallpapers: Series 1</h2>
    <div class="project-id-grid">
        <ul>
            <li class="item">
                <h3 class="heading">Concept</h3>
                <p class="content">Create bespoke wallpapers for mobile devices consisting of topographical map imagery of the coast of Lake Superior in Michigan, USA. Colorways and patterns are inspired by the m90 camouflage used by the Swedish military.</p>
            </li>
            <li class="item">
            	<h3 class="heading">Process &amp; Tools</h3>
                <p class="content">Hand traced topographical maps using screenshots of GIS data with grunge texture overlays. Tools used: Illustrator, Photoshop, and GaiaGPS</p>
            </li>
            <li class="item">
            	<h3 class="heading">Year</h3>
                <p class="content">2022</p>
            </li>
        </ul>
    </div>
    <div class="backgrounds-grid">
        <ul>
            <li class="item">
                <ul>
                    <li class="bg-image"><img src="/user/pages/lab/series_1_A_display.jpg" alt="Series 1 A Topographic Map Background"></li>
                    <li><a href="/user/pages/lab/series_1_A.jpg" target="_blank" title="series_1_A"><img class="icon" src="/user/themes/blog-folio-2021/images/icon_download-solid.svg"> Download for Mobile</a></li>
                    <li class="small">res: [ 1080px x 1920px ] | format: [ jpg ]</li>
                </ul>
            </li>
            <li class="item">
                <ul>
                    <li class="bg-image"><img src="/user/pages/lab/series_1_B_display.jpg" alt="Series 1 B Topographic Map Background"></li>
                    <li><a href="/user/pages/lab/series_1_B.jpg" target="_blank" title="series_1_B"><img class="icon" src="/user/themes/blog-folio-2021/images/icon_download-solid.svg"> Download for Mobile</a></li>
                    <li class="small">res: [ 1080px x 1920px ] | format: [ jpg ]</li>
                </ul>
            </li>
            <li class="item">
                <ul>
                    <li class="bg-image"><img src="/user/pages/lab/series_1_C_display.jpg" alt="Series 1 C Topographic Map Background"></li>
                    <li><a href="/user/pages/lab/series_1_C.jpg" target="_blank" title="series_1_C"><img class="icon" src="/user/themes/blog-folio-2021/images/icon_download-solid.svg"> Download for Mobile</a></li>
                    <li class="small">res: [ 1080px x 1920px ] | format: [ jpg ]</li>
                </ul>
            </li>
    	</ul>
    </div>
    <div class="backgrounds-grid">
        <ul>
            <li class="item">
                <ul>
                    <li class="bg-image"><img src="/user/pages/lab/series_1_D_display.jpg" alt="Series 1 D Topographic Map Background"></li>
                    <li><a href="/user/pages/lab/series_1_D.jpg" target="_blank" title="series_1_D"><img class="icon" src="/user/themes/blog-folio-2021/images/icon_download-solid.svg"> Download for Mobile</a></li>
                    <li class="small">res: [ 1080px x 1920px ] | format: [ jpg ]</li>
                </ul>
            </li>
            <li class="item">
                <ul>
                    <li class="bg-image"><img src="/user/pages/lab/series_1_E_display.jpg" alt="Series 1 E Topographic Map Background"></li>
                    <li><a href="/user/pages/lab/series_1_E.jpg" target="_blank" title="series_1_E"><img class="icon" src="/user/themes/blog-folio-2021/images/icon_download-solid.svg"> Download for Mobile</a></li>
                    <li class="small">res: [ 1080px x 1920px ] | format: [ jpg ]</li>
                </ul>
            </li>
            <li class="item">
                <ul>
                    <li class="bg-image"><img src="/user/pages/lab/series_1_F_display.jpg" alt="Series 1 F Topographic Map Background"></li>
                    <li><a href="/user/pages/lab/series_1_F.jpg" target="_blank" title="series_1_F"><img class="icon" src="/user/themes/blog-folio-2021/images/icon_download-solid.svg"> Download for Mobile</a></li>
                    <li class="small">res: [ 1080px x 1920px ] | format: [ jpg ]</li>
                </ul>
            </li>
    	</ul>
    </div>
</div>