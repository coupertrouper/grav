# Blog Folio 2021 Theme

The **Blog Folio 2021** Theme is for [Grav CMS](http://github.com/getgrav/grav).  This README.md file should be modified to describe the features, installation, configuration, and general usage of this theme.

## Description

Theme for 2021 containing both blog and portfolio

## Compiling Sass

Use node-sass to compile sass. Use the command 'npm run scss'